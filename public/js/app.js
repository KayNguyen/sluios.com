shop.login = function () {
    var email = $('#pop-email-login'),
        password = $('#pop-pw-login');

    if(shop.is_email(email.val())){
        if(password.val() != ''){
            shop.ajax_popup('login', 'post', {
                email: $.trim(email.val()),
                password: $.trim(password.val())
            }, function(json) {
                if(json.error == 1){
                    alert(shop.authMsg(json.code));
                }else {
                    shop.reload();
                    // shop.popup.hide('loginModal', function () {
                    //     shop.redirect(data.data['url']);
                    // });
                }
            });
        }else{
            alert('Vui lòng nhập mật khẩu');
            password.focus();
        }
    }else {
        alert('Email không hợp lệ');
        email.focus();
    }
};

shop.register = function () {
    var email = $('#pop-email'),
        password = $('#pop-pw'),
        password_confirm = $('#pop-pw-rp');
        account_type = $('#account_type');

    if(shop.is_email(email.val())){
        if(password.val() != ''){
            if(password.val().length >= 6) {
                if (password.val() == password_confirm.val()) {
                    shop.ajax_popup('register', 'post', {
                        email: $.trim(email.val()),
                        password: $.trim(password.val()),
                        password_confirmation: $.trim(password_confirm.val()),
                        acc_type : $.trim(account_type.prop('checked')),
                    }, function (json) {
                        if (json.error == 1) {
                            var msg = shop.authMsg(json.code);
                            if(msg == '') {
                                for(var i in json.code){
                                    msg += json.code[i] + '\n';
                                }
                            }
                            alert(msg);
                        } else {
                            shop.redirect(json.data['url']);
                        }
                    });
                } else {
                    alert('Xác thực mật khẩu không khớp');
                    password_confirm.focus();
                }
            }else{
                alert('Mật khẩu phải có tối thiểu 6 kí tự');
                password.focus();
            }
        }else{
            alert('Vui lòng nhập mật khẩu');
            password.focus();
        }
    }else {
        alert('Email không hợp lệ');
        email.focus();
    }
};


shop.subscribe = function(){
    var email = $('#sub_email'),
        email_val = $.trim(email.val());
    // name = $('#sub_name'),
    // name_val = $.trim(name.val());
    // if(name_val == ''){
    //     $.alertable.alert('Vui lòng nhập tên của bạn');
    //     name.focus();
    //     return false;
    // }else
    if(email_val == ''){
        Swal.fire({
            type: 'error',
            title: 'Vui lòng nhập email của bạn',
            text: 'Bạn chưa nhập email!',
            inputAttributes: {
                autocapitalize: 'off'
            },
            confirmButtonText: 'ok',
            showLoaderOnConfirm: true,

        });
        email.focus();
        return false;
    }else if(!shop.is_email(email_val)){
        Swal.fire({
            type: 'error',
            title: 'Email không hợp lệ',
            text: 'Bạn kiểm tra lại email!',
            inputAttributes: {
                autocapitalize: 'off'
            },
            confirmButtonText: 'ok',
            showLoaderOnConfirm: true,

        });
        email.focus();
        return false;
    }
    shop.ajax_popup('subscribe', 'post', { email: email_val}, function (json) {
        if(json.error == 0) {
            email.val('');
            // name.val('');
            Swal.fire({
                type: 'success',
                title: 'Thành công',
                text: 'Chúc mừng bạn!',
                inputAttributes: {
                    autocapitalize: 'off'
                },
                confirmButtonText: 'ok',
                showLoaderOnConfirm: true,

            });
        }else{
            $.alertable.alert(json.msg);
            email.focus();
        }
    });
};

shop.changeCustomerPassword = function(){
    var curPass = $('#current_password'),
        newPass = $('#new_password'),
        rePass = $('#re_password');

    $('.invalid-feedback').html('');
    $('.is-invalid').removeClass('is-invalid');

    //check old pass
    if (shop.is_blank($.trim(curPass.val()))) {
        // nếu chưa nhâp thì vào đây
        curPass.addClass('is-invalid');
        $('.invalid-feedback', curPass.parent()).html('Vui lòng nhập mật khẩu cũ');
        return;
    }

    //check new pass
    if (shop.is_blank($.trim(newPass.val()))) {
        newPass.addClass('is-invalid');
        $('.invalid-feedback', newPass.parent()).html('Vui lòng nhập mật khẩu mới');
        return;
    } else if (newPass.val().length < 8) {
        newPass.addClass('is-invalid');
        $('.invalid-feedback', newPass.parent()).html('Mật khẩu mới phải có 8 kí tự trở lên');
        return;
    } else if (newPass.val() == curPass.val()) {
        newPass.addClass('is-invalid');
        $('.invalid-feedback', newPass.parent()).html('Mật khẩu mới phải khác mật khẩu cũ');
        return;
    }

    //check retype pass
    if (shop.is_blank($.trim(rePass.val()))) {
        rePass.addClass('is-invalid');
        $('.invalid-feedback', rePass.parent()).html('Vui lòng nhập lại mật khẩu mới');
        return;
    } else if (newPass.val() != rePass.val()) {
        rePass.addClass('is-invalid');
        $('.invalid-feedback', rePass.parent()).html('Nhập lại mật khẩu mới không khớp');
        return;
    }

    shop.ajax_popup('change-password', 'post', {
        oldPassword: $.trim(curPass.val()),
        newPassword: $.trim(newPass.val()),
    }, function(json) {
        $('.invalid-feedback').html('');
        if (json.error == 1) {
            var msg = shop.authMsg(json.code);
            if(msg == '') {
                for(var i in json.code){
                    msg += json.code[i] + '\n';
                }
            }
            alert(msg);
        } else {
            shop.redirect(json.data['url']);
        }
    });
}


shop.authMsg = function ($code) {
    switch ($code){
        case 'LOGIN_FAIL': return 'Sai tên đăng nhập hoặc mật khẩu';
        case 'BANNED': return 'Tài khoản đã bị vô hiệu, không thể đăng nhập';
        case 'NOT_ACTIVE': return 'Tài khoản chưa được kích hoạt';
        case 'NOT_EXISTED': return 'Email không hợp lệ';
        case 'LOGINED': return 'Đã đăng nhập thành công trước đó';
        case 'EXISTED': return 'Email không hợp lệ';
    }
    return '';
};

shop.getCat = function(type, def, lang, container) {
    shop.ajax_popup('category/fetch-cat-lang', 'POST', {type: type, def: def, lang:lang}, function(json){
        if(json.error == 0) {
            $(container).html(json.data);
        }else{
            alert(json.msg);
        }
    });
}

shop.loadProcess = function(id, type = ''){
    shop.ajax_popup(type+'load-process', 'POST', {id: id}, function(json){
        if(json.error == 0) {
            var data = json.data,
                html = shop.join
                ('<figure class="m-0">')
                ('<img class="w-100" src="'+data.image+'" alt="">')
                ('</figure>')
                ();
                $('#quy-trinh-1').empty();
                $('#quy-trinh-1').append(html);
                $('#content').empty();
                $('#content').append(data.body);
        }else{
            alert(json.msg);
        }
    });
},

shop.system = {
    ckEditor: function (ele,width,height,theme,toolbar, css,id_img_btn) {
        css = css ? css : (ENV.BASE_URL + 'css/style_editor.css?v=1');
        var instance_ck = CKEDITOR.replace(ele ,
            {
                toolbar : toolbar,
                width: width,
                height: height,
                language : 'vi',
                contentsCss: css
            });
        instance_ck.addCommand("mySimpleCommand", {
            exec: function(edt) {
                var abc = $('#uploadifive-'+id_img_btn+' input');
                if(typeof abc != 'undefined') {
                    $(abc[abc.length - 1]).click();
                }
            }
        });
        instance_ck.ui.addButton('ImgUploadBtn', {
            type: 'button',
            label: "Upload ảnh lên chèn vào nội dung",
            command: 'mySimpleCommand',
            // toolbar: 'insert',
            icon: 'plugins/iconfinder_image_272698.png',
        });
    }
};
shop.actived = function() {
    setTimeout(function () {shop.ajax_popup('user/actived', 'GET')}, 1000);
};
shop.tags = {
    init_more:function (container){
        $(container).tagEditor({
            initialTags: $(container).val().split(','),
            sortable: false,
            forceLowercase: false,
            placeholder: '',
            onChange: function (field, editor, tags) {
                $(field).val(tags.length ? tags.join(',') : '');
            }

        });
    },
    init: function(type, container, id, suggest, no_load_more){
        if(suggest || no_load_more) {
            $(container).tagEditor({
                initialTags: $(container).val().split(','),
                autocomplete: {
                    delay: 0, // show suggestions immediately
                    position: {collision: 'flip'}, // automatic menu position up/down
                    source: suggest ? suggest : []
                },
                sortable: false,
                forceLowercase: false,
                placeholder: '',
                onChange: function (field, editor, tags) {
                    $(field).val(tags.length ? tags.join(',') : '');
                },
                beforeTagSave: function (field, editor, tags, tag, val) {
                    shop.tags.add(val, type);
                },
                beforeTagDelete: function (field, editor, tags, val) {
                    var q = confirm('Xóa tag "' + val + '"?');
                    if (q) {
                        shop.tags.remove(val, type, id);
                    }
                    return q;
                }
            });
        }else{
            shop.tags.loadSuggest(type, container, id);
        }
    },
    loadSuggest: function(type, container, id){
        shop.ajax_popup('tag-suggest', 'POST', {type: type}, function(json){
            if(json.error == 0) {
                shop.tags.init(type, container, id, json.data, true);
            }else{
                alert(json.msg);
            }
        });
    },
    add: function(tag, type){
        shop.ajax_popup('tag-add', 'POST', {tag: tag, type: type}, function(json){
            if(json.error != 0) {
                alert(json.msg);
            }
        });
    },
    remove: function(tag, type, id){
        shop.ajax_popup('tag-del', 'POST', {tag: tag, type: type, id: id}, function(json){
            if(json.error != 0) {
                alert(json.msg);
            }
        });
    }
},
shop.ready.add(function (){
    shop.actived();
    $('.datepicker').datepicker({
        dateFormat: 'dd/mm/yy',
        changeMonth: true,
        changeYear: true
    });
}, true);

shop.bookMsg = function ($code) {
    switch ($code){
        case 'BOOK_FAIL': return 'Vui lòng điền đầy đủ thông tin cần thiết';
        case 'BANNED': return 'Tài khoản đã bị vô hiệu, không thể đăng nhập';
        case 'NOT_ACTIVE': return 'Tài khoản chưa được kích hoạt';
        case 'NOT_EXISTED': return 'Email không hợp lệ';
        case 'LOGINED': return 'Đã đăng nhập thành công trước đó';
        case 'EXISTED': return 'Email không hợp lệ';
    }
    return '';
};

shop.book = function () {
    let email = $('#bok-email'),
        phone = $('#bok-phone');
        user_name = $('#bok-name');
        address = $('#bok-address');
        option = $('#bok-option');
        coupon = $('#bok-coupon');
        opt_detail = $('#bok-opt-detail');

    if(shop.is_phone(phone.val())){
        if(user_name.val() != ''){
            if (option.val() != -1) {
                shop.ajax_popup('book', 'post', {
                    email: $.trim(email.val()),
                    phone: $.trim(phone.val()),
                    user_name: $.trim(user_name.val()),
                    opt_detail: (opt_detail) ?  $.trim(opt_detail.val()) : '',
                    address: $.trim(address.val()),
                    option: $.trim(option.val()),
                    coupon: $.trim(coupon.val()),
                }, function(json) {
                    if(json.error == 1){
                        alert(shop.bookMsg(json.code));
                    }else {
                        Swal.fire({
                            type: 'success',
                            title: 'Thành Công',
                            text: 'Bạn đã đặt dịch vụ thành công!',
                            inputAttributes: {
                                autocapitalize: 'off'
                            },
                            confirmButtonText: 'ok',
                            showLoaderOnConfirm: true,
                            preConfirm: () => {
                                shop.reload();
                            },
                        });
                    }
                });
            } else {
                alert('Vui lòng chọn dịch vụ bạn muốn!');
                option.focus();
            }

        }else{
            alert('Vui lòng nhập họ tên');
            user_name.focus();
        }
    }else {
        alert('Số điện thoại không hợp lệ');
        phone.focus();
    }

}

shop.getPriceTabService = function ($id) {
    let email = $('#bok-email'),
        phone = $('#bok-phone');
        user_name = $('#bok-name');
        address = $('#bok-address');
        option = $('#bok-option');
        coupon = $('#bok-coupon');

    if(shop.is_phone(phone.val())){
        if(user_name.val() != ''){
            if (option.val() != -1) {
                shop.ajax_popup('getPriceTabService', 'post', {
                    email: $.trim(email.val()),
                    phone: $.trim(phone.val()),
                    user_name: $.trim(user_name.val()),
                    address: $.trim(address.val()),
                    option: $.trim(option.val()),
                    coupon: $.trim(coupon.val()),
                }, function(json) {
                    if(json.error == 1){
                        alert(shop.bookMsg(json.code));
                    }else {
                        Swal.fire({
                            type: 'success',
                            title: 'Thành Công',
                            text: 'Bạn đã đặt dịch vụ thành công!',
                            inputAttributes: {
                                autocapitalize: 'off'
                            },
                            confirmButtonText: 'ok',
                            showLoaderOnConfirm: true,
                            preConfirm: () => {
                                shop.reload();
                            },
                        });
                    }
                });
            } else {
                alert('Vui lòng chọn dịch vụ bạn muốn!');
                option.focus();
            }

        }else{
            alert('Vui lòng nhập họ tên');
            user_name.focus();
        }
    }else {
        alert('Số điện thoại không hợp lệ');
        phone.focus();
    }

}

var Member = {
    /***
     * Lay template form login
     * @param innerElement
     */
    getLoginForm: function (innerElement) {
        shop.ajax_popup('/member/login/get-template', [], function (json) {
            if (typeof json.data !== 'undefined') {
                if (typeof json.data.template !== 'undefined') {
                    document.getElementById(innerElement).innerHTML = json.data.template;
                }
            }
        });
    },
    showLoginForm: function () {
        $('#registerForm').modal('hide');

        $('#loginForm').modal('show');

    },
    doLogin: function (form) {
        var data = jQuery(form).serializeArray();
        console.log(data)
        shop.ajax_popup('login', 'post', data, function (json) {
            console.log(json)
            if (typeof json.data !== 'undefined') {
                if (json.error == 0) {
                    window.location.reload();
                } else {
                    alert(shop.authMsg(json.code));
                }
            } else {
                if (typeof json.msg === 'undefined') {
                    alert('Đăng nhập không thành công. Vui lòng liên hệ admin để được hỗ trợ: 0886.509.919');
                } else {
                    alert(shop.authMsg(json.code));
                }

            }
        });
    },
    doRegister: function (form) {
        var data = jQuery(form).serializeArray();
        console.log(data)
        shop.ajax_popup('register', 'post', data, function (json) {
            
            if (json.error == 0) {
                window.location.reload();
            }else {
                alert(shop.authMsg(json.code));
            }
        });
    },

};