<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Tag;

use Carbon\Carbon;
use Illuminate\Support\Facades\Cookie;

class Product extends Model
{
    //
    protected $table = 'products';
    public $timestamps = false;
    const KEY = 'product';
    const NUM_KEY = 1;
    const KEY_COOKIE_PRDS_HISTORY = 'COOKIE_HISTORY_PRODUCT_';

    public function category(){
        return $this->hasOne(Category::class, 'id', 'cat_id');
    }

    public function tags(){
        return $this->belongsToMany(Tag::class, 'tag_details', 'object_id', 'tag_id');
    }


    public static function getImageGallery($hotel_id = 0,$type="hotel", $json = false){
        $images = ProductImage::where('type',$type)->where('object_id', $hotel_id)->orderByRaw('sort desc,created desc')->get();
        $data = [];
        foreach($images as $image){
            $tmp = $image->toArray();
            $tmp['img'] = $image->image;
            $tmp['image_sm'] = $image->getImageUrl('hotel_small');
            $tmp['image_md'] = $image->getImageUrl('hotel_preview');
            $tmp['image'] = $image->getImageUrl('hotel_large');
            $tmp['image_org'] = $image->getImageUrl();
            array_push($data, $tmp);
        }
        return $json ? json_encode($data) : $data;
    }

    public function getImageUrl($size = 'original'){
        return \ImageURL::getImageUrl($this->image, 'product', $size);
    }

    public function getLink(){
        return self::getLinkDetail($this->title_seo, $this->id);
    }

    public static function getType($cat_id){
        $cate = self::with('category')->where('cat_id', $cat_id)->first();
        if($cate) {
            return $cate->category->title;
        }
        // dd($cat_id, $cate);
        return Category::where('id', $cat_id)->first()->title;
    }

    

    public static function getLinkDetail($title_seo = '', $id = 0, $type = 'news', $cat_id = 0){
        return route($type.'.detail', ['safe_title' => str_slug($title_seo), 'id' => $id, 'cat_id' => $cat_id]);
    }

    public function lang(){
        $lang = config('app.locales');
        return isset($lang[$this->lang]) ? $lang[$this->lang] : 'vi';
    }


    public static function getListNew($lang = 'vi', $limit = 7, $except = '', $type = 'all'){
        $cond = [
            ['status', '=', 2],
            ['published', '>', 0],
            ['lang', '=', $lang],
        ];
        // if($type != 'all'){
        //     $cond[] = ['type', '=', $type];
        // }
        $data = self::select('id', 'title', 'title_seo', 'image', 'sort_body')
            ->where($cond);
        if(!empty($except)){
            if(!is_array($except)){
                $except = [$except];
            }
            $data = $data->whereNotIn('id', $except);
        }
        return $data->orderBy('published', 'desc')
            ->limit($limit)
            ->get();
    }

    public function images() {
        return $this->hasMany('App\Models\ProductImage', 'object_id', 'id')->where('type','product');
    }

    public function link_videos() {
        return $this->hasMany('App\Models\History', 'pro_id', 'id')->where('type','1');
    }

    public static function getProductByCate($cat_id, $limit = false) {
        $data = self::where('status', '>', 1)->where('cat_id', $cat_id)
                ->select('id', 'title', 'alias', 'created', 'cat_id')
                ->orderBy('created', 'desc');
        if ($limit > 0) {
           return $data->limit($limit)->get();
        }
        return $data->get();
    }

    public static function getProductAll() {

        return self::with('category')
                ->where('status', '>', 1)->where('cat_id', '!=', null)->select('cat_id', 'id')
                ->orderBy('created', 'desc')->get();
    }

    public static function getProductHot() {

        return self::with('category')
                ->where('status', '>', 1)->where('cat_id', '!=', null)
                ->where('hot', '>', 1)->get();
    }

    public static function getAllNameProduct() {

        return self::select('title', 'alias')
                ->where('status', '>', 1)
                ->get()->toArray();
    }

    public static function getAllTagsForNews($id) {
        return self::with('tags')->where('id', $id)->get();
    }

    public static function savePrdAfterView($id = 0) {
        $prds_cookie = Cookie::get(Product::KEY_COOKIE_PRDS_HISTORY, []);
        $prds_cookie = !empty($prds_cookie) ? unserialize($prds_cookie) : [];

        if(count($prds_cookie) > 0 && count($prds_cookie) < 10){
            $prds_cookie = array_splice($prds_cookie, 0, 1);
        }
        $prds_cookie[] = $id;

        $prds_cookie = serialize(array_unique($prds_cookie));
        Cookie ::queue(Product::KEY_COOKIE_PRDS_HISTORY, $prds_cookie, 60*24*365);
    }

    public static function prdHistory($limit = 8)
    {
        $prds_cookie = Cookie::get(Product::KEY_COOKIE_PRDS_HISTORY, []);

        $prds_cookie = !empty($prds_cookie) ? unserialize($prds_cookie) : [];

        if(!empty($prds_cookie)) {
            return self::where('status', 2)
                ->whereIn('id',$prds_cookie)
                ->limit($limit)
                ->get();
        }
        return [];
    }

    public static function getRelated($lang = 'vi', $limit = 3, $id){
        //lay toan bo danh sach tag cua tin
        $tags = TagDetail::getTags($id);
        $ids = [];
        foreach($tags as $item){
            $ids[] = $item['tag_id'];
        }
        //lay toan bo tin cung tag
        $news = TagDetail::getNews($ids);
        $ids = [];
        foreach($news as $item){
            if($item['object_id'] != $id && !in_array($item['object_id'], $ids)) {
                $ids[] = $item['object_id'];
            }
        }

        return self::with(['detail', 'category'])
            ->leftJoin('news_detail', 'news_detail.new_id', '=', 'news.id')
            ->select(\DB::raw("COUNT(news_detail.new_id) as rank"), \DB::raw("SUM(news_detail.vote) as vote"), 'news.*')
            ->where([
                ['news.status', '=', 2],
                ['news.published', '>', 0],
                ['news.lang', '=', $lang],
            ])->groupBy('news_detail.new_id')
            ->whereIn('news.id', $ids)
            ->limit($limit)
            ->get();
    }

}

