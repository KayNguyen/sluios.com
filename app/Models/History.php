<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class History extends Model
{
    protected $table = 'dev_history';
    public $timestamps = false;
    protected static $cat = [];
    protected static $splitKey = '_';
    protected static $type = [
        '1' => 'Sản phẩm',
        '2' => 'Tin tức',
    ];

    public function getImageUrl($size = 'original'){
        return \ImageURL::getImageUrl($this->image, 'history', $size);
    }
}