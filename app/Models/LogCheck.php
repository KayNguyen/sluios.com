<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class LogCheck extends Model
{
    protected $table = 'logs_check';
    public $timestamps = false;

    public function getAction(){
        return \MyLog::do()->getAction($this->action);
    }

    public static function setLog($param = '', $advice = '', $result = '', $user_id = 0, $type = 1) {

        return self::insert([
            'param' => $param,
            'advice' => $advice,
            'result' => $result,
            'user_id' => $user_id,
            'type' => $type,
            'created' => time(),
        ]);
    }
}