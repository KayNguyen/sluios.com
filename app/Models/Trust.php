<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class Trust extends Model
{
    //
    protected $table = 'trust';
    public $timestamps = false;

    public function getImageUrl($size = 'original'){
        return \ImageURL::getImageUrl($this->image, 'trust', $size);
    }

    public static function getAllTrust() {
        return self::where('status', '>', 1)->orderByRaw('sort DESC')->get();
    }

}