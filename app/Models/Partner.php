<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class Partner extends Model
{
    //
    protected $table = 'partner';
    public $timestamps = false;

    public function getImageUrl($size = 'original', $filed = 'image'){
        return \ImageURL::getImageUrl($this->$filed, 'partner', $size);
    }

    public static function getAllPartner() {
        return self::where('status', '>', 1)->orderByRaw('sort DESC')->get();
    }
}