<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class Widget extends Model
{
    //
    protected $table = 'widget';
    static $orderBy = [
        '_1' => 'Recent',
        '_2' => 'Popular',
        '_3' => 'Random'
    ];
    static $display = [
        '_1' => 'Category/s',
        '_2' => 'Latest',
    ];
    public $timestamps = false;

    public static $register_sidebar = [
        array(
            'name'          => 'Left',
            'id'            => 'abc',
            'description'   => 'Add widgets here to appear in your footer.',
            'before_widget' => '<section id="%1$s" class="widget %2$s">',
            'after_widget'  => '</section>',
            'before_title'  => '<h2 class="widget-title">',
            'after_title'   => '</h2>',
        ),
        array(
            'name'          => 'Main',
            'id'            => 'abc',
            'description'   => 'Add widgets here to appear in your footer.',
            'before_widget' => '<section id="%1$s" class="widget %2$s">',
            'after_widget'  => '</section>',
            'before_title'  => '<h2 class="widget-title">',
            'after_title'   => '</h2>',
        ),
        array(
            'name'          => 'Right',
            'id'            => 'abc',
            'description'   => 'Add widgets here to appear in your footer.',
            'before_widget' => '<section id="%1$s" class="widget %2$s">',
            'after_widget'  => '</section>',
            'before_title'  => '<h2 class="widget-title">',
            'after_title'   => '</h2>',
        )
    ];

    public static function widgetsInit() {
        $init = [
            'wd-post' => array(
                'title' => ' Post',
                'id'  => 'wd-post',
                2 => array(
                    'orderby' => self::$orderBy,
                    'display' => self::$display,
                    'category' => [],
                    'number' => 5,
                ),
                '_multiwidget' => 1,
            ),
            'wd-videos'=> array(
                'title' => 'Videos',
                'id'  => 'wd-videos',
                2 => array(
                    'number' => 5,
                    'orderby' => self::$orderBy,
                    'display' => self::$display,
                    'category' => [],
                ),
                '_multiwidget' => 1,
            ),
            'wd-gallery'=> array(
                'title' => 'Gallery',
                'id'  => 'wd-gallery',
                2 => array(
                    'number' => 5,
                    'orderby' => self::$orderBy,
                    'display' => self::$display,
                    'category' => [],
                ),
                '_multiwidget' => 1,
            ),
        ];
        return $init;
    }

    public static function getAward($limit = 6) {
        return self::where('status', '>', 1)->orderBy('sort', 'desc')->limit($limit)->get();
    }


}