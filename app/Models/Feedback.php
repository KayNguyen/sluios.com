<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class Feedback extends Model
{
    //
    protected $table = 'feedback';
    public $timestamps = false;

    public function getImageUrl($size = 'original'){
        return \ImageURL::getImageUrl($this->image, 'feedback', $size);
    }

    public static function getAllFeedback() {
        return self::where('status', '>', 1)->get();
    }


}