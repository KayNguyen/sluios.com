<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class Orders extends Model
{
    //
    protected $table = 'orders';
    public $timestamps = false;
    const NUM_KEY = 1;

    public function service(){
        return $this->hasOne('App\Models\Service', 'id', 'option');
    }
}