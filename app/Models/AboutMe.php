<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AboutMe extends Model
{
    //
    protected $table = 'aboutme';
    public $timestamps = false;
    
    public function getImageUrl($size = 'original'){
        return \ImageURL::getImageUrl($this->image, 'me', $size);
    }
}
