<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    //
    protected $table = 'categories';
    public $timestamps = false;
    protected static $cat = [];
    protected static $splitKey = '_';
    protected static $type = [
        // '1' => 'Sản phẩm',
        '2' => 'Tin tức',
        '3' => 'Videos',
        '4' => 'Hình ảnh',
    ];


    public function lang(){
        $lang = config('app.locales');
        return isset($lang[$this->lang]) ? $lang[$this->lang] : 'vi';
    }

    public function type(){
        return isset(self::$type[$this->type]) ? self::$type[$this->type] : '1';
    }

    public function news(){
        return $this->belongsToMany(News::class, 'news_categories', 'cate_id', 'new_id');
    }
    
    public static function getNews4Cate(){
        $cate = self::with(['news' => function ($q) {
            $q->selectRaw('alias, title, image, published')->limit(8);
        }])->where('status', '>', '-1')->select('id','title')->get()->keyBy('id')->toArray();
        if($cate) {
            return $cate;
        }
        // dd($cat_id, $cate);
    }

    public function getImageUrl($size = 'medium'){
        return \ImageURL::getImageUrl($this->image, 'category', $size);
    }

    public function link(){
        switch($this->type){
            case 1:
                return '';
            case 2:
                return '';
        }
        return '';
        //return route('product.list', ['safe_title' => str_slug($this->name), 'id' => $this->id]);
    }

    public static function getType(){
        return self::$type;
    }

    public static function getCateByID($id, $safe_title){
        $title = self::where([['id', $id], ['safe_title', $safe_title]])->value('title');
        if($title) {
            return $title;
        }
        return false;
    }

    public static function getCat($type = 0, $lang = '', $imgSize = ''){
        if(empty($lang)){
            $lang = \Lib::getDefaultLang();
        }
        $key = $type . '-' . $lang;
        if(empty(self::$cat[$key])) {

            $sql = [];
            if ($type > 0) {
                $sql[] = ['type', '=', $type];
            }
            $sql[] = ['lang', '=', $lang];
            $sql[] = ['status', '>', 0];

            $data = self::where($sql)
                ->orderByRaw('type, pid, sort DESC, title')
                ->get()
                ->keyBy('id');
            $cat = [];
            if ($type <= 0) {
                foreach ($data as $k => $v) {
                    if(isset(self::$type[$v->type])) {
                        if (!isset($cat[$v->type])) {
                            $cat[$v->type] = [
                                'title' => self::$type[$v->type],
                                'type' => $v->type,
                                'cats' => []
                            ];
                        }
                        $cat[$v->type]['cats'][$v->id] = $v;
                    }
                }
                foreach ($cat as $k => $v){
                    $cat[$k]['cats'] = self::fetchAll($v['cats'], $imgSize);
                }
            } else {
                $cat = self::fetchAll($data, $imgSize);

            }
            self::$cat[$key] = $cat;
        }
        return self::$cat[$key];
    }

    public static function fetchAll($data, $imgSize = ''){
        $cat = [];
        foreach ($data as $k => $v) {
            if ($v->pid == 0) {
                $cat[self::$splitKey.$v->id] = self::fetchCat($v, $imgSize);
                unset($data[$k]);
            } elseif (isset($cat[self::$splitKey.$v->pid])) {
                $cat[self::$splitKey.$v->pid]['sub'][self::$splitKey.$v->id] = self::fetchCat($v,$imgSize);
                unset($data[$k]);
            }
        }
        foreach ($data as $v) {
            foreach ($cat as $pid => $item){
                foreach ($item['sub'] as $id => $sub){
                    if(self::$splitKey.$v->pid == $id){
                        $cat[$pid]['sub'][$id]['sub'][self::$splitKey.$v->id] = self::fetchCat($v,$imgSize);
                    }
                }
            }
        }
        return $cat;
    }

    public static function fetchCat($cat, $imgSize = ''){
        $out = $cat->toArray();
        $out['image'] = $cat->getImageUrl($imgSize);
        $out['link'] = $cat->link();
        $out['sub'] = [];
        return $out;
    }
}
