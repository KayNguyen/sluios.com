<?php
\View::share('def', \Lib::tplShareGlobal());
\View::share('isHome', false);
\View::share('defLang', \Lib::getDefaultLang());
\View::share('menu', \Menu::getMenu(3));
\View::share('menu_footer', \Menu::getMenu(4));
// \View::share('category', \App\Models\Category::getCat());
\View::share('catNews', \App\Models\Category::getCat(2));
\View::share('news4Cat', \App\Models\Category::getNews4Cate());
\View::share('catVideos', \App\Models\Category::getCat(3));
\View::share('seoDefault', \App\Models\ConfigSite::getSeo());
$config = \App\Models\ConfigSite::getConfig('config', '');
\View::share('config', !empty($config) ? json_decode($config, true) : null);

// \View::share('facebook_shares', \App\Models\News::facebook_shares($url));