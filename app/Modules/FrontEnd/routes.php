<?php
Route::group(
    [
        'module' => 'FrontEnd',
        'namespace'=>'App\Modules\FrontEnd\Controllers',
        'middleware' => ['web']
    ],
    function(){
        //for ajax
        Route::any('ajax/{cmd}', [ 'as' => 'ajax', 'uses' => 'AjaxController@init']);

        //home
        Route::get('/', ['as' => 'home', 'uses' => 'HomeController@index']);

	    Route::get('category/detail', function (){
		    return view('FrontEnd::pages.category.detail', [
			    'site_title' => 'Chi tiết danh mục',
			    'menu' => \Menu::getMenu(3)
		    ]);
	    });
        Route::get('/search', ['as' => 'news.search', 'uses' => 'NewsController@search']);


        Route::get('login', ['as' => 'login', 'uses' => 'Auth\AuthController@showLoginForm']);
        Route::get('register', ['as' => 'register', 'uses' => 'Auth\AuthController@showRegisterForm']);
        Route::post('login', ['as' => 'login.post', 'uses' => 'Auth\AuthController@login']);
        Route::get('logout', ['as' => 'logout', 'uses' => 'Auth\AuthController@logout']);

        Route::group(['as' => 'videos.', 'prefix' => 'videos'], function() {


            Route::get('{p_cate?}/{child_cate?}',['as' => 'list', 'uses' =>'VideosController@index'])
                ->where([
                    'p_cate' => '[a-zA-Z0-9_\-]+',
                    'child_cate' => '[a-zA-Z0-9_\-]+',
                ]);
            Route::get('search/tag/{tag_title}', ['as' => 'search.tag', 'uses' => 'VideosController@searchTag']);

            Route::get('{alias}.htm', ['as' => 'detail', 'uses' => 'VideosController@detail'])->where('alias', '.*');

        });

        Route::group(['as' => 'profile.', 'prefix' => 'channel/{user}'], function() {
            Route::get('/', ['as' => 'home', 'uses' => 'ProfileController@index']);
            Route::post('email', ['as' => 'password.post', 'uses' =>'Auth\AuthController@sendResetLinkEmail']);
            Route::get('reset/{token}', ['as' => 'password.reset', 'uses' =>'Auth\AuthController@showPasswordResetForm']);
            Route::post('reset', ['as' => 'password.reset.post', 'uses' =>'Auth\AuthController@reset']);
        });

        Route::group(['as' => 'news.', 'prefix' => ''], function() {
            
            Route::get('tin/',['as' => '', 'uses' =>'NewsController@all' ]);

            Route::get('{p_cate?}/{child_cate?}',['as' => 'list', 'uses' =>'NewsController@index'])
                ->where([
                    'p_cate' => '[a-zA-Z0-9_\-]+',
                    'child_cate' => '[a-zA-Z0-9_\-]+',
                ]);
            Route::get('search/tag/{tag_title}', ['as' => 'search.tag', 'uses' => 'NewsController@searchTag']);

            Route::get('{alias}.htm', ['as' => 'detail', 'uses' => 'NewsController@detail'])->where('alias', '.*');

        });


        Route::get('email/{tpl}', ['as' => 'email', 'uses' => 'EmailController@showEmailTemplate']);

        Route::get('register/success', ['as' => 'register.success', 'uses' => 'Auth\AuthController@showRegSuccess']);
        Route::get('register/verify', ['as' => 'register.verify', 'uses' => 'Auth\AuthController@regVerify']);

        Route::get('feedback', ['as' => 'feedback', 'uses' => 'Auth\AuthController@showFeedback']);




        Route::group(['prefix' => 'password'], function() {
            Route::get('reset', ['as' => 'password', 'uses' => 'Auth\AuthController@showPasswordRequestForm']);
            Route::post('email', ['as' => 'password.post', 'uses' =>'Auth\AuthController@sendResetLinkEmail']);
            Route::get('reset/{token}', ['as' => 'password.reset', 'uses' =>'Auth\AuthController@showPasswordResetForm']);
            Route::post('reset', ['as' => 'password.reset.post', 'uses' =>'Auth\AuthController@reset']);
        });

        Route::group(['prefix' => 'auth'], function() {
            Route::get('facebook/redirect', ['as' => 'facebook.login', 'uses' => 'Auth\SocialAuthFacebookController@redirect']);
            Route::get('facebook/callback', ['as' => 'facebook.callback', 'uses' => 'Auth\SocialAuthFacebookController@callback']);

            Route::get('google/redirect', ['as' => 'google.login', 'uses' => 'Auth\SocialAuthGoogleController@redirect']);
            Route::get('google/callback', ['as' => 'google.callback', 'uses' => 'Auth\SocialAuthGoogleController@callback']);
        });

        //set language
        Route::get('language/{lang}', ['as' => 'language', 'uses' => 'LanguageController@change']);
        Route::get('js/lang.js', ['as' => 'assets.lang', 'uses' => 'LanguageController@getJson']);

        //error
        Route::get('404', function () {
            return abort(404);
        });
        Route::get('500', function () {
            return abort(500);
        });

    }
);

