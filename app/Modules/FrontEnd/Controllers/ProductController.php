<?php


namespace App\Modules\FrontEnd\Controllers;

use App\Models\Category;
use App\Models\Feature;
use App\Models\Menu;
use App\Models\News;
use App\Models\Product;
use App\Models\ProductImage;

class ProductController
{
    public function __construct(){
        \Lib::addBreadcrumb();
    }

    public function index(){

        $product = Product::getProductAll();
        $product_hot = Product::getProductHot();

        foreach ($product as $item) {
            $data[$item->category->title] = Product::getProductByCate($item->cat_id, 8);
        }
        $data['hot'] = $product_hot;
        $cate = Menu::where('link', 'product.list')->first();
        if($data && $cate) {
            return view('FrontEnd::pages.product.index', [
                'data' => $data,
                'slide' => Feature::getSlides($cate->id),
            ]);
        }
        return abort(404);
    }

    public function listCate($action_name = ''){
        if ($action_name) {
            $cate = Category::where('safe_title', $action_name)->where('type', 1)->first();
            if ($cate) {
                $product = Product::getProductByCate($cate->id);
                $product_hot = Product::getProductHot();
                foreach ($product as $item) {
                    $data[$item->category->title] = Product::getProductByCate($item->cat_id, 8);
                }
                $data['hot'] = $product_hot;
                if($data) {
                    return view('FrontEnd::pages.product.index', [
                        'data' => $data,
                        'slide' => Feature::getSlidesCate($cate->id),
                    ]);
                }
            }
        }

        return abort(404);
    }
    public function detail($alias){
        $data = Product::where('alias', $alias)->where('status', '>', 1)->first();
        $lang = \Lib::getDefaultLang();
        if(!empty($data)) {
            set_old($data);
            Product::savePrdAfterView($data->id);
            $prd_history = Product::prdHistory(8);
            $images = ProductImage::where('object_id', $data->id)->get();
            \Lib::addBreadcrumb($data->category->title);
            return view('FrontEnd::pages.product.detail', [
                'site_title' => $data->title,
                'data' => $data,
                'images' => $images,
                'prd_history' => $prd_history,

            ]);
        }
        return abort(404);
    }

}