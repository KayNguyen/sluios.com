<?php


namespace App\Modules\FrontEnd\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use App\Models\Menu;
use App\Models\Feature;

use App\Models\Service as THIS;

class ServiceController extends Controller
{

    public function __construct(){
        \Lib::addBreadcrumb();
    }

    public function index(){
        $data = THIS::getAllServices();
        $cate = Menu::where('link', 'service.list')->first();
        if($data && $cate) {
            return view('FrontEnd::pages.service.index', [
                'data' => $data,
                'slide' => Feature::getSlides($cate->id),
            ]);
        }
        return abort(404);
    }

    public function book(){
        $data = THIS::getAllServices();
        $jenny = [];
        if (\request()->input('service')) {
            $jenny = json_encode(\request()->input('service'));

        }
        if($data) {
            return view('FrontEnd::pages.service.book', [
                'data' => $data,
                'jenny' => $jenny
            ]);
        }
        return abort(404);
    }



}