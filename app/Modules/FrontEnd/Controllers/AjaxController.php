<?php

namespace App\Modules\FrontEnd\Controllers;

use App\Models\Author;
use App\Models\Customer;
use App\Models\News;
use App\Models\NewsDetail;
use App\Models\Process;
use App\Models\Orders;
use App\Models\Service;
use App\Models\Tag;
use App\Models\UploadFile;
use App\Models\BonusLog;
use App\Models\TagDetail;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

//custom models
use App\Models\Subscriber;


class AjaxController extends Controller
{
    public function __construct()
    {
        //
    }

    public function init(Request $request, $cmd){
        $data = [];
        switch ($cmd) {
            case 'book':
                $data = $this->book($request);
                break;
            case 'load-process':
                $data = $this->loadProcess($request);
                break;
            case 'login':
                $data = $this->login($request);
                break;
            case 'register':
                $data = $this->register($request);
                break;
            case 'email-active':
                $data = $this->emailActive($request);
                break;
            case 'subscribe':
                $data = $this->subscribe($request);
                break;
            case 'route':
                $data = $this->updateRoute($request);
                break;
            case 'vote-add':
                $data = $this->updateVote($request);
                break;
            case 'update-views':
                $data = $this->updateViews($request);
                break;
            case 'tag-suggest':
                $data = $this->ajaxLoadSuggestTag($request);
                break;
            case 'tag-add':
                $data = $this->ajaxAddTag($request);
                break;
            case 'tag-del':
                $data = $this->ajaxRemoveTag($request);
                break;
            case 'change-password':
                $data = $this->changePassword($request);
                break;
            case 'upload-file':
                $data = $this->ajaxUploadFile($request);
                break;
            default:
                $data = $this->nothing();
        }
        return response()->json($data);
    }

    public function changePassword(Request $request){
        $validate = \Validator::make(
            $request->all(),
            [
                'newPassword' => 'required|min:8|regex:/^.*(?=.{3,})(?=.*[a-zA-Z])(?=.*[0-9])(?=.*[\d\X])(?=.*[!$#%@]).*$/',
            ],
            [
                'newPassword.required' => 'Chưa nhập mật khẩu',
                'newPassword.min' => 'Mật khẩu phải có từ 8 kí tự trở lên',
                'newPassword.regex' => 'Mật khẩu phải bao gồm chữ, số và kí tự đặc biệt (!, $, #, %, @)',
            ]
        );
        if ($validate->fails()) {
            return \Lib::ajaxRespond(false, 'error', $validate->errors()->all());
        }elseif(\Lib::isWeakPassword($request->password)){
            return \Lib::ajaxRespond(false, 'error', ['Mật khẩu quá yếu']);
        }
        if (!(\Hash::check($request->oldPassword, \Auth::guard('customer')->user()->password))){
            return \Lib::ajaxRespond(false, 'error', ['Mật khẩu hiện tại không đúng']);
        }

        $change = Customer::changePass($request);
        if (!$change) {
            return \Lib::ajaxRespond(false, 'error', ['Có lỗi xảy ra, vui lòng liên hệ admin để xử lý!']);
        }else {
            return \Lib::ajaxRespond(true, 'success', ['url' => route('logout')]);
        }
    }

    public function ajaxUploadFile(Request $request){
        if ($request->hasFile('Filedata')) {
            $image = $request->file('Filedata');
            if ($image->isValid()) {
                $key = 'file';
                $err = '';
                $fname = basename($image->getClientOriginalName(), '.'.$image->getClientOriginalExtension());
                $fname = \ImageURL::makeFileName($fname, $image->getClientOriginalExtension());
                $type = $image->getClientMimeType();
                $size = $image->getClientSize();
                $image = \ImageURL::upload($image, $fname, $key, $err);
                if($image){
                    $images = new UploadFile();
                    $images->name = $fname;
                    $images->size = $size;
                    $images->type = $type;
                    $images->created = time();
                    $images->save();

                    \MyLog::do()->add('upload', $images->id, $images);

                    return \Lib::ajaxRespond(true, 'ok', [
                        'id' => $images->id,
                        'name' => $images->name,
                        'image' => $images->getImageUrl('large'),
                        'image_sm' => $images->getImageUrl('small')
                    ]);
                }
                return \Lib::ajaxRespond(false, 'Upload ảnh thất bại! '.$err);
            }
            return \Lib::ajaxRespond(false, 'File không hợp lệ!');
        }
        return \Lib::ajaxRespond(false, 'Không tìm thấy ảnh!');
    }

    public function ajaxLoadSuggestTag(Request $request){
        if(!empty($request->type)) {
            $tags = Tag::getTags($request->type);
            $data = [];
            foreach ($tags as $item){
                $data[] = $item['title'];
            }
            return \Lib::ajaxRespond(true, 'success', $data);
        }
        return \Lib::ajaxRespond(false, 'Không tìm thấy dữ liệu');
    }

    public function ajaxAddTag(Request $request){
        if (!empty($request->tag)) {
            $safe_title = str_slug($request->tag);
            $tag = Tag::where([
                ['safe_title', '=', $safe_title],
                ['type', '=', $request->type],
            ])->first();
            if (empty($tag)) {
                $tag = new Tag();
                $tag->title = $request->tag;
                $tag->safe_title = $safe_title;
                $tag->type = $request->type;
                $tag->created = time();
                $tag->save();
                \MyLog::do()->add('tag-add', $tag->id, $tag);
            }
            return \Lib::ajaxRespond(true, 'success', $tag->id);
        }
        return \Lib::ajaxRespond(false, 'Không tìm thấy dữ liệu');
    }

    public function ajaxRemoveTag(Request $request){
        if (!empty($request->tag)) {
            $safe_title = str_slug($request->tag);
            $tag = Tag::where([
                ['safe_title', '=', $safe_title],
                ['type', '=', $request->type]
            ])->first();
            if (!empty($tag)) {
                switch ($request->type) {
                    case 1:
                        if ($request->id > 0) {
                            TagDetail::where([
                                ['tag_id', '=', $tag->id],
                                ['news_id', '=', $request->id]
                            ])->delete();
                        }
                        //kiem tra neu ko trong che do su dung thi xoa
                        $check = TagDetail::where([
                            ['tag_id', '=', $tag->id]
                        ])->first();
                        if (empty($check)) {
                            $tag->delete();
                            \MyLog::do()->add('tag-remove', $tag->id);
                        }
                        break;
                }
            }
            return \Lib::ajaxRespond(true, 'success', $tag->id);
        }
        return \Lib::ajaxRespond(false, 'Không tìm thấy dữ liệu');
    }

    public function updateRoute(){
        $routes = \Lib::saveRoutes();
        return \Lib::ajaxRespond(true, 'ok', $routes);
    }

    public function subscribe(Request $request){
        $subscriber = Subscriber::where('email', $request->email)->first();
        if(empty($subscriber)){
            $subscriber = new Subscriber();
            $subscriber->email = $request->email;
            $subscriber->created = time();
        }
        $customer = Customer::where('email', $request->email)->first();
        if(!empty($customer)){
            $subscriber->customer_id = $customer->id;
        }
        $subscriber->save();
        return \Lib::ajaxRespond(true, __('site.camonbandadangkyyeucaucuabandaduocghinhan'));
    }

    public function updateVote(Request $request){
        $user = \Auth::guard('customer')->user()->id;
        $vote = NewsDetail::where('id', $request->id)->value('vote');
        $user_vote_new_id = CustomerVote::where([['vote_id', $request->id], ['user_id', $user]])->first();
        if(empty($user_vote_new_id)) {
            // ô chưa vote thì vô
            $user_vote_new_id = CustomerVote::insertGetId([
                'user_id' => $user,
                'vote_id' => $request->id,
                'type' => $request->type_id,
                'created' => time(),
            ]);
            $vote_id =  NewsDetail::where([['id', $request->id], ['type', $request->type_id]])->update(['vote' => ($vote + 1)]);
            $allvote_id = News::getVoteById($request->new_id);
            $customer_id = Author::getAuthorByNewsId($request->new_id);
            if ($customer_id) {
                $customer = Customer::where('id', $customer_id->id)->first();
//                dd($customer);
                if ($allvote_id == 50) {
                    $bonus_log = new BonusLog();
                    $bonus_log->bonus = 20000;
                    $bonus_log->new_id = $request->new_id;
                    $bonus_log->created = time();
                    $bonus_log->vote = $allvote_id;
                    $bonus_log->save();
                }
                $vote_bonus_log = \DB::table('bonus_log')->where('new_id', $request->new_id)->orderBy('created', 'DESC')->first();
                if ($vote_bonus_log) {
                    if((int)$allvote_id - $vote_bonus_log->vote == 50 || $allvote_id == 50) {
                        $customer->money = $customer->money + 20000.0;
                        $customer->save();
                        //
                        $bonus_log = new BonusLog();
                        $bonus_log->bonus = 20000.0;
                        $bonus_log->new_id = $request->new_id;
                        $bonus_log->created = time();
                        $bonus_log->vote = $allvote_id;
                        $bonus_log->user_id = $customer->id;
                        $bonus_log->save();
                    }
                }
            }
        }
    }

    public function updateViews(Request $request){
        // $views = Statistics::where('id', $request->id)->value('vote');
        // $user_vote_new_id = CustomerVote::where([['vote_id', $request->id], ['user_id', $user]])->first();
        // if(empty($user_vote_new_id)) {
        //     // ô chưa vote thì vô
        //     $user_vote_new_id = CustomerVote::insertGetId([
        //         'user_id' => $user,
        //         'vote_id' => $request->id,
        //     ]);
        //     return NewsDetail::where('id', $request->id)->update(['vote' => ($vote + 1)]);
        // }
        

    }


    public function emailActive(Request $request){
        $customer = Customer::find($request->id);
        if($customer){
            $customer->token->newToken();

            event('customer.register.resend', $customer->id);

            return \Lib::ajaxRespond(true, 'success');
        }
        return \Lib::ajaxRespond(false, 'Người dùng không tồn tại hoặc đã bị khóa');
    }

    public function register(Request $request){
        $validate = \Validator::make(
            $request->all(),
            [
                'email' => 'required|email',
                'password' => 'required|min:8|confirmed'
            ],
            [
                'email.required' => 'Chưa nhập Email',
                'email.email' => 'Email không hợp lệ',
                'password.required' => 'Chưa nhập mật khẩu',
                'password.min' => 'Mật khẩu phải có từ 8 kí tự trở lên',
                'password.confirmed' => 'Xác nhận mật khẩu sai',
            ]
        );
        if ($validate->fails()) {
            return \Lib::ajaxRespond(false, 'error', $validate->errors()->all());
        }elseif(\Lib::isWeakPassword($request->password)){
            return \Lib::ajaxRespond(false, 'error', ['Mật khẩu quá yếu']);
        }

        $customer = Customer::where('email', $request->email)->first();
        if($customer){
            return \Lib::ajaxRespond(false, 'error', 'EXISTED');
        }
        $customer = Customer::createOne($request);

        //gui email
        event( "customer.register", $customer->id);
        return \Lib::ajaxRespond(true, 'success', ['url' => route('register.success').'?email='.$customer->email]);
    }

    public function login(Request $request){
        if(!\Auth::guard('customer')->check()){
            $username = $request->userOrEmail;
            $password = $request->password;

            $customer = Customer::where('user_name', $username)->first();
            if(filter_var($username, FILTER_VALIDATE_EMAIL)) {
                $customer = Customer::where('email', $username)->first();
            }

            if(!empty($customer)) {
                if($customer->active > 0) {
                    if($customer->status > 0) {
                        if(filter_var($username, FILTER_VALIDATE_EMAIL)) {
                            if (\Auth::guard('customer')->attempt(['email' => $username, 'password' => $password])) {
                                $request->session()->regenerate();
                                return \Lib::ajaxRespond(true, 'success', ['url' => route('home')]);
                            }
                        }else {
                            if (\Auth::guard('customer')->attempt(['user_name' => $username, 'password' => $password])) {
                                $request->session()->regenerate();
                                return \Lib::ajaxRespond(true, 'success', ['url' => route('home')]);
                            }
                        }
                        return \Lib::ajaxRespond(false, 'error', 'LOGIN_FAIL');
                    }
                    return \Lib::ajaxRespond(false, 'error', 'BANNED');
                }
                return \Lib::ajaxRespond(false, 'error', 'NOT_ACTIVE');
            }
            return \Lib::ajaxRespond(false, 'error', 'NOT_EXISTED');
        }
        return \Lib::ajaxRespond(false, 'error', 'LOGINED');
    }

    public function book(Request $request){
        if ($request->phone && $request->user_name && $request->address) {
            $id = Orders::insertGetId([
                'address'   => $request->address,
                'coupon'   => (isset($request->coupon)) ? $request->coupon : null,
                'email'   => (isset($request->email)) ? $request->email : null,
                'opt_detail'   => (isset($request->opt_detail)) ? $request->opt_detail : null,
                'phone'     => $request->phone,
                'option'     => $request->option,
                'name'      => $request->user_name,
                'status'      => 1,     // 1: đơn mới 2: đã xác nhận 3: Đang lấy hàng 4: Đang xử lý 5: Đang giao hàng 6: Hoàn thành
                'created'   => time(),
            ]);
            if ($id) {
                return \Lib::ajaxRespond(true, 'success');
            }
            return abort(403);
        }
        return \Lib::ajaxRespond(false, 'error', 'BOOK_FAIL');
    }

    public function loadProcess(Request $request){
        if ($request->id > 0 && $request->id) {
            $data = Process::find($request->id);
            $aja['image'] = $data->getImageUrl($data->process_image, 'process_image');
            $aja['body'] = $data->body;
            return \Lib::ajaxRespond(true, 'success', $aja);

        }
        return \Lib::ajaxRespond(false, 'error', 'BOOK_FAIL');
    }

    public function nothing(){
        return "Nothing...";
    }
}
