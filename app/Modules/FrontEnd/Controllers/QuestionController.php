<?php


namespace App\Modules\FrontEnd\Controllers;

use App\Models\QuestionAnswer;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;

class QuestionController extends Controller
{

    public function _saveQuestion(Request $request){

        $name = $request->_name;
        $phone = $request->_phone;
        $content = $request->_content;
        if (\Auth::guard('customer')->check()) {
            $user_id = \Auth::guard('customer')->user()->id;
        }
        $valid = [
            '_name' => 'required|max:50',
            '_phone' => 'required|numeric|regex:/(0)[0-9]{10}/',
            '_content' => 'required',
        ];
        $messages = [
            '_name.required' => 'Bạn cần nhập tiêu đề bài viết',
            '_phone.required' => 'Bạn cần nhập số điện thoại',
            '_phone.numeric' => 'Số điện thoại không hợp lệ',
            '_phone.regex' => 'Số điện thoại không hợp lệ',
            '_content.required' => 'Nội dung không được để trống',
            '_name.max' => 'Tên bạn quá dài, tối đa 50 ký tự',
            '_phone.max' => 'Số điện thoại không hợp lệ',
        ];


        $validator = Validator::make($request->all(), $valid, $messages);
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }
        if($name && $phone && $content) {
            $id = QuestionAnswer::insertGetId([
                'content'   => strip_tags($content),
                'user_id'   => (isset($user_id)) ? $user_id : null,
                'phone'     => $phone,
                'name'      => strip_tags($name),
                'created'   => time(),
            ]);
            if ($id) {
                return back();
            }
            return abort(403);
        }
    }

}