<?php

namespace App\Modules\FrontEnd\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Feedback;
use App\Models\Video;
use App\Models\News;
use App\Models\Menu;
use App\Models\Product;
use App\Models\Category;
use App\Models\Feature;
use Youtube;

class HomeController extends Controller
{   //
    public function __construct(){

    }

    public function index(){
        view()->share('isHome', true);
        $pagi = 6;
        $order = 'created DESC, id DESC';
        $li_video = Video::with('category')->where('status', '>', 1)->select('videos_id', 'cate_par')->orderByRaw($order)->limit(20)->get()->keyBy('videos_id')->toArray();
        foreach($li_video as $item) {
            $vidarr[] = $item['videos_id'];
        }
        $lang = \Lib::getDefaultLang();
        $videos = Youtube::getVideoInfo($vidarr);
        $li_news = News::getListNew($lang, 3);
        $li_news_latest = News::getLatestPost4AllCate();
        $prd_history = News::prdHistory(8);
        foreach($li_news as $k => $item) {
            $li_news[$k]['related'] = News::getRelated($lang, 4, $item['id']);
        }
//         dd($prd_history->toArray());
        return view('FrontEnd::pages.home.index', [
            'site_title' => 'Kênh tin tức - video giải trí',
            'videos' => $videos,
            'rootCVideo' => $li_video,
            'news' => $li_news,
            'li_news_history' => $prd_history,
            'li_news_latest' => $li_news_latest,
        ]);
    }

}
