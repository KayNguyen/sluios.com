<?php

namespace App\Modules\FrontEnd\Controllers;

use App\Models\Category;
use App\Models\Customer;
use App\Models\QuestionAnswer;
use App\Models\NewsCate;
use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use App\Models\News;
use App\Models\LogCheck;
use App\Models\Tag;
use Illuminate\Support\Facades\Auth;
use Validator;
use App\Libs\ImageURL;
class ProfileController extends Controller
{   //
    public function __construct($action = ''){
        \View::share('tagType', 1);
        \View::share('langOpt', config('app.locales'));
        \View::share('catOpt', Category::getCat(2));
        $this->middleware('auth:customer');
    }

    public function index(Request $request){

        $user = \Auth::guard('customer')->user();
        $cond = [];
        $order = 'created DESC, id DESC';

        // $history = LogCheck::where('user_id', $user->id)->orderBy('created', 'DESC')->get();
        return view('FrontEnd::pages.profile.index', [
            'site_title' => $user->user_name,
            'data' => $user,
        ]);
    }

    public function listNews(){
        $user = \Auth::guard('customer')->user();
        if ($user->account_type == 1 && \Auth::guard('customer')->check() ) {
            $total_money = $user->money;
            $all_new = Author::getAllNewsByAuthor($user->id, 8);
            return view('FrontEnd::pages.profile.list-my-news', [
                'site_title' => 'Bài viết của tôi',
                'data' => $all_new,
            ]);
        }
        return abort(404);

    }
    
    public function postNews(){
        if (\Auth::guard('customer')->user()->account_type == 1 && \Auth::guard('customer')->check() ) {
            return view('FrontEnd::pages.profile.add', [
                'site_title' => 'Trang cá nhân',
                'data' => \Auth::guard('customer')->user(),
            ]);
        }
        return abort(404);

    }

    public function wallet(){
        $user = \Auth::guard('customer')->user();
        if ($user->account_type == 1 && \Auth::guard('customer')->check() ) {
            $total_money = $user->money;
            $all_log_request_money = RoyaltyPaymentLog::getAllLogByIDAuthor($user->id, 6);
            $all_log_bonus = RoyaltyPaymentLog::getAllLogBonusByIDNews($user->id, 6);
            return view('FrontEnd::pages.profile.wallet', [
                'site_title' => 'Ví tiền của bạn',
                'total_money' => $total_money,
                'all_log_payment' => $all_log_request_money,
                'all_log_bonus' => $all_log_bonus,
                'data' => \Auth::guard('customer')->user(),
            ]);
        }
        return abort(404);

    }

    public function historyReceiveMoney(){
        $user = \Auth::guard('customer')->user();
        if ($user->account_type == 1 && \Auth::guard('customer')->check() ) {
            $total_money = $user->money;
            $all_log_history = ReceiveMoneyLog::getAllLogReceiveMoneyByIDAuthor($user->id, 6);
//            dd($all_log_request_money);
            return view('FrontEnd::pages.profile.history-receive', [
                'site_title' => 'Lịch sử chuyển tiền',
                'data' => $all_log_history,
            ]);
        }
        return abort(404);

    }

    public function requestMoney(){
        $user = \Auth::guard('customer')->user();
        if ($user->account_type == 1 && \Auth::guard('customer')->check() ) {
            $request_money = \request('money');
            if ($request_money < $user->money && $request_money > 0) {
                RequestMoney::where([['user_id', $user->id], ['status', 1]])->delete();

                $req = new RequestMoney();
                $req->user_id = $user->id;
                $req->money = $request_money;
                $req->created = time();
                $req->status = 1;
                $req->save();
                // ghi log
                $log = new RequestMoneyLog();
                $log->user_id = $user->id;
                $log->money = $request_money;
                $log->created = time();
                $log->save();
                return back()->with('success', 'Yêu cầu của bạn đã được gửi đến Admin, Xin vui lòng đợi Admin xác nhận chuyển tiền!');
            }else {
                return back()->with('error', 'Số tiền bạn yêu cầu không hợp lệ!');
            }

//            $user->save
        }
        return abort(404);

    }

    public function savePost(Request $request){
        if (\Auth::guard('customer')->user()->account_type == 1 && \Auth::guard('customer')->check() ) {
            $valid = [
                'title' => 'required|max:250',
                'title_seo' => 'max:250',
                'cat_id' => 'required',
                'sort_body' => 'required',
                'body.*' => 'required|min:50',
            ];
            $messages = [
                    'title.required' => 'Bạn cần nhập tiêu đề bài viết',
                    'cat_id.required' => 'Chưa chọn danh mục bài viết',
                    'sort_body.required' => 'Mô tả ngắn không được để trống',
                    'body.required' => 'Nội dung chính không được để trống',
                    'title_seo.max' => 'Tiều đề seo quá dài, tối đa 250 ký tự',
                    'title.max' => 'Tiều đề bài viết quá dài, tối đa 250 ký tự',
                    'body.min' => 'Nội dung chính quá ngắn, tối thiểu 50 ký tự',
                ];


            $validator = Validator::make($request->all(), $valid, $messages);
            if ($validator->fails()) {
                return redirect()->back()->withErrors($validator)->withInput();
            }
            else{
                if(!$request->id) {
                    //thêm mới
                    $save = [
                        'title' => $request->title,
                        'title_seo' => !empty($request->title_seo) ? $request->title_seo : $request->title,
                        'published' => \Lib::getTimestampFromVNDate($request->published),
                        'sort_body' => $request->sort_body,
                        'image' => $this->uploadImg($request),
                        'n_view' => 0,
                        'status' => 1,
                        'created' => time(),
                        'lang' => 'vi',
                        'cat_id' => null,
                    ];
                    $id = News::insertGetId($save);
                    $body = $request->body;
                    // dd($request->id);
                    Author::insertOneObj($id, 2);
                    foreach ($body as $key => $value) {
                        $detail = new NewsDetail;
                        $detail->new_id = $id;
                        $detail->body = $value;
                        $detail->vote = 0;
                        $detail->type = 1;
                        $detail->save();
                    }
                    if ($request->cat_id) {
                        foreach ($request->cat_id as $cat) {
                            $wery = new NewsCate();
                            $wery->new_id = $id;
                            $wery->cat_id = $cat;
                            $wery->save();
                        }
                    }
                    if(!empty($request->tags)) {
                        Tag::addTags($request->tags, 1, $id);
                    }

                    return redirect()->route('post.news')->with('success','Thành công') ;
                }else {
                    //edit
                    $save = [
                        'title' => $request->title,
                        'title_seo' => !empty($request->title_seo) ? $request->title_seo : $request->title,
                        'published' => \Lib::getTimestampFromVNDate($request->published),
                        'sort_body' => $request->sort_body,
                        'image' => $this->uploadImg($request),
                        'lang' => $request->lang,
                        'cat_id' => null,
                    ];
//                    dd($request->id);
                    News::where('id', $request->id)->update($save);
                    Tag::addTags($request->tags, 1, $request->id);
                    NewsDetail::addContentById($request->id, $request->body, 1);
                    NewsCate::addCateById($request->id, $request->cat_id);
                    return redirect()->route('my.profile')->with('success','Thành công') ;
                }
            }
        }
    }

    public function editNews(Request $request){
        if (\Auth::guard('customer')->user()->account_type == 1 && \Auth::guard('customer')->check() ) {
            $id = $request->id;
            $tags = Tag::getNewsTags($id);
            if(!empty($tags)){
                $tmp = [];
                foreach ($tags as $item){
                    $tmp[] = $item->title;
                }
                $tags = implode(',', $tmp);
            }else{
                $tags = '';
            }
            $news = News::with(['detail', 'category'])->find($id);
            return view('FrontEnd::pages.profile.edit', [
                'site_title' => 'Chỉnh sửa bài viết',
                'data' => $news,
                'tags' => $tags
            ]);
        }
        return abort(404);

    }



    public function update(Request $request){
        $customer = Auth::guard('customer')->user();
        $valid = [
            'fullname' => 'required',
            'phone' => 'required|numeric|unique:customers,phone,'.$customer->id,
        ];
        if($request->new_password != '') {
            $valid['password'] = 'required';
            $valid['new_password'] = 'required|min:6';
            $valid['new_password_confirm'] = 'required|same:new_password';
        }

        $this->validate($request, $valid,
            [
                'fullname.required' => 'Chưa nhập họ tên',
                'phone.required' => 'Chưa nhập số điện thoại',
                'phone.numeric' => 'Số điện thoại chỉ chấp nhận chữ số',
                'phone.unique' => 'Số điện thoại đã được sử dụng',
                'password.required' => 'Chưa nhập Mật khẩu cũ',
                'new_password.required' => 'Chưa nhập Mật khẩu mới',
                'new_password.min' => 'Mật khẩu phải có từ 6 kí tự trở lên',
                'new_password_confirm.required' => 'Chưa nhập lại Mật khẩu mới',
                'new_password_confirm.same' => 'Nhập lại Mật khẩu không khớp',
            ]
        );

        $customer->fullname = $request->fullname;
        $customer->phone = $request->phone;

        if($request->new_password != '') {
            $customer->password = bcrypt($request->new_password);
        }

        //process image
        if ($request->hasFile('image')) {
            $image = $request->file('image');
            if ($image->isValid()) {
                $fname = \ImageURL::makeFileName($request->fullname, $image->getClientOriginalExtension());
                $image = \ImageURL::upload($image, $fname, 'avatar');
                if($image){
                    $customer->avatar = $fname;
                }else{
                    redirect()->back()->withInput()->withErrors(['image' => 'Upload ảnh lên server thất bại!']);
                }
            }else{
                redirect()->back()->withInput()->withErrors(['image' => 'Upload ảnh thất bại!']);
            }
        }
        $customer->save();
        return redirect()->route('profile')->with('status', 'Cập nhật thông tin thành công');
    }

    private function uploadImg(Request $request)
    {
        if ($request->hasFile('image')) {
            $image = $request->file('image');
            if ($image->isValid()) {
                $title = basename($image->getClientOriginalName(), '.'.$image->getClientOriginalExtension());
                $fname = ImageURL::makeFileName($title, $image->getClientOriginalExtension());
                $upload = ImageURL::upload($image, $fname, 'news');
                if($upload) {
                    return $fname;

                }
            }
        }
        return false;
    }

    public static function profileMenu($active = ''){
        $user = \Auth::guard('customer')->user();
        if($user->account_type == 0){
            return [

                [
                    'title' => 'Đăng xuất',
                    'link'  => route('logout'),
                    'icon'  => 'fa-sign-out',
                    'active'=> false
                ]
            ];
        }
        elseif($user->account_type == 1){
            return [
                [
                    'title' => 'Thông tin tài khoản',
                    'link'  => route('profile'),
                    'icon'  => 'fa-id-card',
                    'active'=> $active == 'profile'
                ],
                [
                    'title' => 'Bài viết của tôi',
                    'link'  => route('my.profile'),
                    'notice' => 1,
                    'icon'  => 'fa-list-ul',
                    'active'=> $active == 'my-profile'
                ],
                [
                    'title' => 'Quản lý ví tiền',
                    'link'  => route('money'),
                    'icon'  => 'fa-money',
                    'active'=> $active == 'connect_loan_appl'
                ],
                [
                    'title' => 'Lịch sử chuyển tiền',
                    'link'  => route('history.receive.money'),
                    'icon'  => 'fa-history',
                    'active'=> $active == 'connect_loan_appl'
                ],
                [
                    'title' => 'Đăng xuất',
                    'link'  => route('logout'),
                    'icon'  => 'fa-sign-out',
                    'active'=> false
                ]
            ];
        }

    }
    
}
