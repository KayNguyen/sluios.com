<?php

namespace App\Modules\FrontEnd\Controllers;

use App\Models\Category;
use App\Models\Menu;
use App\Models\Feature;
use App\Models\TagDetail;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cookie;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\CustomerVote;
use App\Models\NewsDetail;
use App\Models\Statistics;
use App\Models\Tag;
use App\Models\News as THIS;

class NewsController extends Controller
{   
    public function __construct(){
        \Lib::addBreadcrumb();
    }

    public function all(){

        $recperpage = 10;
        $where = [
            ['status', '>', 0],
            ['published', '>', 0],
            ['lang', '=', \Lib::getDefaultLang()],
        ];
        $cate = Menu::where('link', 'news.')->first();
        $data = THIS::with(['authors', 'category'])->where($where)->orderBy('published', 'desc')->paginate(10);
        if ($data) {
            \Lib::addBreadcrumb('Blogs');
            $slide = '';
            if($cate) {
                $slide = Feature::getSlides($cate->id);
            }
            return view('FrontEnd::pages.news.index', [
                'site_title' => 'Tin tức Blogs',
                'data' => $data,
                'slide' => $slide,
            ]);
        }
        return abort(404);

    }
    
    public function index($parent_cate, $child_cate = false){

        $recperpage = 10;
        if($child_cate) {
            $cate = Category::where('safe_title', $child_cate)->where('type', 2)->first();
            $child = true;
        }else {
            $cate = Category::where('safe_title', $parent_cate)->where('type', 2)->first();
            $child = false;
        }
        if ($cate) {
            $where = [
                ['status', '>', 0],
                ['published', '>', 0],
                ['lang', '=', \Lib::getDefaultLang()],
            ];
            $data = THIS::getAllNewsByCate($cate->id, 10, $child);
            $relCate = THIS::getRelatedCate($cate->id, 10);
            $listTrend = THIS::getlistTrend(10);
            return view('FrontEnd::pages.news.index', [
                'site_title' => $cate->title,
                'data' => $data,
                'relCate' => $relCate,
                'listTrend' => $listTrend,
            ]);
        }
        return abort(404);

    }

    
    
    public function viewMore($cate_news){
        $title = request('title');
        \Lib::addBreadcrumb($title);
        $recperpage = 8;
        // dd($safe_title);
        $data = THIS::where([
            ['status', '>', 1],
            ['cat_id', $id],
            ['published', '>', 0],
            ['lang', '=', \Lib::getDefaultLang()],
        ])
            ->orderBy('published', 'desc')
            ->paginate($recperpage);
        
        return view('FrontEnd::pages.news.index', [
            'site_title' => $title,
            'data' => $data,
            
        ]);
    }

    public function detail(Request $request, $alias){
        $news = THIS::with('authors')->where('alias', $alias)->where('status', '>', 1)->first();
        $lang = \Lib::getDefaultLang();
        // dd($news->toArray());
        if(!empty($news)) {
            THIS::updateViewNews($news->id);
            $related = THIS::getRelated($lang, 6, $news->id);
            THIS::savePrdAfterView($news->id);
            $prd_history = THIS::prdHistory(8);
            return view('FrontEnd::pages.news.detail', [
                'site_title' => $news->title,
                'data' => $news,
                'tags' => Tag::getNewsTags($news->id),
                'related' => $related,

            ]);
        }
        return abort(404);
    }




    public function indexPromotions(){
        return $this->index('promotions');
    }

    public function detailPromotions($safe_title, $id){
        return $this->detail($safe_title, $id, 'promotions');
    }

    public function indexBooks(){
        return $this->index('books');
    }

    public function detailBooks($safe_title, $id){
        return $this->detail($safe_title, $id, 'books');
    }

    public function _saveComment(Request $request) {
        $tpl = [];
        $obj = request('obj', []);
        $save = [
            'comment' => (isset($obj['comment']) && trim($obj['comment'])) ? $obj['comment'] : '',
            'uid' => Auth::guard('customer')->user()->id,
            'new_detail_id' => request('news_detail_id'),
            'new_id' => request('new_id'),
            'created' => time(),
            'status' => 2,
        ];
        $id = Comments::insertGetId($save);

        return back()->withInput();
    }

    public function search(Request $request){
        \Lib::addBreadcrumb('Tìm kiếm');

        $sort_by = $request->sort_by;
        $cat_id = $request->cat_id;
        $keyword = $request->q;

        $list_news = THIS::getByCate($cat_id,20, $keyword, $sort_by);
        if($list_news->total() > 0) {
            return view('FrontEnd::pages.news.search', [
                'site_title' => 'Tìm kiếm',
                'data' => $list_news,
                'keyword' => $keyword,
                'sort_by' => $sort_by,
            ]);
        }else {
            return view('FrontEnd::pages.news.search', [
                'site_title' => 'Tìm kiếm',
                'keyword' => $keyword,
                'sort_by' => $sort_by,
            ]);
        }
    }

    public function searchTag(Request $request){
        \Lib::addBreadcrumb('Tìm kiếm');

        $sort_by = $request->sort_by;
        $cat_id = $request->cat_id;
        $keyword = $request->tag_title;
        $tag = Tag::where('safe_title', str_slug($keyword))->first();

        if ($tag) {

            $id_tags = $tag->id;
            $keyword = $tag->title;
            $list_news = THIS::getAllNewsByTags($id_tags);
            if (count($list_news) > 0) {
                return view('FrontEnd::pages.news.search', [
                    'site_title' => 'Tìm kiếm',
                    'data' => $list_news,
                    'keyword' => $keyword,
                    'sort_by' => $sort_by,
                ]);
            } else {
                return view('FrontEnd::pages.news.search_notfound', [
                    'site_title' => 'Tìm kiếm',
                    'keyword' => $keyword,
                    'sort_by' => $sort_by,
                ]);
            }
        }
        return view('FrontEnd::pages.news.search_notfound', [
            'site_title' => 'Tìm kiếm',
            'keyword' => $keyword,
            'sort_by' => $sort_by,
        ]);
    }
    
}
