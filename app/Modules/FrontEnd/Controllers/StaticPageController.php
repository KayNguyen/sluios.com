<?php

namespace App\Modules\FrontEnd\Controllers;

use App\Http\Controllers\Controller;

use App\Models\Comments;
use App\Models\Page;
use App\Models\CustomerVote;
use App\Models\NewsDetail;


class StaticPageController extends Controller
{   //
    public function __construct(){

    }

    public function index($link_seo){
        $page = Page::where('link_seo', $link_seo)->first();
        if($page && $page->status == 2){
            return view('FrontEnd::pages.page.index', [
                'site_title' => $page->title_seo,
                'data' => $page,
            ]);
        }
        return redirect()->route('home');
    }
}
