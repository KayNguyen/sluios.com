<?php


namespace App\Modules\FrontEnd\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Tag;
use App\Models\Video as THIS;
use Illuminate\Http\Request;

class VideosController extends Controller
{
    public function __construct(){
        \Lib::addBreadcrumb();
    }

    public function index($parent_cate, $child_cate = false){

        $recperpage = 10;
        if($child_cate) {
            $cate = Category::where('safe_title', $child_cate)->where('type', 3)->first();
            $child = true;
        }else {
            $cate = Category::where('safe_title', $parent_cate)->where('type', 3)->first();
            $child = false;
        }
        if ($cate) {
            $where = [
                ['status', '>', 0],
                ['published', '>', 0],
                ['lang', '=', \Lib::getDefaultLang()],
            ];
            $data = THIS::getAllVideosByCate($cate->id, 10, $child);
            $relCate = THIS::getRelatedCate($cate->id, 10);
            return view('FrontEnd::pages.videos.index', [
                'site_title' => $cate->title,
                'data' => $data,
                'relCate' => $relCate,
            ]);
        }
        return abort(404);

    }

    public function detail(Request $request, $alias){
        $video = THIS::where([['videos_id', $alias], ['status', '>', 1]])->first();
        $lang = \Lib::getDefaultLang();
        $relCate = THIS::getRelatedCate($video->category->id, 10);
        $video = \Youtube::getVideoInfo($video->videos_id);
        if(!empty($video)) {
            return view('FrontEnd::pages.videos.detail', [
                'site_title' => $video->snippet->title,
                'data' => $video,
                'related' => $relCate,

            ]);
        }
        return abort(404);
    }

}