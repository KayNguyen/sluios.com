<?php


namespace App\Modules\FrontEnd\Controllers;


use App\Models\Feature;
use App\Models\Menu;
use App\Models\Partner;
use App\Models\Trust;

class PartnerController
{
    public function __construct(){
        \Lib::addBreadcrumb();
    }

    public function index(){

        $cate = Menu::where('link', 'partner')->first();
        if($cate) {
            return view('FrontEnd::pages.partner.index', [
                'site_title' => 'Đối tác',
                'trust' => Trust::getAllTrust(),
                'partner' => Partner::getAllPartner(),
                'slide' => Feature::getSlides($cate->id),
            ]);
        }
        return abort(404);
    }
}