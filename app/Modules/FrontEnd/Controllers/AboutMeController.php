<?php


namespace App\Modules\FrontEnd\Controllers;


use App\Http\Controllers\Controller;
use App\Models\Menu;
use App\Models\Feature;
use App\Models\AboutMe as THIS;

class AboutMeController extends Controller
{
    public function __construct(){

    }

    public function index(){
        $data = THIS::where('status', '>', 1)->get();
        $cate = Menu::where('link', 'aboutme')->first();
        if ($data && $cate) {
            return view('FrontEnd::pages.me.index', [
                'site_title' => 'Về chúng tôi',
                'data' => $data,
                'slide' => Feature::getSlides($cate->id),
            ]);

        }
        return abort(404);
        
    }
    
    public function glycemic(){
        $pagi = 6;
        $data = [];
        return view('FrontEnd::pages.journeys.history', [
            'site_title' => 'Hành trình đường huyết',
            'data' => $data,
            'news' => News::getAllNews(),
        ]);
    }
}