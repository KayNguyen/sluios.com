<?php


namespace App\Modules\FrontEnd\Controllers;
use App\Models\Process as THIS;
use App\Models\Feature;
use App\Models\Menu;
class ProcessController
{

    public function index(){
        $data = THIS::where('status', '>', 1)->select('id', 'title', 'image', 'process_image', 'sort_body', 'body')->get();
        $cate = Menu::where('link', 'process')->first();
        if ($data && $cate) {
            return view('FrontEnd::pages.process.index', [
                'site_title' => 'Quy trình',
                'data' => $data,
                'slide' => Feature::getSlides($cate->id),
            ]);

        }
        return abort(404);
    }

}