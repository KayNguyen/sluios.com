
<footer>
    <div class="page-footer">
        <div class="container tags">
            <div class="row">
                @foreach ($catNews as $item)
                    
                <div class="col-6 col-md-2 section-nav">
                    <h6 class="nav-title sp-line-1"><a href="{{ route('news.list', ['p_cate' => $item['safe_title']]) }}" title="{{ $item['title'] }}">{{ $item['title'] }}</a></h6>
                    <ul>
                        @foreach ($item['sub'] as $i_sub)
                            <li class="nav-item sp-line-1"><a href="{{ route('news.list', ['p_cate' => $item['safe_title'], 'child_cate' => $i_sub['safe_title']]) }}" title="{{ $i_sub['title'] }}">{{ $i_sub['title'] }}</a></li>
                        @endforeach
                    </ul>
                </div>
                @endforeach

            </div>
            <div class="directional row">
                <div class="container">
                    <div class="row">
                        <div class="col-md-8">
                            <div class="copyright">
                                <div class="row ne">
                                    <div class="branding">
                                        <img src="{{ asset('html/images/logo.png') }}" alt="">
                                    </div>
                                    <div class="legal">

                                        <ul>
                                            <li><a href="//www.foxnews.com/terms-of-use">Terms of Use</a></li>
                                            <li><a href="//www.foxnews.com/privacy-policy">Updated Privacy Policy</a></li>
                                            <li><a href="//www.foxnews.com/closed-captioning">Closed Captioning Policy</a></li>
                                            <li><a href="//help.foxnews.com">Help</a></li>
                                            <li><a href="//www.foxnews.com/contact">Contact Us</a></li>
                                        </ul>
                                        <div class="copyright">
                                            <span>© Copyright 2019. All rights reserved.</span>
                                            <span>flexnews.com</span>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="social-icons">
                                <ul>
                                    <li class="fb"><a href="//www.facebook.com/FoxNews">Facebook</a></li>
                                    <li class="tw"><a href="//twitter.com/foxnews">Twitter</a></li>
                                    <li class="ig"><a href="//www.instagram.com/foxnews">Instagram</a></li>
                                    <li class="email"><a href="//www.foxnews.com/newsletters">Email</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="fb-root"></div>
        <div class="back-to-top">
        <i class="fa fa-angle-double-up"></i></div>
    </div>
</footer>



<!-- Load Facebook SDK for JavaScript -->
      <div id="fb-root"></div>
      <script>
        window.fbAsyncInit = function() {
          FB.init({
            xfbml            : true,
            version          : 'v4.0'
          });
        };

        (function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = 'https://connect.facebook.net/vi_VN/sdk/xfbml.customerchat.js';
        fjs.parentNode.insertBefore(js, fjs);
      }(document, 'script', 'facebook-jssdk'));</script>

      <!-- Your customer chat code -->
      <div class="fb-customerchat"
        attribution=setup_tool
        page_id="2420391644689940"
  theme_color="#0084ff"
  logged_in_greeting="Bạn cần trợ giúp ? Washfriends xin hân hạnh phục vụ quý khách."
  logged_out_greeting="Washfriends xin hân hạnh phục vụ quý khách.">
      </div>
<!--/// Load Facebook SDK for JavaScript -->