
@php($countBr = count($breadcrumb))
@if($countBr > 0 || !empty($extraCommand))
    <div class="breadcrumb">
        <ul>
            @if($countBr > 1)
                @foreach($breadcrumb as $item)
                    @if($loop->last)
                        <li><a href="javascript:void(0);" class="active">{{ $item['title'] }} </a></li>
                    @else
                        <li>
                            @if(!empty($item['link']))
                                <a href="{{ $item['link'] }}">{{ $item['title'] }}</a>
                            @else
                                {{ $item['title'] }}
                            @endif
                        </li>
                    @endif
                @endforeach
            @else
                <li>{{ $defBr['title'] }}</li>
            @endif

            @if(!empty($extraCommand))
                <li class="d-md-down-none">
                    <div class="btn-group" role="group" aria-label="Button group">
                        @foreach($extraCommand as $item)
                            <a class="btn{{!empty($item['class'])?' '.$item['class']:''}}" href="{{ route($item['link']) }}">@if(!empty($item['icon']))<i class="{{ $item['icon'] }}"></i>@endif &nbsp;{{ $item['title'] }}</a>
                        @endforeach
                    </div>
                </li>
            @endif
        </ul>
    </div>
@endif
