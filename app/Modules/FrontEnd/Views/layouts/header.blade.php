
<header class="ruby-menu-demo-header">
    <!-- ########################### -->
    <!-- START: RUBY HORIZONTAL MENU -->
    <div class="ruby-wrapper container">
        <button class="c-hamburger c-hamburger--htx visible-xs">
            <span>toggle menu</span>
        </button>
        <ul class="ruby-menu">
            <li class="ruby-active-menu-item"><a href="{{ route('home') }}">Home</a></li>
            @foreach ($catNews as $item)
            <li>
                <a href="{{ route('news.list', ['p_cat' => $item['safe_title']]) }}">{{ $item['title'] }} <i class="fas fa-angle-up"></i></a>
                @if(!empty($item['sub']))
                <ul class="">
                    @foreach ($item['sub'] as $i_sub)
                    <li>
                        <a href="{{ route('news.list', ['p_cate' => $item['safe_title'], 'child_cate' => $i_sub['safe_title']]) }}">{{ $i_sub['title'] }}</a>
                        @if(!empty($i_sub['sub']))
                        <ul>
                            @foreach ($i_sub['sub'] as $_sub)
                            <li><a href="#"><i class="fa fa-university" aria-hidden="true"></i>{{ $_sub['title'] }}</a></li>
                            @endforeach
                        </ul>
                        <span class="ruby-dropdown-toggle"></span>
                        @endif
                    </li>
                    @endforeach
                </ul>
                <span class="ruby-dropdown-toggle"></span>
                @endif
            </li>
            @endforeach

            <li class="ruby-menu-mega"><a href="#">Videos</a>
                <div class="ruby-grid ruby-grid-lined">
                    <div class="ruby-row">
                        @foreach ($catVideos as $item)
                        <div class="ruby-col-2">
                            <h3 class="ruby-list-heading"><a href="{{ route('videos.list', ['p_cate' => $item['safe_title']]) }}">{{ $item['title'] }}</a></h3>
                            @if (!empty($item['sub']))
                               <ul>
                                @foreach ($item['sub'] as $i_sub)
                                    <li><a href="{{ route('videos.list', ['p_cate' => $item['safe_title'], 'child_cate' => $i_sub['safe_title']]) }}">{{ $i_sub['title'] }}</a></li>
                                @endforeach
                                </ul> 
                            @endif
                        </div>
                        @endforeach
                    </div>
                </div>
                <span class="ruby-dropdown-toggle"></span>
            </li>

            
            <li class="ruby-menu-right ruby-menu-social">
            @if(\Auth::guard('customer')->check())
                <a class="text-white w-100" href="{{ route('profile.home', ['user' => \Auth::guard('customer')->user()->user_name]) }}"><i class="fas fa-user-tie" aria-hidden="true"></i> {{ \Auth::guard('customer')->user()->user_name }}</a>
                @else 
                <a class="text-white" data-toggle="modal" data-target="#loginForm"><i class="fas fa-user-tie" aria-hidden="true"></i></a>
                @endif
            </li>

            <li class="ruby-menu-right ruby-menu-social ruby-menu-search"><a href="#"><i class="fa fa-search" aria-hidden="true"></i><span><input type="text" name="search" placeholder="Search.."></span></a></li>

        </ul>
    </div>
    <!-- END:   RUBY HORIZONTAL MENU -->
    <!-- ########################### -->
    <!-- END: RUBY DEMO HEADER -->
</header>
