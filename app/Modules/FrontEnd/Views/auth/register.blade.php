@extends('FrontEnd::layouts.home')

@section('title') {!! \Lib::siteTitle($site_title, $def['site_title']) !!} @stop

@section('content')
    <div class="form-user pt-5">
        <div class="container">
            <div class="form form-register active" style="z-index:unset">
                <div id="pop_register">
                    <div class="title-heading">đăng kí</div>
                    <div class="validate-input">
                        <label for="name">Email:</label>
                        <input class="has-icon icon-user" type="email" id="pop-email" name="email"
                               placeholder="Số điện thoại hoặc địa chỉ Email">
                    </div>
                    <div class="validate-input">
                        <label for="name">Mật khẩu:</label>
                        <input class="has-icon icon-lock" id="pop-pw" type="password" name="pwd" placeholder="Mật khẩu:">
                    </div>
                    <div class="validate-input">
                        <label for="name">Xác nhận mật khẩu:</label>
                        <input class="has-icon icon-lock" id="pop-pw-rp" type="password" name="re-pwd" placeholder="Xác nhận mật khẩu">
                    </div>
                    <div class="wrap-rem">
                        <div class="remember-login">
                            <input type="checkbox" class="form-check-input" id="exampleCheck1">
                            <label class="form-check-label" for="exampleCheck1">Ghi nhớ đăng nhập</label>
                        </div>
                    </div>
                    <div class="submit">
                        <button onclick="shop.register();"  class="btn-submit">Đăng nhập</button>
                    <div class="creat-acc">Bạn đã có tài khoản: <a href="{{ route('login') }}" class="link-register">Đăng nhập</a></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop