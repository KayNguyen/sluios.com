@extends('FrontEnd::layouts.home')
@section('title') {!! \Lib::siteTitle('Không tìm thấy trang', $def['site_title'],false,true) !!} @stop

@section('content')

    <div class="ag-page-404">
        <div class="ag-toaster-wrap">
            <div class="ag-toaster">
                <div class="ag-toaster_back"></div>
                <div class="ag-toaster_front">
                    <div class="js-toaster_lever ag-toaster_lever"></div>
                </div>
                <div class="ag-toaster_toast-handler">
                    <div class="js-toaster_toast ag-toaster_toast js-ag-hide"></div>
                </div>
            </div>

            <canvas id="canvas-404" class="ag-canvas-404"></canvas>
            <img class="ag-canvas-404_img" src="{{ asset('admin/images/error/smoke.png') }}">
        </div>
    </div>
@endsection

@section('js_bot')
    {!! \Lib::addMedia('admin/js/error/404.js') !!}
@endsection