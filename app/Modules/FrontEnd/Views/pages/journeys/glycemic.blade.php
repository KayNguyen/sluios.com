@extends('FrontEnd::layouts.home', ['bodyClass' => 'has-cover'])

@section('title') {!! \Lib::siteTitle($site_title, $def['site_title']) !!} @stop

@section('content')
    <div class="banner-journey journey-pc-2"><img src="{{ asset('html-washfriends/images/banner-journey.png') }}" alt=""></div>
    <div class="journey side-2 journey-pc-2">
        <div class="journey-part-1">
            <div class="wrapper">
                <div class="title title-head has-bg">Tiểu đường là nguyên nhân <span>Gây tử vong cao thứ 3 tại Việt Nam</span>
                </div>
                <div class="content">
                    <div class="title">Những con số <span>biết nói</span> về bệnh tiểu đường</div>
                    <div class="row">
                        <div class="col-lg-3 col-sm-6 col-6">
                            <div class="items">
                            <div class="image"><img src="{{ asset('html-washfriends/images/icon-jn-1.png') }}" alt=""></div>
                                <div class="line"></div>
                                <div class="desc">
                                    <span class="number">70%</span> người Việt Nam mắc đái tháo đường chưa được chuẩn đoán
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-sm-6 col-6">
                            <div class="items">
                                <div class="image"><img src="{{ asset('html-washfriends/images/icon-jn-2.png') }}" alt=""></div>
                                <div class="line"></div>
                                <div class="desc">
                                    <span class="number">90%</span> người mắc tiểu đường thuộc tiểu đường tuýp 2
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-sm-6 col-6">
                            <div class="items">
                                <div class="image"><img src="{{ asset('html-washfriends/images/icon-jn-3.png') }}" alt=""></div>
                                <div class="line"></div>
                                <div class="desc">
                                    <span class="number">29%</span> người bệnh tiểu đường được điều trị tại các cơ sở y tế
                                    thấp
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-sm-6 col-6">
                            <div class="items">
                                <div class="image"><img src="{{ asset('html-washfriends/images/icon-jn-4.png') }}" alt=""></div>
                                <div class="line"></div>
                                <div class="desc">
                                    Cứ <span class="number">30s</span> Người lại có thêm <span>3 người</span> mất đi <span>1 phần cơ thể</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="journey-part-2">
            <div class="title title-heading has-bg">Dự kiến năm 2045 <span>có 6.13 triệu người</span> mắc bệnh</div>
            <div class="img-wrap section-1">
                <div class="image-text">Biến chứng</div>
            <div class="image"> <img src="{{ asset('html-washfriends/images/img-jn-2.png') }}" alt=""></div>
                <p>Ai cũng biết tiểu đường nếu không kiểm soát tốt sẽ tăng nguy cơ biến chứng nguy hiểm như bệnh tim, đột
                    quỵ, huyết áp cao, mù, thận, cắt chi,... tử vong</p>
            </div>
            <img src="{{ asset('html-washfriends/images/bg-jn-1.png') }}" alt="" class="image-bg">
            <div class="img-wrap">
                <div class="image-text">Kiểm soát đường huyết là điều tối quan <br> trọng để ngăn ngừa biến chứng</div>
                <div class="image"> <img src="{{ asset('html-washfriends/images/img-jn-2.png') }}" alt=""></div>
            </div>
        </div>
        <div class="journey-part-3">
            <div class="title">
                Với chúng tôi, việc nhìn bệnh nhân <span>đơn độc chữa trị</span> <br> là điều không thể chấp nhận
            </div>
            <div class="image-bg"><img src="{{ asset('html-washfriends/images/bg-jn-2.png') }}" alt=""></div>
            <!-- <div class="desc-text">Cotarin quyết tâm thực hiện<span>10.000 chuyến thăm khám bệnh miễn phí </span>cùng tham gia là các bác sĩ, dược sĩ dày dặn kinh nghiệm</div> -->
        </div>
        <div class="journey-part-4">
            <div class="line-month">
                <div class="line"><img src="{{ asset('html-washfriends/images/line-plabe.png') }}" alt=""></div>
                <div class="month august">Tháng 8 <br> 2019</div>
                <div class="month march"> Tháng 3 <br> 2020</div>
                <div class="month december">Tháng 12 <br> 2019</div>
                <div class="month june">Tháng 6 <br> 2020</div>
            </div>
            <div class="title">Hành trình <span>không điểm dừng</span></div>
            <div class="title-sub">Cùng Cotarin Đường Huyết lan tỏa và <span>kéo dài mãi hành trình đầy ý nghĩa</span></div>
            <div class="description">Bệnh mãn tính nói chung, tiểu đường nói riêng sẽ không thể chữa khỏi hoàn toàn, <span>chúng tôi cam kết sẽ đồng hành cùng bạn trong suốt chặng đường khó khăn phía trước</span>, thành công của Cotarin đường huyết sẽ là nguồn động lực giúp chúng tôi <span>lan tỏa và kéo dài mãi chuyến hành trình</span> đầy ý nghĩa này</div>
            <img src="{{ asset('html-washfriends/images/bg-joyney-5.png') }}" alt="" class="img-bg">
        </div>
        <div class="journey-part-5">
            <div class="journey-part-5-wrap">
                <div class="title">Sự kiện đang diễn ra</div>
                <div id="slide" class="js-carousel" data-items="3" data-arrows="true" data-dots="true" data-margin="40">
                    @foreach($news as $item)
                    <div class="items">
                        <div class="image">
                            <div class="img-wrap">
                                <img src="{{ $item->getImageUrl('medium') }}" alt="">
                            </div>
                        </div>
                        <div class="desc">
                            <a href="{{ route('news.detail', ['alias' => $item->alias]) }}">{!! mb_substr($item->sort_body, 0) !!}</a>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
    {{--mobile--}}
    <div class="journey-mobile-2">
        <div class="banner"><img src="{{ asset('html-washfriends/images/journey-2-banner.png') }}" alt=""></div>
        <div class="content first-section">
            <div class="title main-title">Tiểu đường là nguyên nhân <br> <span>Gây tử vong cao thứ 3 tại Việt Nam</span></div>
            <div class="title sub-title">Những con số <span>biết nói</span> về bệnh tiểu đường</div>
            <div class="js-carousel slide-first-section" data-loop="false" data-items="1" data-dots="true" data-arrows="true" data-margin="20">
                <div class="items">
                    <div class="item-image"><img src="{{ asset('html-washfriends/images/icon-journey-1.png') }}" alt=""></div>
                    <div class="item-line"></div>
                    <div class="item-desc"><span>70%</span> người Việt Nam mắc đái tháo đường chưa được chuẩn đoán</div>
                </div>
                <div class="items">
                    <div class="item-image"><img src="{{ asset('html-washfriends/images/icon-journey-2.png') }}" alt=""></div>
                    <div class="item-line"></div>
                    <div class="item-desc"><span>70%</span> người Việt Nam mắc đái tháo đường chưa được chuẩn đoán</div>
                </div>
                <div class="items">
                    <div class="item-image"><img src="{{ asset('html-washfriends/images/icon-journey-3.png') }}" alt=""></div>
                    <div class="item-line"></div>
                    <div class="item-desc"><span>70%</span> người Việt Nam mắc đái tháo đường chưa được chuẩn đoán</div>
                </div>
                <div class="items">
                    <div class="item-image"><img src="{{ asset('html-washfriends/images/icon-journey-4.png') }}" alt=""></div>
                    <div class="item-line"></div>
                    <div class="item-desc"><span>70%</span> người Việt Nam mắc đái tháo đường chưa được chuẩn đoán</div>
                </div>
            </div>
        </div>

        <div class="content second-section">
            <div class="title main-title">Dự kiến năm 2045 <span>có 6.13 triệu người</span> mắc bệnh</div>
            <div class="image"><img src="{{ asset('html-washfriends/images/journey-2-img-1.png') }}" alt=""></div>
            <div class="desc">
                <div class="wrap">
                    <h4>Biến chứng</h4>
                    <p>“ Ai cũng biết tiểu <br> đường nếu không kiểm  <br>soát tốt sẽ tăng nguy cơ <br> biến chứng nguy hiểm <br> như bệnh tim, đột quỵ,  <br>huyết áp cao, mù, thận, <br> cắt chi,... tử vong ”</p>
                </div>
            </div>
        </div>

        <div class="line">
            <img src="{{ asset('html-washfriends/images/journey-2-line.png') }}" alt="">
        </div>

        <div class="content third-section">
            <div class="third-section-title">Kiểm soát đường huyết là điều tối quan trọng để ngăn ngừa biến chứng</div>
            <div class="image">
                <div class="wrap">
                    <img src="{{ asset('html-washfriends/images/journey_doctor.png') }}" alt="">
                </div>
            </div>
        </div>

        <img src="{{ asset('html-washfriends/images/journey-2-img-2.png') }}" alt="" class="image-stc">

        <div class="content four-section">
            <div class="title main-title">Hành trình <span>không điểm dừng</span></div>
            <div class="desc">
                Bệnh mãn tính nói chung, tiểu đường nói riêng sẽ không thể chữ khỏi hoàn toàn, <span> chúng tôi cam kết  sẽ đồng hành cùng bạn trong suốt chặng đường khó khăn phía trước </span>, thành công của Cotarin đường huyết sẽ là nguồn động lực giúp chúng tôi <span> lan tỏa và kéo dài mãi chuyến hành trình </span> đầy ý nghĩa này
            </div>
        </div>

        <div class="content five-section">
            <div class="five-section-title">Cùng Cotarin Đường Huyết lan tỏa và <span>kéo dài mãi hành trình đầy ý nghĩa</span></div>
            <img src="{{ asset('html-washfriends/images/journey-2-img-3.png') }}" alt="">
        </div>

        <div class="content six-section">
            <div class="six-section-title">Sự kiện đang diễn ra</div>
            <div class="js-carousel slide-six-section" data-items="1" data-dots="true" data-arrows="true" data-margin="20">
                @foreach($news as $item)
                    <div class="items">
                        <div class="image">
                            <div class="img-wrap">
                                <img src="{{ $item->getImageUrl('medium') }}" alt="">
                            </div>
                        </div>
                        <div class="desc">
                            <a href="{{ route('news.detail', ['alias' => $item->alias]) }}">{!! mb_substr($item->sort_body, 0) !!}</a>
                        </div>
                    </div>
                    <div class="items">
                        <div class="item-image"><img src="{{ $item->getImageUrl('medium') }}" alt=""></div>
                        <div class="item-desc">{!! mb_substr($item->sort_body, 0) !!}</div>
                    </div>
                @endforeach

            </div>
        </div>
    </div>
@endsection

