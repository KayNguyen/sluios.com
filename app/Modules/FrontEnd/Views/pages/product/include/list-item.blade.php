<!-- col-12 col-sm-6 col-lg-3  -->
<div class="col-12 col-sm-6 col-lg-3 p-item">
    <div class="p-wrap">
        <a href="{{ route('product.detail', ['alias' => $product->alias]) }}" class="p-img">
            <img src="{{\ImageURL::getImageUrl($product->image, \App\Models\Product::KEY, 'medium')}}" alt="{{ $product->title }}">
        </a>
        <a href="{{ route('product.detail', ['alias' => $product->alias]) }}" class="p-name">
            {{ $product->title }}
        </a>
        <p class="p-dec">
            {!! str_limit( strip_tags($product->sort_body), $limit = 50) !!}
        </p>
        <a href="{{ route('product.detail', ['alias' => $product->alias]) }}" class="p-view">
            Xem chi tiết <i class="fa fa-chevron-right" aria-hidden="true"></i>
        </a>
    </div>
</div>