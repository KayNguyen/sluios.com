@extends('FrontEnd::layouts.home', ['bodyClass' => 'has-cover'])

@section('title') {!! \Lib::siteTitle('Danh sách sản phẩm') !!} @stop

@section('content')
    <main>
        <section class="slider_home">
            <div class="js-carousel" data-items="1" data-arrows="false">
                @if(isset($slide))
                @foreach($slide as $sl)
                <div class="home-slider-item">
                    <a href="{{ $sl->link }}">
                        <img src="{{ $sl->getImageUrl('original', 'image') }}" alt="{{ $sl->title }}">
                    </a>
                </div>
                @endforeach
                @endif
            </div>
        </section>



        @foreach($data as $cat => $value)
        @if($cat != 'hot')
        <section class="box-cat-pro">
            <div class="container">
                <div class="box-cat-header">
                    <h3>
                        Lựa chọn {{ $cat }}
                    </h3>
                    <a href="{{ route('product.list.cate', ['alias' => str_slug($cat)]) }}">Xem tất cả {{ $cat }} <i class="fa fa-chevron-right" aria-hidden="true"></i></a>
                </div>
                <div class="row p-list">
                    @foreach($value as $key => $product)
                        @if($key < 4)
                            @include('FrontEnd::pages.product.include.list-item')
                        @endif
                    @endforeach
                </div>
            </div>
        </section>
        @endif
        @endforeach

        <div class="mt-5 d-block"></div>
        <section class="slider_home">
            <div class="js-carousel" data-items="1" data-arrows="false">
                @if(isset($slide))
                @foreach($slide as $sl)
                @if($loop->last)
                <div class="home-slider-item">
                    <a href="{{ $sl->link }}">
                        <img src="{{ $sl->getImageUrl('original', 'image') }}" alt="{{ $sl->title }}">
                    </a>
                </div>
                @endif
                @endforeach
                @endif
            </div>
        </section>
        
        <div class="container" class="cat-info">
            @foreach($data['hot'] as $pro_hot)
            <div class="row cat-info-box mt-4">
                <div class="col-12 col-lg-6 cat-info-box-text">
                    <h4>{{ $pro_hot->title }}</h4>
                    <div class="cat-info-box-content">
                        <!-- <strong>
                            Công Nghệ Giặt Tay Kết Hợp
                        </strong> -->
                        <p>
                            {!! str_limit( strip_tags($pro_hot->sort_body), $limit = 200) !!}
                        </p>
                    </div>
                    <a href="{{ route('product.detail', ['alias' => $pro_hot->alias]) }}">Khám phá ngay <i class="fa fa-chevron-right" aria-hidden="true"></i></a>
                </div>
                <div class="col-12 col-lg-6  cat-info-box-img">
                    <figure class="m-0">
                        <img src="{{\ImageURL::getImageUrl($pro_hot->image, \App\Models\Product::KEY, 'medium')}}" alt="{{ $pro_hot->title }}">
                    </figure>
                </div>
            </div>
            @endforeach
        </div>
    </main>


    
@endsection

