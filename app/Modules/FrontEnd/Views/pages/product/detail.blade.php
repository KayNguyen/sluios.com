@extends('FrontEnd::layouts.home', ['bodyClass' => 'has-cover'])

@section('title') {!! \Lib::siteTitle(@$data->title) !!} @stop

@section('content')
    <!-- loader -->
    <div id="pb_loader" class="show fullscreen">
        <svg class="circular" width="48px" height="48px">
            <circle class="path-bg" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke="#eeeeee" />
            <circle class="path" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke-miterlimit="20"
                stroke="#1d82ff" />
        </svg>
    </div>
    <main>
        <div class="product-detail-home">
            <div class="container pb-4">
                {!! \Lib::renderBreadcrumb(false, true,'FrontEnd::layouts.breadcrumb') !!}

                <div class="row">
                    @if(count($images) > 0)
                    <div class="col-12 col-lg-5 mb-5">
                        <div class="product-image">
                            <div class="js-carousel product-image-slider" data-items="1" data-arrows="false">
                                @foreach($images as $img)
                                    @if($loop->first)
                                        @php($img_first = $img->getImageUrl('medium'))
                                    @endif
                                <div class="img-item">
                                    <a href="{{ $img->getImageUrl('large') }}" data-fancybox="gallery" class="MagicZoom" data-options="zoomPosition: inner">
                                        <img src="{{ $img->getImageUrl('medium') }}" alt="{{ $img->title }}">
                                    </a>
                                </div>
                                @endforeach
                            </div>
                            <span class="view-zoom"><i class="fa fa-search" aria-hidden="true"></i> Click hoặc rê chuột vào ảnh để phóng to</span>
                            <div class="view-other">
                                @if(@$data->link_video != null)
                                <a data-fancybox href="{{ @$data->link_video }}" class="play-video">
                                    <img src="{{asset('html-washfriends/images/ico_play_yt.png')}}" alt="Video">
                                    <span>Video</span>
                                </a>
                                @endif
                                @if(count($images) > 0)
                                <a data-fancybox="gallery" href="{{ $img_first }}" class="more-img">
                                    <img src="{{asset('html-washfriends/images/more_img.png')}}" alt="Xem thêm">
                                    <span>
                                        Xem thêm
                                        {{ count($images) }} ảnh
                                    </span>
                                </a>
                                @endif
                            </div>
                        </div>
                    </div>
                    @endif

                    <div class="col-12 col-lg-4 ">
                        <div class="product-emtry">
                            <h2 class="product-name">{{ @$data->title }}</h2>
                            <span class="product-price">{{\Lib::priceFormat(@$data->price)}}</span>
                            <span class="pro-promotion">{{ @$data->note }}</span>
                            <ul class="pro-emtry-info">
                                {!! @$data->sort_body !!}
                            </ul>
                        </div>
                    </div>
                    <div class="col-12 col-lg-3">
                        <div class="pro-sidebar">
                            {!! @$data->body !!}
                        </div>
                    </div>
                </div>
            </div>
            @if(!empty($data->properties) && $data->properties !== 'null')
            <div class="pro-specifications">
                <div class="container">
                    <div class="row">
                        <div class="col-12 col-md-6 offset-md-3">
                            <div class="head-title">
                                <h4>thông số kỹ thuật</h4>
                                <a href="javascript:;">
                                    Xem tất cả thông số máy bên dưới <i class="fa fa-chevron-down" aria-hidden="true"></i>
                                </a>
                            </div>
                            <ul class="tab_specifications">
                                @php($properties = json_decode($data->properties))
                                @foreach($properties as $pro)
                                <li>
                                    <span>
                                        {{ $pro->title }}
                                    </span>
                                    <span>
                                        {{ $pro->value }}
                                    </span>
                                </li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            @endif
            <div class="pro-content">
                <div class="container">
                    <div class="head-title">
                        <h4>
                            Mô tả sản phẩm
                        </h4>
                        <a href="#">
                            Xem tất cả tính năng máy giặt bên dưới <i class="fa fa-chevron-down" aria-hidden="true"></i>
                        </a>
                    </div>
                    <div class="content-detail">
                        {!! !empty(@$data->content) ? @$data->content : '' !!}
                    </div>
                </div>
            </div>
            @if(!empty($prd_history))
            <div class="product-viewed">
                <div class="container">
                    <div class="head-title">
                        <h4>đã xem gần đây</h4>
                    </div>
                    
                    <div class="row p-list">
                        @foreach($prd_history as $prd_hi)
                        <div class="col-12 col-sm-6 col-lg-3 p-item">
                            <div class="p-wrap">
                                <a href="{{ route('product.detail', ['alias' => $prd_hi->alias]) }}" class="p-img">
                                    <img src="{{ $prd_hi->getImageUrl('original') }}" alt="{{ $prd_hi->title }}">
                                </a>
                                <a href="{{ route('product.detail', ['alias' => $prd_hi->alias]) }}" class="p-name">
                                    {{ $prd_hi->title }}
                                </a>
                                <p class="p-dec">
                                    {!! str_limit( strip_tags($prd_hi->sort_body), $limit = 100) !!}[...]
                                </p>
                                <a href="{{ route('product.detail', ['alias' => $prd_hi->alias]) }}" class="p-view">
                                    Xem chi tiết <i class="fa fa-chevron-right" aria-hidden="true"></i>
                                </a>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
            @endif
        </div>
    </main>
@endsection