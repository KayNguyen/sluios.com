@extends('FrontEnd::layouts.home', ['bodyClass' => 'has-cover'])

@section('title') {!! \Lib::siteTitle('Đặt dịch vụ') !!} @stop

@section('content')

    <main>
        
        <section class="slider_home">
            <div class="js-carousel" data-items="1" data-arrows="false">
                @foreach($slide as $sl)
                <div class="home-slider-item">
                    <a href="{{ $sl->link }}">
                        <img src="{{ $sl->getImageUrl('original', 'image') }}" alt="">
                    </a>
                </div>
                @endforeach

            </div>
        </section>

        <form action="{{ route('service.book') }}" method="GET">
            <div class="tab-price-page">
                <div class="container">
                    <div class="tab-control">
                        @foreach($data as $item)
                            <input type="hidden" name="service[{{ $item->id }}]">
                            <div class="tab-control-item @if ($loop->first) active @endif" data-tab="#price-{{ $loop->index }}">
                                <figure class="my-0">
                                    <img class="img-1" src="{{ $item->getImageUrl('medium', 'sub_image') }}" alt="{{ $item->title }}">
                                    <img class="img-2" src="{{ $item->getImageUrl('medium', 'image')}}" alt="{{ $item->title }}">
                                </figure>
                                <h6>
                                    {{ $item->title }}
                                </h6>
                                <p>
                                    {{ $item->desc }}
                                </p>
                            </div>
                        @endforeach
                    </div>
                    <div class="tab-pannel">
                        @foreach($data as $item)
                            <div class="tab-item" @if (!$loop->first) style="display:none" @endif id="price-{{ $loop->index }}">
                                <div class="tab-item-head">
                                    <h5>
                                        bảng giá {{ $item->title }}
                                    </h5>
                                    <p>
                                        Thêm 10.000 để quần áo thêm trắng, sáng hơn
                                    </p>
                                </div>
                                <div class="tab-main">
                                    <ul>

                                    @php($services = json_decode($item->service, true))
                                    <!-- this is title in table -->
                                        <li class="tab-row tab-title">
                                            <ul>
                                                @foreach($services['name'] as $key => $it)
                                                    <li>
                                                        <b>{{ $it }}</b>
                                                    </li>
                                                @endforeach
                                                <li>
                                                    <b>
                                                        chọn dịch vụ
                                                    </b>
                                                </li>
                                            </ul>
                                        </li>
                                        <!--end this is title in table -->

                                        <!-- tab row  -->
                                        @foreach($services['value'] as $key => $items)
                                            <li class="tab-row row-content">
                                                <ul>
                                                    @foreach($items as $k => $it )
                                                        <li id="fcku_{{ $k }}" data-val="{{ $it }}" @if ($loop->first) class="js-value" @endif>
                                                            <b>
                                                                {{ $it }}
                                                            </b>
                                                        </li>
                                                    @endforeach
                                                    <li>
                                                        <input type="checkbox" class="service_child" data-id="{{ $item->id }}" name="service{{ $item->id }}[]">
                                                        <span></span>
                                                        <div class="fck_jenny"></div>
                                                    </li>
                                                </ul>
                                            </li>
                                    @endforeach
                                    <!-- end tab row  -->

                                    </ul>
                                </div>
                                <div class="btn-gr-book row">
                                    <div class="col-lg-6 col-12">
                                        <button type="submit" class="btn-theme w-100 btn d-block text-center">
                                            Đặt dịch vụ ngay <i class="fa fa-chevron-right" aria-hidden="true"></i>
                                        </button>
                                    </div>
                                    <div class="col-lg-6 col-12">
                                        <a href="/quy-trinh" class="btn-theme btn-view d-block text-center">
                                            xem quy trình <i class="fa fa-chevron-right" aria-hidden="true"></i>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </form>

    </main>
@endsection

@section('js_bot')
    <script>
        $('.service_child').click(function(){
            if(this.checked == true){
                var cate_val = $(this).parent().siblings('.js-value').attr('data-val');
                var fk_id = $(this).attr('data-id');
                $(this).siblings('.fck_jenny').append('<input type="hidden" class="cate_val" name="service['+fk_id+'][]" value="' + cate_val + '">');
            }else {
                $(this).siblings('.fck_jenny').empty();
            }
        });
        $('.cate_fe').each(function() {
            if(this.checked == true){
                var cate_val = $(this).attr('data-val');
                $(this).parent('.checkbox-info').append('<input type="hidden" class="cate_val" name="cat_id[]" value="' + cate_val + '">');
            }
        });
    </script>
@endsection



