@extends('FrontEnd::layouts.home', ['bodyClass' => 'has-cover'])

@section('title') {!! \Lib::siteTitle('Đặt dịch vụ') !!} @stop

@section('content')

    <main class="book-sevice-page">
        <div class="container">
            <div class="row">
                <div class="offset-lg-1 col-lg-10">
                    <div class="book-sevice-main">
                        <div class="book-sevice-head">
                            <figure class="m-0">
                                <img src="{{ asset('html-washfriends/images/img_datlich.png') }}" alt="">
                            </figure>
                            <h2>Đặt dịch vụ</h2>
                        </div>
                        <div class="book-sevice-form">
                            <form id="book-service">
                                <label for="bok-name">Họ và tên : ( Bắt buộc ) </label>
                                <input type="text" id="bok-name" placeholder="Nhập họ và tên của bạn">

                                <label for="bok-phone">Số điện thoại của bạn : ( Bắt buộc ) </label>
                                <input type="text" id="bok-phone" placeholder="Nhập số điện thoại của bạn ">

                                <label for="bok-address">Địa chỉ của bạn ( Khách sạn, căn hộ, ...) ( Bắt buộc )</label>
                                <input type="text" id="bok-address" placeholder="Nhập địa chỉ của bạn">

                                <label for="bok-email">Email của bạn : </label>
                                <input type="text" id="bok-email" placeholder="Nhập email của bạn">

                                <label for="bok-option">Dịch vụ bạn cần ?</label>
                                <select name="" id="bok-option">
                                    <option value="-1" selected>Chọn ...</option>
                                    @foreach($data as $item)
                                    <option value="{{ $item->id }}">{{ $item->title }}</option>
                                    @endforeach
                                </select>
                                @if(!empty($jenny))
                                    <input type="hidden" id="bok-opt-detail" value="{{$jenny}}">
                                @endif

                                <label for="bok-coupon">Mã khuyến mại ( Nếu có )</label>
                                <input type="text" id="bok-coupon" placeholder="Nhập mã KM">

                                <a href="javascript:;" onclick="shop.book({{$item->id}})" class="d-block btn-theme text-center py-3">Đặt ngay <i class="fa fa-chevron-right" aria-hidden="true"></i></a>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
@endsection

