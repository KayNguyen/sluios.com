@extends('FrontEnd::layouts.home')

@section('title') {!! \Lib::siteTitle($site_title, $def['site_title']) !!} @stop



@section('content')
<main>
    <div class="home-page-content">
        <!--<div class="banner">
            <div class="m">
                <a href="">
                    <img src="./images/banner_1.jpg" alt="">
                </a>
            </div>
        </div>-->
        <div class="container page-content">
            <div class="row">
                <div class="col-md-12 col-lg-4 col-xl-3 mb-3">
                    <aside>
                        <section class="collection-video">
                            <header class="heading">
                                <h3 class="title">Videos</h3>
                            </header>
                            <div class="list-article list-videos">
                                @foreach ($videos as $item)
                                    <article>
                                        <div class="d-flex">
                                            <div class="m">
                                                <a href="#">
                                                    <img src="{{ ($item->snippet->thumbnails->maxres) ? $item->snippet->thumbnails->maxres->url : $item->snippet->thumbnails->standard->url }}" alt="">
                                                    <span class="pill duration">{{ \Lib::duration($item->contentDetails->duration) }}</span>
                                                </a>
    
                                            </div>
                                            <div class="info">
                                                <header class="header-info">
                                                    <div class="meta">
                                                        <span class="eyebrow">
                                                            <a href="#">{{ $rootCVideo[$item->id]['category']['title'] }}</a>
                                                        </span>
                                                    </div>
                                                    <h2 class="title sp-line-4">
                                                        <a href="#">{{ $item->snippet->title }}</a>
                                                    </h2>
                                                </header>
                                            </div>
                                        </div>
                                    </article>
                                @endforeach
                                
                            </div>
                        </section>
                    </aside>
                </div>
                <div class="col-md-12 col-lg-8 col-xl-6 mb-3">
                    <div class="row sticky-region mx-md-0">
                        <div class="main">
                            <div class="collection">
                                <div class="content">
                                    <article class="article story-1">
                                        <div class="m">
                                            <a href="{{ route('news.detail', ['alias' => $news[0]->alias]) }}">
                                                <picture>
                                                    <img src="{{ $news[0]->getImageUrl('original') }}" alt="{{ $news[0]->title }}">
                                                </picture>
                                            </a>
                                        </div>
                                        <div class="info">
                                            <header class="info-header">
                                                <h2 class="title sp-line-2">
                                                    <a href="{{ route('news.detail', ['alias' => $news[0]->alias]) }}">{{ $news[0]->title }}</a>
                                                </h2>
                                            </header>
                                            <div class="content">
                                                <p class="dek">
                                                    <a href="#"></a>
                                                </p>
                                                <div class="related">
                                                    <ul>
                                                        @foreach ($news[0]->related as $rel)
                                                        <li class="related-item sp-line-2"><a href="{{ route('news.detail', ['alias' => $rel->alias]) }}">{{ $rel->title }}</a></li>
                                                        @endforeach
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </article>
                                    

                                </div>
                            </div>
                            <div class="collection collection-spotlight">
                                <div class="content">
                                    <div class="row">
                                        @foreach ($news as $item)
                                        @if(!$loop->first)
                                        <div class="col-md-6">
                                            <article class="article story-1">
                                                <div class="m">
                                                    <a href="{{ route('news.detail', ['alias' => $item->alias]) }}">
                                                        <picture>
                                                            <img src="{{ $item->getImageUrl() }}" alt="{{ $item->title }}">
                                                        </picture>
                                                    </a>
                                                </div>
                                                <div class="info">
                                                    <header class="info-header">
                                                        <h2 class="title sp-line-2">
                                                            <a href="{{ route('news.detail', ['alias' => $item->alias]) }}">{{ $item->title }}</a>
                                                        </h2>
                                                    </header>
                                                    <div class="content">
                                                        <p class="dek">
                                                            <a href="#"></a>
                                                        </p>
                                                        <div class="related">
                                                            <ul class="flex-column">
                                                                @foreach ($item->related as $rel)
                                                                <li class="related-item flex-100 sp-line-2"><a href="#">{{ $rel->title }}</a></li>
                                                                @endforeach
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </article>
                                        </div>
                                        @endif
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                            <div class="collection collection-article-list">
                                <section class="border-0 p-0">
                                    <div class="content list-article">
                                        @foreach ($li_news_latest as $item)
                                        <article class="article story-1">
                                            <div class="d-flex">
                                                <div class="m">
                                                    <a href="{{ route('news.detail', ['alias' => $item->alias]) }}">
                                                        <img src="{{ $item->getImageUrl('medium') }}" alt="{{ $item->title }}">
                                                    </a>
                                                </div>
                                                <div class="info">
                                                    <header class="header-info">
                                                        <div class="meta">
                                                        <span class="eyebrow">
                                                            @foreach ($item->categories as $child_cate)
                                                            <a href="{{ route('news.list', ['p_cate' => $item->category['safe_title'], 'child_cate' => $child_cate->safe_title]) }}" class="mr-2">{{ $child_cate->title }}</a>
                                                            @endforeach
                                                        </span>
                                                        </div>
                                                        <h2 class="title sp-line-2">
                                                            <a href="{{ route('news.detail', ['alias' => $item->alias]) }}">{{ $item->title }}</a>
                                                        </h2>
                                                    </header>
                                                </div>
                                            </div>
                                        </article>
                                        @endforeach
                                    </div>
                                </section>
                            </div>

                        </div>
                    </div>
                </div>
                @if(!empty($li_news_history))
                <div class="col-md-12 col-lg-12 col-xl-3 mb-3">
                    <aside>
                        <section>
                            <header class="heading">
                                <h3 class="title">Đã xem gần đây</h3>
                            </header>
                            <div class="list-article">
                                @foreach($li_news_history as $his)
                                <article>
                                    <div class="d-flex">
                                        <div class="m">
                                            <a href="{{ route('news.detail', ['alias' => $his->alias]) }}" title="{{ $his->title }}">
                                                <img src="{{ $his->getImageUrl('medium') }}" alt="{{ $his->title }}">
                                            </a>
                                        </div>
                                        <div class="info">
                                            <header class="header-info">
                                                <div class="meta">
                                                    <span class="eyebrow">
                                                        @foreach ($his->categories as $child_cate)
                                                            <a href="{{ route('news.list', ['p_cate' => $his->category['safe_title'], 'child_cate' => $child_cate->safe_title]) }}" class="mr-2">{{ $child_cate->title }}</a>
                                                        @endforeach
                                                    </span>
                                                </div>
                                                <h2 class="title sp-line-4" title="{{ $his->title }}">
                                                    <a href="{{ route('news.detail', ['alias' => $his->alias]) }}">{{ $his->title }}</a>
                                                </h2>
                                            </header>
                                        </div>
                                    </div>
                                </article>
                                @endforeach
                            </div>
                        </section>
                    </aside>
                </div>
                @endif
                <div class="col-md-12 col-sm-12 col-xl-12 mb-3">
                    <div class="mb-5">
                        <header class="heading">
                            <h3 class="title">
                                <a href="#">Videos</a>
                            </h3>
                            <div class="more">
                                <a href="#">View All</a>
                            </div>
                        </header>
                        <section class="collection-section">
                            <div class="row">
                                <div class="col-md-6">
                                    <article class="article bg-black">
                                        <div class="lastest">
                                            <div class="m">
                                                <a href="#" class="home-item blog-post-item">
                                                    <img src="./images/sidebar_2.jpg" alt="">
                                                </a>
                                            </div>
                                            <div class="info">
                                                <header class="header-info">
                                                    <div class="meta">
                                            <span class="eyebrow">
                                                <a href="#">Lorem ryc</a>
                                            </span>
                                                    </div>
                                                    <h2 class="title sp-line-4">
                                                        <a href="#">Hannity: Republicans must demand to know if whistleblower was deep state operative</a>
                                                    </h2>
                                                </header>
                                            </div>
                                        </div>
                                    </article>
                                </div>
                                <div class="col-md-6">
                                    <div class="">
                                        <div class="list-article">
                                            <article class="article">
                                                <div class="info">
                                                    <header class="header-info">
                                                        <div class="meta">
                                                <span class="eyebrow">
                                                    <a href="#">Lorem ryc</a>
                                                </span>
                                                        </div>
                                                        <h2 class="title sp-line-2">
                                                            <a href="#">Hannity: Republicans must demand to know if whistleblower was deep state operative</a>
                                                        </h2>
                                                    </header>
                                                </div>
                                            </article>
                                            <article class="article">
                                                <div class="info">
                                                    <header class="header-info">
                                                        <div class="meta">
                                                <span class="eyebrow">
                                                    <a href="#">Lorem ryc</a>
                                                </span>
                                                        </div>
                                                        <h2 class="title sp-line-2">
                                                            <a href="#">Hannity: Republicans must demand to know if whistleblower was deep state operative</a>
                                                        </h2>
                                                    </header>
                                                </div>
                                            </article>
                                            <article class="article">
                                                <div class="info">
                                                    <header class="header-info">
                                                        <div class="meta">
                                                <span class="eyebrow">
                                                    <a href="#">Lorem ryc</a>
                                                </span>
                                                        </div>
                                                        <h2 class="title sp-line-2">
                                                            <a href="#">Hannity: Republicans must demand to know if whistleblower was deep state operative</a>
                                                        </h2>
                                                    </header>
                                                </div>
                                            </article>
                                            <article class="article">
                                                <div class="info">
                                                    <header class="header-info">
                                                        <div class="meta">
                                                <span class="eyebrow">
                                                    <a href="#">Lorem ryc</a>
                                                </span>
                                                        </div>
                                                        <h2 class="title sp-line-2">
                                                            <a href="#">Hannity: Republicans must demand to know if whistleblower was deep state operative</a>
                                                        </h2>
                                                    </header>
                                                </div>
                                            </article>
                                            <article class="article">
                                                <div class="info">
                                                    <header class="header-info">
                                                        <div class="meta">
                                                <span class="eyebrow">
                                                    <a href="#">Lorem ryc</a>
                                                </span>
                                                        </div>
                                                        <h2 class="title sp-line-2">
                                                            <a href="#">Hannity: Republicans must demand to know if whistleblower was deep state operative</a>
                                                        </h2>
                                                    </header>
                                                </div>
                                            </article>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </div>
                    <div class="mb-5">
                        <header class="heading">
                            <h3 class="title">
                                <a href="#">Videos</a>
                            </h3>
                            <div class="more">
                                <a href="#">View All</a>
                            </div>
                        </header>
                        <section class="collection-section">
                            <div class="row">
                                <div class="col-md-6">
                                    <article class="article bg-black">
                                        <div class="lastest">
                                            <div class="m">
                                                <a href="#" class="home-item blog-post-item">
                                                    <img src="./images/sidebar_1.jpg" alt="">
                                                </a>
                                            </div>
                                            <div class="info">
                                                <header class="header-info">
                                                    <div class="meta">
                                            <span class="eyebrow">
                                                <a href="#">Lorem ryc</a>
                                            </span>
                                                    </div>
                                                    <h2 class="title sp-line-4">
                                                        <a href="#">Hannity: Republicans must demand to know if whistleblower was deep state operative</a>
                                                    </h2>
                                                </header>
                                            </div>
                                        </div>
                                    </article>
                                </div>
                                <div class="col-md-6">
                                    <div class="">
                                        <div class="list-article">
                                            <article class="article">
                                                <div class="info">
                                                    <header class="header-info">
                                                        <div class="meta">
                                                <span class="eyebrow">
                                                    <a href="#">Lorem ryc</a>
                                                </span>
                                                        </div>
                                                        <h2 class="title sp-line-2">
                                                            <a href="#">Hannity: Republicans must demand to know if whistleblower was deep state operative</a>
                                                        </h2>
                                                    </header>
                                                </div>
                                            </article>
                                            <article class="article">
                                                <div class="info">
                                                    <header class="header-info">
                                                        <div class="meta">
                                                <span class="eyebrow">
                                                    <a href="#">Lorem ryc</a>
                                                </span>
                                                        </div>
                                                        <h2 class="title sp-line-2">
                                                            <a href="#">Hannity: Republicans must demand to know if whistleblower was deep state operative</a>
                                                        </h2>
                                                    </header>
                                                </div>
                                            </article>
                                            <article class="article">
                                                <div class="info">
                                                    <header class="header-info">
                                                        <div class="meta">
                                                <span class="eyebrow">
                                                    <a href="#">Lorem ryc</a>
                                                </span>
                                                        </div>
                                                        <h2 class="title sp-line-2">
                                                            <a href="#">Hannity: Republicans must demand to know if whistleblower was deep state operative</a>
                                                        </h2>
                                                    </header>
                                                </div>
                                            </article>
                                            <article class="article">
                                                <div class="info">
                                                    <header class="header-info">
                                                        <div class="meta">
                                                <span class="eyebrow">
                                                    <a href="#">Lorem ryc</a>
                                                </span>
                                                        </div>
                                                        <h2 class="title sp-line-2">
                                                            <a href="#">Hannity: Republicans must demand to know if whistleblower was deep state operative</a>
                                                        </h2>
                                                    </header>
                                                </div>
                                            </article>
                                            <article class="article">
                                                <div class="info">
                                                    <header class="header-info">
                                                        <div class="meta">
                                                <span class="eyebrow">
                                                    <a href="#">Lorem ryc</a>
                                                </span>
                                                        </div>
                                                        <h2 class="title sp-line-2">
                                                            <a href="#">Hannity: Republicans must demand to know if whistleblower was deep state operative</a>
                                                        </h2>
                                                    </header>
                                                </div>
                                            </article>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </div>
                    <div class="mb-5">
                        <header class="heading">
                            <h3 class="title">
                                <a href="#">Videos</a>
                            </h3>
                            <div class="more">
                                <a href="#">View All</a>
                            </div>
                        </header>
                        <section class="collection-section">
                            <div class="row">
                                <div class="col-md-6">
                                    <article class="article bg-black">
                                        <div class="lastest">
                                            <div class="m">
                                                <a href="#" class="home-item blog-post-item">
                                                    <img src="./images/sidebar_4.jpg" alt="">
                                                </a>
                                            </div>
                                            <div class="info">
                                                <header class="header-info">
                                                    <div class="meta">
                                            <span class="eyebrow">
                                                <a href="#">Lorem ryc</a>
                                            </span>
                                                    </div>
                                                    <h2 class="title sp-line-4">
                                                        <a href="#">Hannity: Republicans must demand to know if whistleblower was deep state operative</a>
                                                    </h2>
                                                </header>
                                            </div>
                                        </div>
                                    </article>
                                </div>
                                <div class="col-md-6">
                                    <div class="">
                                        <div class="list-article">
                                            <article class="article">
                                                <div class="info">
                                                    <header class="header-info">
                                                        <div class="meta">
                                                <span class="eyebrow">
                                                    <a href="#">Lorem ryc</a>
                                                </span>
                                                        </div>
                                                        <h2 class="title sp-line-2">
                                                            <a href="#">Hannity: Republicans must demand to know if whistleblower was deep state operative</a>
                                                        </h2>
                                                    </header>
                                                </div>
                                            </article>
                                            <article class="article">
                                                <div class="info">
                                                    <header class="header-info">
                                                        <div class="meta">
                                                <span class="eyebrow">
                                                    <a href="#">Lorem ryc</a>
                                                </span>
                                                        </div>
                                                        <h2 class="title sp-line-2">
                                                            <a href="#">Hannity: Republicans must demand to know if whistleblower was deep state operative</a>
                                                        </h2>
                                                    </header>
                                                </div>
                                            </article>
                                            <article class="article">
                                                <div class="info">
                                                    <header class="header-info">
                                                        <div class="meta">
                                                <span class="eyebrow">
                                                    <a href="#">Lorem ryc</a>
                                                </span>
                                                        </div>
                                                        <h2 class="title sp-line-2">
                                                            <a href="#">Hannity: Republicans must demand to know if whistleblower was deep state operative</a>
                                                        </h2>
                                                    </header>
                                                </div>
                                            </article>
                                            <article class="article">
                                                <div class="info">
                                                    <header class="header-info">
                                                        <div class="meta">
                                                <span class="eyebrow">
                                                    <a href="#">Lorem ryc</a>
                                                </span>
                                                        </div>
                                                        <h2 class="title sp-line-2">
                                                            <a href="#">Hannity: Republicans must demand to know if whistleblower was deep state operative</a>
                                                        </h2>
                                                    </header>
                                                </div>
                                            </article>
                                            <article class="article">
                                                <div class="info">
                                                    <header class="header-info">
                                                        <div class="meta">
                                                <span class="eyebrow">
                                                    <a href="#">Lorem ryc</a>
                                                </span>
                                                        </div>
                                                        <h2 class="title sp-line-2">
                                                            <a href="#">Hannity: Republicans must demand to know if whistleblower was deep state operative</a>
                                                        </h2>
                                                    </header>
                                                </div>
                                            </article>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </div>
                    <div class="mb-5">
                        <header class="heading">
                            <h3 class="title">
                                <a href="#">Videos</a>
                            </h3>
                            <div class="more">
                                <a href="#">View All</a>
                            </div>
                        </header>
                        <section class="collection-section">
                            <div class="row">
                                <div class="col-md-6">
                                    <article class="article bg-black">
                                        <div class="lastest">
                                            <div class="m">
                                                <a href="#" class="home-item blog-post-item">
                                                    <img src="./images/sidebar_2.jpg" alt="">
                                                </a>
                                            </div>
                                            <div class="info">
                                                <header class="header-info">
                                                    <div class="meta">
                                            <span class="eyebrow">
                                                <a href="#">Lorem ryc</a>
                                            </span>
                                                    </div>
                                                    <h2 class="title sp-line-4">
                                                        <a href="#">Hannity: Republicans must demand to know if whistleblower was deep state operative</a>
                                                    </h2>
                                                </header>
                                            </div>
                                        </div>
                                    </article>
                                </div>
                                <div class="col-md-6">
                                    <div class="">
                                        <div class="list-article">
                                            <article class="article">
                                                <div class="info">
                                                    <header class="header-info">
                                                        <div class="meta">
                                                <span class="eyebrow">
                                                    <a href="#">Lorem ryc</a>
                                                </span>
                                                        </div>
                                                        <h2 class="title sp-line-2">
                                                            <a href="#">Hannity: Republicans must demand to know if whistleblower was deep state operative</a>
                                                        </h2>
                                                    </header>
                                                </div>
                                            </article>
                                            <article class="article">
                                                <div class="info">
                                                    <header class="header-info">
                                                        <div class="meta">
                                                <span class="eyebrow">
                                                    <a href="#">Lorem ryc</a>
                                                </span>
                                                        </div>
                                                        <h2 class="title sp-line-2">
                                                            <a href="#">Hannity: Republicans must demand to know if whistleblower was deep state operative</a>
                                                        </h2>
                                                    </header>
                                                </div>
                                            </article>
                                            <article class="article">
                                                <div class="info">
                                                    <header class="header-info">
                                                        <div class="meta">
                                                <span class="eyebrow">
                                                    <a href="#">Lorem ryc</a>
                                                </span>
                                                        </div>
                                                        <h2 class="title sp-line-2">
                                                            <a href="#">Hannity: Republicans must demand to know if whistleblower was deep state operative</a>
                                                        </h2>
                                                    </header>
                                                </div>
                                            </article>
                                            <article class="article">
                                                <div class="info">
                                                    <header class="header-info">
                                                        <div class="meta">
                                                <span class="eyebrow">
                                                    <a href="#">Lorem ryc</a>
                                                </span>
                                                        </div>
                                                        <h2 class="title sp-line-2">
                                                            <a href="#">Hannity: Republicans must demand to know if whistleblower was deep state operative</a>
                                                        </h2>
                                                    </header>
                                                </div>
                                            </article>
                                            <article class="article">
                                                <div class="info">
                                                    <header class="header-info">
                                                        <div class="meta">
                                                <span class="eyebrow">
                                                    <a href="#">Lorem ryc</a>
                                                </span>
                                                        </div>
                                                        <h2 class="title sp-line-2">
                                                            <a href="#">Hannity: Republicans must demand to know if whistleblower was deep state operative</a>
                                                        </h2>
                                                    </header>
                                                </div>
                                            </article>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
@endsection