<div class="health-status-wrap">
    <div class="row">
        <div class="col-md-4 col-12">
            <div class="content">
                <div class="card">
                    <img class="image" src="{{ asset('html-washfriends/images/img-health-1.png') }}" alt="">
                    <div class="card-body">
                        <h4 class="card-title">Đường huyết</h4>
                        <p class="card-text">Chẩn đoán nhanh khả năng mắc tiểu đường thông qua chỉ số đường trong máu khi đói và khi no. </p>
                        <a class="btn-test" href="{{ route('history') }}">Kiểm tra</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4 col-12">
            <div class="content">
                <div class="card">
                    <img class="image" src="{{ asset('html-washfriends/images/img-health-2.png') }}" alt="">
                    <div class="card-body">
                        <h4 class="card-title">Huyết áp</h4>
                        <p class="card-text">Theo dõi huyết áp định kỳ để kiểm soát bệnh tăng huyết áp, dấu hiệu bất thường như đau đầu, nặng đầu,…. </p>
                        <a class="btn-test" href="{{ route('check.pressure') }}">Kiểm tra</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4 col-12">
            <div class="content">
                <div class="card">
                    <img class="image" src="{{ asset('html-washfriends/images/img-health-3.png') }}" alt="">
                    <div class="card-body">
                        <h4 class="card-title">BMI</h4>
                        <p class="card-text">Chỉ số khối cơ thể được dùng để đánh giá mức độ gầy hay béo của một người, áp dụng cho người trên 19 tuổi. </p>
                        <a class="btn-test" href="{{ route('check.bmi') }}">Kiểm tra</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>