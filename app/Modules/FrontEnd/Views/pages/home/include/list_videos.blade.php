<section>
    <div class="owl-carousel owl-theme slide__general home__slide--video sync1">
        @foreach($news as $item)
        <div class="plyr__video-embed" id="player">
            <iframe
                    width="100%"
                    height="100%"
                    src="{{ $item->link_videos }}"
                    frameborder="0"
                    allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                    allowfullscreen
            ></iframe>
        </div>
        @endforeach
    </div>

    <div class="owl-carousel owl-theme slide__general home__slide--carousel media-slide sync2" data-items-slide="3">
        @foreach($news as $item)
            <div class="wrap wrap__carousel">
                <div class="wrap__image">
                    <img src="{{ $item->getImageUrl('medium') }}" alt />
                </div>
                <div class="wrap__desc des">{{ $item->title }}</div>
            </div>
        @endforeach
    </div>
</section>