@foreach($news as $item)
    <div class="col-md-6">
        <div class="blog-post-item">
            <div class="post">
                <div class="post-image">
                    <a href="javascript:;">
                        <img src="{{ $item->getImageUrl('medium') }}" alt  width="150px"/>
                    </a>
                </div>
                <div class="desc">
                    <a href="{{ route('news.detail', ['alias' => $item->alias]) }}">{!! mb_substr($item->sort_body, 0) !!}</a>
                </div>
            </div>
        </div>
    </div>
@endforeach