<div class="home-intro">
    <div class="section-1">
      <div class="video">
        <div class="plyr__video-embed wrap" id="player">
          <iframe src="https://www.youtube.com/embed/PABBK-7otxg?wmode=transparent" frameborder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
        </div>
      </div>
      <div class="title">
        Chúng tôi tin niềm vui chỉ trọn vẹn <span>khi mỗi khách hàng đều được chăm</span> sóc tận tâm!
      </div>
    </div>
    <div class="section-2">
      <div class="intro-media">
        <div class="intro-media-content">
          <div class="title">Hành trình Cotarin</div>
          <div class="des">Vì thế hệ người Việt khỏe mạnh hơn!</div>
          <div class="content">Trong cuộc chiến với bệnh mãn tính, có lúc nào bạn mong mỏi một bàn tay đồng cảm, sẻ chia
            và dìu dắt cùng đi qua những khó khăn? Hành trình này là dành cho bạn – người đang cảm thấy mình đơn độc!
          </div>
          <div class="link"><a href="{{ route('journeys') }}" class="more">Tìm hiểu thêm</a></div>
        </div>
      </div>
      <div class="wrap-img">
        <img src="{{ asset('html-washfriends/images/heart-green.png') }}" alt />
      </div>
    </div>
    <div class="section-3 container">
      <div class="row">
        <div class="col-md-6">
          <div class="item">
            <div class="wrap-img">
              <img src="{{ asset('html-washfriends/images/item_demo.png') }}" alt />
              <div class="des">
                <a href="{{ route('history') }}">Hành trình kiểm soát chỉ số đường huyết</a>
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-6">
          <div class="item">
            <div class="wrap-img">
              <img src="{{ asset('html-washfriends/images/item_demo.png') }}" alt />
              <div class="des">
                <a href="{{ route('journeys') }}">Hành trình kiểm soát chỉ số coratin</a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>