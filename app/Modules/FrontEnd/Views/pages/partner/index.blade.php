@extends('FrontEnd::layouts.home', ['bodyClass' => 'has-cover'])

@section('title') {!! \Lib::siteTitle($site_title, $def['site_title']) !!} @stop

@section('content')
    <main>

        <section class="slider_home">
            <div class="js-carousel" data-items="1" data-arrows="false">
                @foreach($slide as $sl)
                    <div class="home-slider-item">
                        <a href="{{ $sl->link }}">
                            <img src="{{ $sl->getImageUrl('original', 'image') }}" alt="">
                        </a>
                    </div>
                @endforeach

            </div>
        </section>
        <section class="custommer_about container">
            <div class="row">
                <div class="col-12 col-lg-6">
                    <figure>
                        <img src="{{ @$config['images_dev'] }}" alt="">
                    </figure>
                </div>
                <div class="col-12 col-lg-6">
                    <div class="custommer_about-text">
                        {!! @$config['body'] !!}
                    </div>
                </div>
            </div>
        </section>

        <section class="box-sevice container">
            <h3 class="box-sevice-title">wash friends niềm tin của bạn - thành công của tôi</h3>
            <div class="row">
                @foreach($trust as $tr)
                    <div class="col-12 col-lg-4 mb-3">
                    <div class="box-sevice-item">
                        <figure>
                            <img src="{{ $tr->getImageUrl('original') }}" title="{{ $tr->title }}" alt="{{ $tr->title }}">
                        </figure>

                        <h5 title="{{ $tr->title }}">{{ $tr->title }}</h5>
                        {!! $tr->body !!}

                        <a href="{{ $tr->link }}">
                            Đặt ngay <i class="fa fa-chevron-right" aria-hidden="true"></i>
                        </a>
                    </div>
                </div>
                @endforeach
            </div>
        </section>

        <section class="layer_custommer">
            <div class="container">
                <h4 class="box-sevice-title">Tập khách hàng lõi được định danh bởi công nghệ</h4>
                <div class="row layer_custommer-list">
                    @foreach($partner as $par)
                        <div class="col-12 col-lg-4 mb-4">
                        <div class="layer_custommer-item">
                            <img src="{{ $par->getImageUrl('original') }}" title="{{ $par->title }}" alt="{{ $par->title }}">

                            <h5 title="{{ $par->title }}">{{ $par->title }}</h5>
                            {!! $par->body !!}
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </section>

        <section class="custommer_slider container">
            <h6>Tập khách hàng của chúng tôi</h6>
            <div class="js-carousel" data-items="6" data-items-xxs="2" data-dots="false" data-arrows="false" data-margin="10">
                <div class="custommer_item">
                    <a href="#">
                        <img src="{{ asset('html-washfriends/images/brand-1.png') }}" alt="">
                    </a>
                </div>
                <div class="custommer_item">
                    <a href="#">
                        <img src="{{ asset('html-washfriends/images/brand-2.png') }}" alt="">
                    </a>
                </div>
                <div class="custommer_item">
                    <a href="#">
                        <img src="{{ asset('html-washfriends/images/brand-3.png') }}" alt="">
                    </a>
                </div>
                <div class="custommer_item">
                    <a href="#">
                        <img src="{{ asset('html-washfriends/images/brand-4.png') }}" alt="">
                    </a>
                </div>
                <div class="custommer_item">
                    <a href="#">
                        <img src="{{ asset('html-washfriends/images/brand-5.png') }}" alt="">
                    </a>
                </div>
                <div class="custommer_item">
                    <a href="#">
                        <img src="{{ asset('html-washfriends/images/brand-6.png') }}" alt="">
                    </a>
                </div>
            </div>
        </section>
    </main>
@endsection

