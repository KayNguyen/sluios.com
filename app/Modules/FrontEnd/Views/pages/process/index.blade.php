@extends('FrontEnd::layouts.home', ['bodyClass' => 'has-cover'])

@section('title') {!! \Lib::siteTitle('Quy trình') !!} @stop

@section('content')
    <main>
        <section class="slider_home">
            <div class="js-carousel" data-items="1" data-arrows="false">
                @foreach($slide as $sl)
                <div class="home-slider-item">
                    <a href="{{ $sl->link }}">
                        <img src="{{ $sl->getImageUrl('original', 'image') }}" alt="">
                    </a>
                </div>
                @endforeach

            </div>
        </section>


            
        <div class="container">
            <div class="control_quy-trinh">
                @foreach ($data as $key => $item)
                     <div class="control_item {{ ($loop->first) ? 'active' : '' }}" onclick="shop.loadProcess({{ $item->id }})" data-tab="#quy-trinh-{{ $key }}">
                        <img src="{{ $item->getImageUrl($item->image) }}" alt="">
                        <h6>{{ $item->title }}</h6>
                        <p>{{ $item->sort_body }}</p>
                    </div>
                @endforeach
            </div>
            <div class="main_quy-trinh">
                <div class="main_item" id="quy-trinh-0" style="display: block;">
                    <figure class="m-0">
                        <img class="w-100" src="{{ $data[0]->getImageUrl($data[0]->process_image, 'process_image') }}" alt="">
                    </figure>
                </div>
                <div class="main_item" id="quy-trinh-1" style="display: none;">
                </div>
            </div>
        </div>
        <style>
            #content li{
                list-style: unset;
            }
        </style>
        <div class="quy-trinh-banner">
            <div class="container">
                <div class="row">
                    <div class="offset-lg-6 col-lg-6">
                        <div id="content">
                            {!! $data[0]->body !!}
                        </div>
                        <div class="btn-gr">
                            <a href="{{ route('service.book')}}" class="btn-theme py-3">Đặt ngay <i class="fa fa-chevron-right" aria-hidden="true"></i></a>
                            <a href="{{ route('service.list')}}" class="btn-view py-3">Xem bảng giá <i class="fa fa-chevron-right" aria-hidden="true"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </main>
@endsection


