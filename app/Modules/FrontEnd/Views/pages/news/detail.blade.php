@extends('FrontEnd::layouts.home', ['bodyClass' => 'homepage'])

@section('title') {!! \Lib::siteTitle($site_title, $def['site_title']) !!} @stop

 @section('facebook_meta')
    <meta name="title" content="{{$data->title_seo}}"/>
    <meta name="description" content="{{$data->description_seo}}"/>
    <meta name="keywords" content="{{$data->keywords}}"/>
    <meta property="og:locale" content="vi_VN" />
    <meta property="og:type" content="article" />
    <meta property="og:title" content="{{ $data->title_seo }}" />
    <meta property="og:description" content="{{$data->description_seo}}" />
    <meta property="og:url" content="{{ url()->full() }}" />
    <meta property="og:site_name" content="{{ env('APP_DOMAIN') }}" />
    <meta property="og:updated_time" content="{{Lib::dateFormat($data->created, 'd/m/Y') }}" />
    @if ($data->image)
    <meta property="og:image" content="{!! $data->getImageUrl() !!}" />
    <meta property="og:image:width" content="800" />
    <meta property="og:image:height" content="800" />
    @endif
    <meta property="article:publisher" content="https://www.facebook.com/{{ $def['facebook_name'] }}/" />

    @foreach($tags as $t)
        @if(!empty($t))
            <meta property="article:tag" content="{{ $t->title }}" />
        @endif
    @endforeach
    <meta property="article:published_time" content="{{Lib::dateFormat($data->published, 'd/m/Y') }}" />
    <meta property="article:modified_time" content="{{Lib::dateFormat($data->published, 'd/m/Y') }}" />
@stop
@section('twitter_meta')
    <meta name="twitter:card" content="summary_large_image" />
    <meta name="twitter:description" content="{{$data->sub_title}}" />
    <meta name="twitter:title" content="{{ $data->title }}" />
    @if ($data->image_seo)
    <meta name="twitter:image" content="{!! $data->getImageSeoUrl() !!}" />
    @endif
    <meta name="twitter:site" content=”{{ $data->getLink() }}” />
@stop
@section('g_meta')
    <meta itemprop="name" content="{{ $data->title }}">
    <meta itemprop="description" content="{{$data->sub_title}}">
    @if ($data->image_seo)
    <meta itemprop="image" content="{!! $data->getImageSeoUrl() !!}">
    @endif
@stop

@section('content')
    <main>
        <div class="home-page-content bg-white">
            <div class="container page-content">
                <div class="row">
                    <div class="container">
                        <header class="article-header">
                            <div class="article-meta article-meta-upper">
                                <div class="eyebrow">
                                    <a href="{{ route('news.list',['p_cate' => $data->category->safe_title]) }}">{{ $data->category->title }}</a>
                                </div>
                                <div class="article-date">
                                    <strong>Published</strong><time> {{ \Lib::dateFormat(@$data->published, 'd-m-Y H:i') }}</time>
                                </div>
                            </div>
                            <h1 class="headline">{{ $data->title }}</h1>
                            <div class="article-meta article-meta-lower">
                                <div class="author-byline">
                                    <!----><span>
                                    By
                                    <span><a href="javascript:void(0);" title="{{ $data->authors->fullname }}">{{ $data->authors->fullname }}</a><!---->
                                        <span class="article-source">
                                             | 
                                            @foreach ($data->categories as $item)
                                            <a target="_blank" href="{{ route('news.list',['p_cate' => $data->category->safe_title, 'child_cate' => $item->safe_title]) }}">
                                                {{ $item->title }} @if(!$loop->last) - @endif
                                            </a>
                                            @endforeach
                                        </span></span>
                                    </span>
                                    <!---->
                                </div>
                                
                            </div>
                        </header>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 col-lg-8 col-xl-8 mb-3">
                        <div class="content">
                            <article class="article story-1">
                                <div class="m">
                                    <a href="#">
                                        @if(empty($data->link_videos))
                                        <picture>
                                            <img src="{{ $data->getImageUrl('original') }}" alt="{{ $data->title }}">
                                        </picture>
                                        @else
                                        @php($link = str_replace('watch?v=', 'embed/', $data->link_videos))
                                        <iframe width="100%" height="100%" class="position-absolute" src="{{ $link }}" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                        @endif
                                    </a>
                                </div>
                                <div class="info">
                                    <header class="info-header">
                                        <div class="meta">
                                            @foreach ($data->categories as $item)
                                                <span class="eyebrow">
                                                    <a href="{{ route('news.list',['p_cate' => $data->category->safe_title, 'child_cate' => $item->safe_title]) }}">{{ $item->title }}</a>
                                                </span>
                                            @endforeach
                                            
                                        </div>
                                    </header>
                                    <div class="content">
                                        <p class="dek">{!! $data->sort_body !!}</p>
                                        {!! $data->body !!}
                                    </div>
                                    <div class="footer-detail">
                                        <div class="recipe-ratings">
                                            @if(\Auth::guard('customer')->check())
                                            <div>
                                                <p>Đánh giá bài viết</p>
                                                <div class="rating">
                                                    <input type="radio" id="star5" name="rating" value="5" /><label for="star5"></label>
                                                    <input type="radio" id="star4" name="rating" value="4" /><label for="star4"></label>
                                                    <input type="radio" id="star3" name="rating" value="3" /><label for="star3"></label>
                                                    <input type="radio" id="star2" name="rating" value="2" /><label for="star2"></label>
                                                    <input type="radio" id="star1" name="rating" value="1" /><label for="star1"></label>
                                                </div>
                                            </div>
                                            @endif
                                            <div class="sharethis-inline-share-buttons"></div>
                                        </div>
                                        <div class="post-tags">
                                            <span class="pt-title">Tags: </span>
                                            @foreach ($tags as $t)
                                                <a href="{{ route('news.search.tag', ['tag_title' => $t->safe_title]) }}">{{ $t->title }}</a>
                                            @endforeach
                                        </div>
                                        <div class="fb-comments" data-href="{{ url()->full() }}" data-width="100%" data-numposts="5"></div>
                                    </div>
                                </div>
                            </article>

                        </div>
                    </div>
                    <div class="col-md-12 col-lg-4 col-xl-4 mb-3">
                        <aside class="mb-3">
                            <section>
                                <header class="heading">
                                    <h3 class="title">Tin liên quan</h3>
                                </header>
                                <div class="list-article">
                                    @foreach($related as $item)
                                    <article>
                                        <div class="d-flex">
                                            <div class="m">
                                                <a href="{{ route('news.detail', ['alias' => $item->alias]) }}" title="{{ $item->title }}">
                                                    <img src="{{ $item->getImageUrl('medium') }}" alt="{{ $item->title }}">
                                                </a>
                                            </div>
                                            <div class="info">
                                                <header class="header-info">
                                                    <div class="meta">
                                                        <span class="eyebrow">
                                                            @foreach ($item->categories as $child_cate)
                                                                <a href="{{ route('news.list', ['p_cate' => $item->category['safe_title'], 'child_cate' => $child_cate->safe_title]) }}" class="mr-2">{{ $child_cate->title }}</a>
                                                            @endforeach
                                                        </span>
                                                    </div>
                                                    <h2 class="title sp-line-4">
                                                        <a href="{{ route('news.detail', ['alias' => $item->alias]) }}" title="{{ $item->title }}">{{ $item->title }}</a>
                                                    </h2>
                                                </header>
                                            </div>
                                        </div>
                                    </article>
                                    @endforeach
                                </div>
                            </section>
                        </aside>
                        {{-- <aside>
                            <section class="collection-video">
                                <header class="heading">
                                    <h3 class="title">Videos</h3>
                                </header>
                                <div class="list-article list-videos">
                                    <article>
                                        <div class="d-flex">
                                            <div class="m">
                                                <a href="#">
                                                    <img src="./images/sidebar_1.jpg" alt="">
                                                    <span class="pill duration">5:15</span>
                                                </a>

                                            </div>
                                            <div class="info">
                                                <header class="header-info">
                                                    <div class="meta">
                                                        <span class="eyebrow">
                                                            <a href="#">Lorem ryc</a>
                                                        </span>
                                                    </div>
                                                    <h2 class="title sp-line-4">
                                                        <a href="#">Hannity: Republicans must demand to know if whistleblower was deep state operative</a>
                                                    </h2>
                                                </header>
                                            </div>
                                        </div>
                                    </article>
                                    <article>
                                        <div class="d-flex">
                                            <div class="m">
                                                <a href="#">
                                                    <img src="./images/sidebar_2.jpg" alt="">
                                                    <span class="pill duration">5:15</span>
                                                </a>

                                            </div>
                                            <div class="info">
                                                <header class="header-info">
                                                    <div class="meta">
                                                        <span class="eyebrow">
                                                            <a href="#">Lorem ryc</a>
                                                        </span>
                                                    </div>
                                                    <h2 class="title sp-line-4">
                                                        <a href="#">Hannity: Republicans must demand to know if whistleblower was deep state operative</a>
                                                    </h2>
                                                </header>
                                            </div>
                                        </div>
                                    </article>
                                    <article>
                                        <div class="d-flex">
                                            <div class="m">
                                                <a href="#">
                                                    <img src="./images/sidebar_3.jpg" alt="">
                                                    <span class="pill duration">5:15</span>
                                                </a>

                                            </div>
                                            <div class="info">
                                                <header class="header-info">
                                                    <div class="meta">
                                                        <span class="eyebrow">
                                                            <a href="#">Lorem ryc</a>
                                                        </span>
                                                    </div>
                                                    <h2 class="title sp-line-4">
                                                        <a href="#">Hannity: Republicans must demand to know if whistleblower was deep state operative</a>
                                                    </h2>
                                                </header>
                                            </div>
                                        </div>
                                    </article>
                                    <article>
                                        <div class="d-flex">
                                            <div class="m">
                                                <a href="#">
                                                    <img src="./images/sidebar_4.jpg" alt="">
                                                    <span class="pill duration">5:15</span>
                                                </a>

                                            </div>
                                            <div class="info">
                                                <header class="header-info">
                                                    <div class="meta">
                                                        <span class="eyebrow">
                                                            <a href="#">Lorem ryc</a>
                                                        </span>
                                                    </div>
                                                    <h2 class="title sp-line-4">
                                                        <a href="#">Hannity: Republicans must demand to know if whistleblower was deep state operative</a>
                                                    </h2>
                                                </header>
                                            </div>
                                        </div>
                                    </article>
                                    <article>
                                        <div class="d-flex">
                                            <div class="m">
                                                <a href="#">
                                                    <img src="./images/sidebar_1.jpg" alt="">
                                                    <span class="pill duration">5:15</span>
                                                </a>

                                            </div>
                                            <div class="info">
                                                <header class="header-info">
                                                    <div class="meta">
                                                        <span class="eyebrow">
                                                            <a href="#">Lorem ryc</a>
                                                        </span>
                                                    </div>
                                                    <h2 class="title sp-line-4">
                                                        <a href="#">Hannity: Republicans must demand to know if whistleblower was deep state operative</a>
                                                    </h2>
                                                </header>
                                            </div>
                                        </div>
                                    </article>
                                    <article>
                                        <div class="d-flex">
                                            <div class="m">
                                                <a href="#">
                                                    <img src="./images/sidebar_2.jpg" alt="">
                                                    <span class="pill duration">5:15</span>
                                                </a>

                                            </div>
                                            <div class="info">
                                                <header class="header-info">
                                                    <div class="meta">
                                                        <span class="eyebrow">
                                                            <a href="#">Lorem ryc</a>
                                                        </span>
                                                    </div>
                                                    <h2 class="title sp-line-4">
                                                        <a href="#">Hannity: Republicans must demand to know if whistleblower was deep state operative</a>
                                                    </h2>
                                                </header>
                                            </div>
                                        </div>
                                    </article>
                                    <article>
                                        <div class="d-flex">
                                            <div class="m">
                                                <a href="#">
                                                    <img src="./images/sidebar_3.jpg" alt="">
                                                    <span class="pill duration">5:15</span>
                                                </a>

                                            </div>
                                            <div class="info">
                                                <header class="header-info">
                                                    <div class="meta">
                                                        <span class="eyebrow">
                                                            <a href="#">Lorem ryc</a>
                                                        </span>
                                                    </div>
                                                    <h2 class="title sp-line-4">
                                                        <a href="#">Hannity: Republicans must demand to know if whistleblower was deep state operative</a>
                                                    </h2>
                                                </header>
                                            </div>
                                        </div>
                                    </article>
                                    <article>
                                        <div class="d-flex">
                                            <div class="m">
                                                <a href="#">
                                                    <img src="./images/sidebar_4.jpg" alt="">
                                                    <span class="pill duration">5:15</span>
                                                </a>

                                            </div>
                                            <div class="info">
                                                <header class="header-info">
                                                    <div class="meta">
                                                        <span class="eyebrow">
                                                            <a href="#">Lorem ryc</a>
                                                        </span>
                                                    </div>
                                                    <h2 class="title sp-line-4">
                                                        <a href="#">Hannity: Republicans must demand to know if whistleblower was deep state operative</a>
                                                    </h2>
                                                </header>
                                            </div>
                                        </div>
                                    </article>
                                    <article>
                                        <div class="d-flex">
                                            <div class="m">
                                                <a href="#">
                                                    <img src="./images/sidebar_1.jpg" alt="">
                                                    <span class="pill duration">5:15</span>
                                                </a>

                                            </div>
                                            <div class="info">
                                                <header class="header-info">
                                                    <div class="meta">
                                                        <span class="eyebrow">
                                                            <a href="#">Lorem ryc</a>
                                                        </span>
                                                    </div>
                                                    <h2 class="title sp-line-4">
                                                        <a href="#">Hannity: Republicans must demand to know if whistleblower was deep state operative</a>
                                                    </h2>
                                                </header>
                                            </div>
                                        </div>
                                    </article>
                                    <article>
                                        <div class="d-flex">
                                            <div class="m">
                                                <a href="#">
                                                    <img src="./images/sidebar_2.jpg" alt="">
                                                    <span class="pill duration">5:15</span>
                                                </a>

                                            </div>
                                            <div class="info">
                                                <header class="header-info">
                                                    <div class="meta">
                                                        <span class="eyebrow">
                                                            <a href="#">Lorem ryc</a>
                                                        </span>
                                                    </div>
                                                    <h2 class="title sp-line-4">
                                                        <a href="#">Hannity: Republicans must demand to know if whistleblower was deep state operative</a>
                                                    </h2>
                                                </header>
                                            </div>
                                        </div>
                                    </article>
                                    <article>
                                        <div class="d-flex">
                                            <div class="m">
                                                <a href="#">
                                                    <img src="./images/sidebar_3.jpg" alt="">
                                                    <span class="pill duration">5:15</span>
                                                </a>

                                            </div>
                                            <div class="info">
                                                <header class="header-info">
                                                    <div class="meta">
                                                        <span class="eyebrow">
                                                            <a href="#">Lorem ryc</a>
                                                        </span>
                                                    </div>
                                                    <h2 class="title sp-line-4">
                                                        <a href="#">Hannity: Republicans must demand to know if whistleblower was deep state operative</a>
                                                    </h2>
                                                </header>
                                            </div>
                                        </div>
                                    </article>
                                    <article>
                                        <div class="d-flex">
                                            <div class="m">
                                                <a href="#">
                                                    <img src="./images/sidebar_4.jpg" alt="">
                                                    <span class="pill duration">5:15</span>
                                                </a>

                                            </div>
                                            <div class="info">
                                                <header class="header-info">
                                                    <div class="meta">
                                                        <span class="eyebrow">
                                                            <a href="#">Lorem ryc</a>
                                                        </span>
                                                    </div>
                                                    <h2 class="title sp-line-4">
                                                        <a href="#">Hannity: Republicans must demand to know if whistleblower was deep state operative</a>
                                                    </h2>
                                                </header>
                                            </div>
                                        </div>
                                    </article>
                                    <article>
                                        <div class="d-flex">
                                            <div class="m">
                                                <a href="#">
                                                    <img src="./images/sidebar_1.jpg" alt="">
                                                    <span class="pill duration">5:15</span>
                                                </a>

                                            </div>
                                            <div class="info">
                                                <header class="header-info">
                                                    <div class="meta">
                                                        <span class="eyebrow">
                                                            <a href="#">Lorem ryc</a>
                                                        </span>
                                                    </div>
                                                    <h2 class="title sp-line-4">
                                                        <a href="#">Hannity: Republicans must demand to know if whistleblower was deep state operative</a>
                                                    </h2>
                                                </header>
                                            </div>
                                        </div>
                                    </article>
                                    <article>
                                        <div class="d-flex">
                                            <div class="m">
                                                <a href="#">
                                                    <img src="./images/sidebar_2.jpg" alt="">
                                                    <span class="pill duration">5:15</span>
                                                </a>

                                            </div>
                                            <div class="info">
                                                <header class="header-info">
                                                    <div class="meta">
                                                        <span class="eyebrow">
                                                            <a href="#">Lorem ryc</a>
                                                        </span>
                                                    </div>
                                                    <h2 class="title sp-line-4">
                                                        <a href="#">Hannity: Republicans must demand to know if whistleblower was deep state operative</a>
                                                    </h2>
                                                </header>
                                            </div>
                                        </div>
                                    </article>
                                    <article>
                                        <div class="d-flex">
                                            <div class="m">
                                                <a href="#">
                                                    <img src="./images/sidebar_3.jpg" alt="">
                                                    <span class="pill duration">5:15</span>
                                                </a>

                                            </div>
                                            <div class="info">
                                                <header class="header-info">
                                                    <div class="meta">
                                                        <span class="eyebrow">
                                                            <a href="#">Lorem ryc</a>
                                                        </span>
                                                    </div>
                                                    <h2 class="title sp-line-4">
                                                        <a href="#">Hannity: Republicans must demand to know if whistleblower was deep state operative</a>
                                                    </h2>
                                                </header>
                                            </div>
                                        </div>
                                    </article>
                                    <article>
                                        <div class="d-flex">
                                            <div class="m">
                                                <a href="#">
                                                    <img src="./images/sidebar_4.jpg" alt="">
                                                    <span class="pill duration">5:15</span>
                                                </a>

                                            </div>
                                            <div class="info">
                                                <header class="header-info">
                                                    <div class="meta">
                                                        <span class="eyebrow">
                                                            <a href="#">Lorem ryc</a>
                                                        </span>
                                                    </div>
                                                    <h2 class="title sp-line-4">
                                                        <a href="#">Hannity: Republicans must demand to know if whistleblower was deep state operative</a>
                                                    </h2>
                                                </header>
                                            </div>
                                        </div>
                                    </article>
                                </div>
                            </section>
                        </aside> --}}
                    </div>
                </div>
            </div>
        </div>
    </main>

<!-- ====================
==================== -->


@endsection

@section('js_bot')
<script async defer crossorigin="anonymous" src="https://connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v4.0"></script>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
<script type="text/javascript" src="https://platform-api.sharethis.com/js/sharethis.js#property=5dc7e8150024c90012aa3f67&product=inline-share-buttons" async="async"></script>
@endsection
