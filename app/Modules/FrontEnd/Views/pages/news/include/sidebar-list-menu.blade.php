<div class="list">
    <ul>
        @if (!empty($category) && isset($relate))
            @foreach ($category as $item)
            <li>
                <a href="{{route('news.list', ['cate_news'=> \Illuminate\Support\Str::slug($item['title']), 'id' => $item['id']])}}">
                    <i class="fa fa-caret-right"></i> {{$item['title']}}
                </a>
            </li>
            @endforeach
        @else
        <li><a href="{{route('news.list', ['cate_news'=> str_slug('Top bài mới nhất'), 'id' => 'a101'])}}"><i class="fa fa-caret-right"></i> Mới nhất</a></li>
        <li><a href="{{route('news.list', ['cate_news'=> \Illuminate\Support\Str::slug('Top bài hót nhất'), 'id' => 'a102'])}}"><i class="fa fa-caret-right"></i> Hot nhất</a></li>
        @endif
    </ul>
</div>