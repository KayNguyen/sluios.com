<div class="col-12 col-sm-6 col-lg-4 blog-main-item">
    <div class="b-item">
        @if(!empty($item->id_videos))
        <div class="video-embed">
            <iframe src="https://player.vimeo.com/video/{{ $item->id_videos }}?byline=0&badge=0" width="100%" height="100%" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
        </div>
        @elseif(!empty($item->iframe_soundcloud))
        <div class="video-embed">
            {!! $item->iframe_soundcloud !!}
        </div>
        @else
        <a href="{{ route('news.detail', ['alias' => $item->alias]) }}" class="b-img">
            <img src="{{ $item->getImageUrl('original') }}" alt="">
        </a>
        @endif

        <a href="{{ route('news.detail', ['alias' => $item->alias]) }}" class="b-name sp-line-2">
            {{ $item->title }}
        </a>
        @if(!empty($item->sort_body))
        <p class="b-dec" style="height: 150px">
            {!! str_limit( strip_tags($item->sort_body), $limit = 250) !!}[...]
        </p>
        @endif
        <p class="b-author"><b>By :</b> <a href="javascript:;">{{ $item->authors->fullname }}</a></p>

        <span class="b-date">{{ \Lib::dateFormat($item->published, 'd/m/Y') }}</span>
        <a href="{{ route('news.list', ['alias' => str_slug($item->category->title)]) }}"><span class="b-cat">{{ $item->category->title }}</span></a>
        <a href="{{ route('news.detail', ['alias' => $item->alias]) }}" class="view-detail">xem chi tiết <i class="fa fa-caret-right" aria-hidden="true"></i></a>
    </div>
</div>