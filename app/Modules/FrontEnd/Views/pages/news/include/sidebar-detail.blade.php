@foreach ($item as $item)
    @if(!empty( $item->id))
    <a href="{{route('news.detail',['alias' => $item->alias])}}" class="home-post-item-small d-flex mb-3 d-md-block">
    <div class="thumbnail">
        <img src="{{\ImageURL::getImageUrl($item->image, 'news', 'large')}}" alt="">
    </div>
    <div class="body">
        <div class="title">{{$item->title}}</div>
        <div class="interact">
            <span class="rank"><i class="fa fa-sort-amount-asc"></i> {{$item->rank}}</span>
            <span class="seen"><i class="fa fa-eye"></i>{{$item->n_view}}</span>
            <span class="like"><i class="fa fa-heart"></i>{{$item->vote}}</span>
            @if(isset($total_comment_related[$item->id]))
                <span class="comment"><i class="fa fa-comments spec"></i> {{ $total_comment_related[$item->id]->comments}} </span>
            @else
                <span class="comment"><i class="fa fa-comments spec"></i> 0 </span>
            @endif
        </div>
    </div>
    </a>
    @endif
@endforeach