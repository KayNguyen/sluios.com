
@php
    if (isset($item->cate_id)) {
        $category_id = $item->cate_id;
    }

@endphp

<div class="cate-item blog-post-item">
    <a href="{{route('news.detail',['alias' => $item->alias])}}" class="wrap-thumbnail">
        <div class="thumbnail">
            <img src="{{\ImageURL::getImageUrl($item->image, 'news', 'large')}}" alt="">
        </div>
    </a>
    <div class="content">
        <a href="{{route('news.detail',['alias' => $item->alias])}}" class="title">
            {{$item->title}}
        </a>
        <div class="info d-flex justify-content-between">
            <div class="date"> <div class="around"></div> <span>{{Lib::dateFormat($item->published, 'd/m/Y') }}</span> </div>
            <div class="fb-share-button fb-share-custom" data-href="{{route('news.detail',['alias' => $item->alias])}}" data-layout="button_count" data-size="small">
                <a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u={{route('news.detail',['alias' => $item->alias])}};src=sdkpreparse" class="fb-xfbml-parse-ignore">Chia sẻ</a>
            </div>
        </div>
            <div class="interact">
            <span class="rank"><i class="fa fa-sort-amount-asc spec"></i>{{$item->rank}}</span>
            <span class="seen"><i class="fa fa-eye spec"></i>{{$item->n_view}}</span>
            <span class="like"><i class="fa fa-heart spec"></i>{{$item->vote}}</span>
            @if(isset($total_comment[$item->id]))
                <span class="comment"><i class="fa fa-comments spec"></i> {{ $total_comment[$item->id]->comments}} </span>
            @else
                <span class="comment"><i class="fa fa-comments spec"></i> 0 </span>
            @endif
        </div>
        <div class="des">{!! mb_substr($item->sort_body, 0) !!}</div>
    </div>
</div>