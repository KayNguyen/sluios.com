<div class="content-page-detail">
    <h1 class="title">{{$item->title}}</h1>
    <div class="directional">
        <span class="top" onclick="window.scrollTo(0, 0);"><i class="fa fa-arrow-up"></i></span>
        @for ($i = 1; $i <= count($item->detail); $i++)
        <span><a href="#content-{{$i}}">{{$i}}</a></span>
        @endfor
    </div>
    <div class="info d-flex justify-content-between">
        <div class="date"> <div class="around"></div> <span>{{Lib::dateFormat($item->published, 'd/m/Y') }}</span> </div>
        <div class="fb-share-button" data-href="{{url()->full()}}" data-layout="button_count" data-size="small"><a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u={{url()->full()}}&amp;src=sdkpreparse" class="fb-xfbml-parse-ignore">Chia sẻ</a></div>
    </div>
 
    <div class="interact">
        <span class="rank"><i class="fa fa-sort-amount-asc spec"></i> {{$rank->rank}}</span>
        <span class="seen"><i class="fa fa-eye spec"></i>{{$item->n_view}}</span>
        <span class="like"><i class="fa fa-heart spec"></i>{{$rank->vote}}</span>
        <span class="comment"><i class="fa fa-comments spec"></i> {{($total_comment) ? $total_comment : 0}} </span>
    </div>
    <div class="conduct">
        <h2 class="fs-18">{!! mb_substr($item->sort_body, 0) !!}</h2>
    </div>
    @foreach ($item->detail as $key => $item_body)
    <div class="main-content" id="content-{{++$key}}">
        <div class="pos">
            <div class="number"><span>{{$key}}</span></div>
            <div class="like">
                <a class="text-secondary" @if(\Auth::guard('customer')->check()) v-on:click="toVote({{$key-1}})" @else href="{{route('facebook.login')}}" @endif><i class="fa fa-heart"></i> @{{data[{!!$key-1!!}].vote}}</a>
            </div>
        </div>
        <div class="content">
        {!! mb_substr($item_body->body, 0) !!}
            @if(Auth::guard('customer')->check())
                @php($auth = \Auth::guard('customer')->user())
                <form action="{{route('post.comments', ['news_detail_id' => $item_body->id, 'new_id' => $item->id])}}" class="my-3" method="POST">
                    @csrf
                    <div class="media">
                        <div class="media-left mr-2">
                            <<!-- a title="{{$auth->fullname}}"><img style="width:40px; height: 40px;" class="rounded-circle user_avatar_link" src="{{asset('html-tophay/images/user-cmt-1.png')}}" alt="{{$auth->fullname}}"></a> -->
                        </div>
                        <div class="media-body">
                            <input type="hidden" name="obj[name]" value="{{$auth->id}}">
                            <textarea  name="obj[comment]" id="comment_content_16503_172042_0" class="text_data_commnet form-control" rows="2" placeholder="Viết bình luận của bạn"></textarea>
                            <button style="margin-top: 6px; display: inline-block;" type="submit" id="data_comment_btn" class="btn btn-primary btn_data_commnet"> Gửi</button>

                        </div>
                    </div>
                </form>
            @endif
            <div><p>Bình luận: </p></div>
            @foreach($comments as $cmt)
                @if($cmt->new_detail_id == $item_body->id)
                    <div class="comment_container mb-3">
                        <div class="media">
                            <div class="media-left">
                                <a name="comment_22479"></a>
                               <!--  <a title="{{$cmt->fullname}}"><img style="width:40px; height: 40px;" class="rounded-circle user_avatar_link" src="{{asset('html-tophay/images/user-cmt-'.rand(1, 2).'.png')}}" alt="{{$cmt->fullname}}"></a> -->
                            </div>
                            <div class="media-body ml-2">
                                <h5 class="media-heading fs-14"><a href="javascript:;">{{$cmt->fullname}}</a>
                                    <i class="fa fa-clock-o"></i> {{Lib::dateFormat($cmt->created, 'd-m-Y H:i') }}</h5>
                                {{$cmt->comment}}
                            </div>
                        </div>
                    </div>
                @endif
            @endforeach
        </div>
    </div>
    @endforeach

</div>
