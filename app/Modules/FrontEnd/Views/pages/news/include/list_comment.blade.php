@if (!empty($comment))
    <div class="list-rating col-12">
        <ul>
            @foreach ($comments as $cmt)
                <li class="rating-item">
                    {{$cmt['comment']}}
                </li>
            @endforeach
        </ul>
    </div>

@endif