@extends('FrontEnd::layouts.home', ['bodyClass' => 'has-cover'])

@section('title') {!! \Lib::siteTitle($site_title, $def['site_title']) !!} @stop

@section('content')

<main>
    <div class="home-page-content bg-white searching">
        <div class="container page-content">
            <div class="row">
                <div class="col-lg-12">
                    <div class="search-result-box m-t-30 card-box">
                        <div class="row">
                            <div class="col-md-8 offset-md-2">
                                <div class="pt-3 pb-4">
                                    <form action="{{ route('news.search') }}">
                                        <div class="input-group m-t-10">
                                            <input type="text" name="q" class="form-control" value="{{ @$keyword }}">
                                        <span class="input-group-append">
                                            <button type="submit" class="btn waves-effect waves-light btn-blue"><i class="fa fa-search mr-1"></i> Search</button>
                                        </span>
                                    </form>
                                    </div>
                                    <div class="mt-3 text-center">
                                        <h4>Search Results For "{{ @$keyword }}"</h4>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- end row -->

                        <ul class="nav nav-tabs nav-bordered">
                            <li class="nav-item">
                                <a href="#home" data-toggle="tab" aria-expanded="true" class="nav-link active">
                                    All results <span class="badge badge-success ml-1">{{ isset($data) ? $data->count() : 0 }}</span>
                                </a>
                            </li>
                            
                        </ul>

                        <div class="tab-content">
                            <div class="tab-pane active" id="home">
                                <div class="row">
                                    <div class="col-md-12">
                                        @if (isset($data))
                                        @foreach ($data as $item)
                                        <div class="search-item">
                                            <h4 class="mb-1"><a title="{{ $item->title }}" href="{{ route('news.detail', ['alias' => $item->alias]) }}">{{ $item->title }}</a></h4>
                                            <div class="font-13 mb-2 text-truncate">
                                                <a class="text-success" href="{{ route('news.detail', ['alias' => $item->alias]) }}">{{ env('APP_URL').'/'.$item->alias }}</a>
                                            </div>
                                            <p class="mb-0 text-muted">
                                                {!! ($item->description_seo) ? $item->description_seo : strip_tags($item->sort_body) !!}
                                            </p>
                                        </div> <!-- end search item -->
                                        @endforeach
                                        


                                        {{ $data->render('FrontEnd::layouts.pagin') }}
                                        @endif
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                            </div>

                            <!-- end All results tab -->


                        
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
@endsection