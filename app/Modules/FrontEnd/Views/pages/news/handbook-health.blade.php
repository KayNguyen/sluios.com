@extends('FrontEnd::layouts.home', ['bodyClass' => 'has-cover'])

@section('title') {!! \Lib::siteTitle($site_title, $def['site_title']) !!} @stop

@section('content')
    @if( count($errors) > 0)
        <div class="alert alert-danger">
            @foreach ($errors->all() as $error)
                <div>{!! $error !!}</div>
            @endforeach
        </div>
    @endif

    @if (session('status'))
        <div class="alert alert-success">
            {!! session('status') !!}
        </div>
    @endif
    <div class="handbook-banner">
        <img src="{{ asset('html-washfriends/images/banner-handbook.png') }}" alt="">
    </div>
    <div class="container diabetes-1 handbook__diabetes">
        <div class="handbook__diabetes__title">Bệnh tiểu đường</div>
        <div class="handbook__diabetes__status">
            <div class="content">
                <div class="desc text-center">
                    Thực tế cho thấy, để đạt hiệu quả ổn định đường huyết lâu dài và ngăn ngừa diễn tiến của biến chứng nguy hiểm người bệnh tiểu đường cần kết hợp với chế độ dinh dưỡng hợp lí và lối sống lành mạnh.
                </div>
            </div>

            <div class="wrap-slide">
                <div class="owl-carousel owl-theme directional sync-handbook-2">
                    @foreach($data as $key => $item)
                        <div class="item"><span>{{ $key }}</span></div>
                    @endforeach
                </div>
                <div class="owl-carousel owl-theme slide-handle-diabeters sync-handbook-1">
                    @foreach($data as $key => $item)
                        @foreach($item as $k => $v)
                            <div class="item">
                                <div class="vid">
                                    <div class="plyr__video-embed handbook-vid">
                                        <iframe
                                                src="{{ $v->link_videos }}"
                                                allowfullscreen
                                                allowtransparency
                                                allow="autoplay"
                                        ></iframe>
                                    </div>
                                </div>
                                <div class="cont">
                                    <a href="{{ route('news.detail', ['alias' => $v->alias]) }}" class="name">{{ $v->title }}</a>
                                    <div class="vid-des">{!! mb_substr($v->sort_body, 0) !!}</div>
                                </div>
                            </div>
                        @endforeach
                    @endforeach


                </div>
            </div>

        </div>
    </div>


    <div class="wrap-handbook-diabetes" style="background-image: url({{ asset('html-washfriends/images/handbook_bg.png') }})">
        <div class="container diabetes-2 handbook__diabetes">
            <div class="handbook__diabetes__title">Bệnh tiểu đường</div>
            <div class="handbook__diabetes__status">
                <div class="content">
                    <div class="desc text-center">
                        Đối với người bệnh tiểu đường, thực phẩm có chứa nhiều đường và khó tiêu là “kẻ thù số 1”. Dùng các thực phẩm này sẽ làm tình trạng bệnh thêm nghiêm trọng hơn.
                    </div>
                </div>

                <div class="wrap-slide">
                    <div class="owl-carousel owl-theme directional sync-handbook-2">
                        @foreach($data as $key => $item)
                            <div class="item"><span>{{ $key }}</span></div>
                        @endforeach
                    </div>
                    <div class="owl-carousel owl-theme slide-handle-diabeters sync-handbook-1">
                        @foreach($data as $key => $item)
                            @foreach($item as $k => $v)
                                <div class="item">
                                    <div class="vid">
                                        <div class="plyr__video-embed handbook-vid">
                                            <iframe
                                                    src="{{ $v->link_videos }}"
                                                    allowfullscreen
                                                    allowtransparency
                                                    allow="autoplay"
                                            ></iframe>
                                        </div>
                                    </div>
                                    <div class="cont">
                                        <a href="{{ route('news.detail', ['alias' => $v->alias]) }}" class="name">{{ $v->title }}</a>
                                        <div class="vid-des">{!! mb_substr($v->sort_body, 0) !!}</div>
                                    </div>
                                </div>
                            @endforeach
                        @endforeach


                    </div>
                </div>

            </div>
        </div>
    </div>
    @include('FrontEnd::pages.product.include.form_contact')
@endsection

