@extends('FrontEnd::layouts.home', ['bodyClass' => 'has-cover'])

@section('title') {!! \Lib::siteTitle($site_title, $def['site_title']) !!} @stop

@section('content')
    <div class="handbook-banner">
        <img src="{{ asset('html-washfriends/images/page_story_banner.png') }}" alt="">
    </div>
    <div class="container">
        <div class="story__customer">
            <div class="owl-carousel owl-theme sync1" data-items-slide="1" data-dots="false">
                @foreach($data as $item)
                <div class="story__customer--wrap">
                    <div class="story__customer--video">
                        <div class="row">
                            <div class="col-lg-8">
                                <div class="plyr__video-embed vid-story-customer" id="">
                                    <iframe width="100%" height="100%" src="{{ $item->link_videos }}" frameborder="0"
                                            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                                            allowfullscreen></iframe>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="story__customer__content">
                                    <div class="story__customer--title">
                                        <a href="{{ route('news.detail', ['alias' => $item->alias]) }}">{{ $item->title }}</a>
                                    </div>

                                    <div class="type">
                                        {{ $item->category->title }}
                                        <span>{{Lib::dateFormat($item->published, 'd/m/Y') }}</span>
                                    </div>
                                    <div class="desc">{!! mb_substr($item->sort_body, 0) !!}</div>
                                    <div class="views">
                                        <i class="fa fa-eye" aria-hidden="true"></i> {{ ($item->n_view) ? $item->n_view : 0 }} lượt xem
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>

            <div class="slider-sync">
                <div class="owl-carousel owl-theme story__customer--slide slider-genaral sync2" data-items-slide="4" data-dots="true">
                    @foreach($data as $item)
                    <div class="items">
                        <div class="story__customer--slide--images">
                            <img src="{{ \ImageURL::getImageUrl($item->image, 'news', 'large') }}" alt />
                        </div>
                        <div class="story__customer--slide--desc">
                            <div class="title-link">{{ $item->title }}</div>
                            <div class="desc-text">{!! mb_substr($item->sort_body, 0) !!}</div>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
        <div class="customer_share_section">
            @foreach($data as $item)
            <div class="customer__share__content">
                <div class="row">
                    <div class="col-md-4">
                        <div class="image">
                            <a href="javascript:;">
                                <img src="{{ \ImageURL::getImageUrl($item->image, 'news', 'large') }}" alt />
                            </a>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="desc">
                            <div class="link">
                                <a href="{{ route('news.detail', ['alias' => $item->alias]) }}">{{ $item->title }}</a>
                            </div>
                            <div class="date__time">{{Lib::dateFormat($item->published, 'd/m/Y') }}</div>
                            <div class="desc__detail">{!! mb_substr($item->sort_body, 0) !!}</div>
                        </div>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
        {{$data->render('FrontEnd::layouts.pagin')}}
    </div>
    <div class="page-story-customer-form-contact">
        <from_contact_product></from_contact_product>
    </div>
@endsection

