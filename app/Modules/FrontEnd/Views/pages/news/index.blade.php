@extends('FrontEnd::layouts.home', ['bodyClass' => 'has-cover'])

@section('title') {!! \Lib::siteTitle($site_title, $def['site_title']) !!} @stop

@section('content')
<main>
    @if($data->total() > 0)
    <div class="home-page-content">
        <div class="container page-content">
            <div class="row">
                <div class="col-md-12 col-lg-8 col-xl-8 mb-3">
                    <div class="row sticky-region mx-md-0">
                        <div class="main">
                            <div class="collection">
                                <div class="content">
                                    <article class="article story-1">
                                        <div class="m">
                                            <a href="{{ route('news.detail', ['alias' => $data[0]->alias, 'id' => $data[0]->id]) }}">
                                                <picture>
                                                    <img src="{{ $data[0]->getImageUrl('original') }}" alt="{{ $data[0]->title }}">
                                                </picture>
                                            </a>
                                        </div>
                                        <div class="info">
                                            <header class="info-header">
                                                <div class="meta">
                                                    <span class="eyebrow">
                                                        @foreach ($data[0]->categories as $child_cate)
                                                        <a href="{{ route('news.list', ['p_cate' => $data[0]->category['safe_title'], 'child_cate' => $child_cate->safe_title]) }}" class="mr-2">{{ $child_cate->title }}</a>
                                                        @endforeach
                                                    </span>
                                                </div>
                                                <h2 class="title sp-line-2">
                                                    <a href="{{ route('news.detail', ['alias' => $data[0]->alias, 'id' => $data[0]->id]) }}">{{ $data[0]->title }}</a>
                                                </h2>
                                            </header>
                                            <div class="content">
                                                <p class="dek sp-line-4">
                                                    {!! strip_tags($data[0]->sort_body) !!}
                                                </p>
                                                
                                            </div>
                                        </div>
                                    </article>

                                </div>
                            </div>
                            <div class="collection collection-spotlight">
                                <div class="content">
                                    <div class="row">
                                        @foreach ($data as $k => $item)
                                        @if ($k > 0)
                                            <div class="col-md-6">
                                                <article class="article story-1">
                                                    <div class="m">
                                                        <a href="{{ route('news.detail', ['alias' => $item->alias]) }}" title="{{ $item->title }}">
                                                            <picture>
                                                                <img src="{{ $item->getImageUrl('medium') }}" alt="{{ $item->title }}">
                                                            </picture>
                                                        </a>
                                                    </div>
                                                    <div class="info">
                                                        <header class="info-header">
                                                            <div class="meta">
                                                            <span class="eyebrow">
                                                                @foreach ($item->categories as $child_cate)
                                                                    <a href="{{ route('news.list', ['p_cate' => $item->category['safe_title'], 'child_cate' => $child_cate->safe_title]) }}" class="mr-2">{{ $child_cate->title }}</a>
                                                                @endforeach
                                                            </span>
                                                            </div>
                                                            <h2 class="title sp-line-2">
                                                                <a href="{{ route('news.detail', ['alias' => $item->alias]) }}" title="{{ $item->title }}">{{ $item->title }}</a>
                                                            </h2>
                                                        </header>
                                                        <div class="content">
                                                            <p class="dek sp-line-4">
                                                                {!! strip_tags($item->sort_body) !!}
                                                            </p>
                                                        </div>
                                                    </div>
                                                </article>
                                            </div>
                                            @if($k == 2) @break @endif
                                        @endif
                                        @endforeach
                                        
                                    </div>
                                </div>
                            </div>
                            <div class="collection collection-article-list">
                                <section class="border-0 p-0">
                                    <div class="content list-article">
                                        @foreach($data as $k => $item)
                                        @if($k > 2)
                                        <article class="article story-1">
                                            <div class="d-flex">
                                                <div class="m">
                                                    <a href="{{ route('news.detail', ['alias' => $item->alias]) }}" title="{{ $item->title }}">
                                                        <img src="{{ $item->getImageUrl('medium') }}" alt="{{ $item->title }}">
                                                    </a>
                                                </div>
                                                <div class="info">
                                                    <header class="header-info">
                                                        <div class="meta">
                                                        <span class="eyebrow">
                                                            @foreach ($item->categories as $child_cate)
                                                                <a href="{{ route('news.list', ['p_cate' => $item->category['safe_title'], 'child_cate' => $child_cate->safe_title]) }}" class="mr-2">{{ $child_cate->title }}</a>
                                                            @endforeach
                                                        </span>
                                                        </div>
                                                        <h2 class="title sp-line-2">
                                                            <a href="{{ route('news.detail', ['alias' => $item->alias]) }}" title="{{ $item->title }}">{{ $item->title }}</a>
                                                        </h2>
                                                    </header>
                                                </div>
                                            </div>
                                        </article>
                                        @endif
                                        @endforeach
                                    </div>
                                    @if($data->total() > 10)
                                    <div class="load-more">
                                        <a href="" class="btn btn-block bg-indigo">Show more</a>
                                    </div>
                                    @endif
                                </section>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12 col-lg-4 col-xl-4 mb-3">
                    <aside class="mb-3">
                        <section>
                            <header class="heading">
                                <h3 class="title">Trending</h3>
                            </header>
                            <div class="list-article">
                                @foreach($listTrend as $item)
                                <article>
                                    <div class="d-flex">
                                        <div class="m">
                                            <a href="{{ route('news.detail', ['alias' => $item->alias]) }}" title="{{ $item->title }}">
                                                <img src="{{ $item->getImageUrl('medium') }}" alt="{{ $item->title }}">
                                            </a>
                                        </div>
                                        <div class="info">
                                            <header class="header-info">
                                                <div class="meta">
                                                    <span class="eyebrow">
                                                        @foreach ($item->categories as $child_cate)
                                                            <a href="{{ route('news.list', ['p_cate' => $item->category['safe_title'], 'child_cate' => $child_cate->safe_title]) }}" class="mr-2">{{ $child_cate->title }}</a>
                                                        @endforeach
                                                    </span>
                                                </div>
                                                <h2 class="title sp-line-4">
                                                    <a href="{{ route('news.detail', ['alias' => $item->alias]) }}" title="{{ $item->title }}">{{ $item->title }}</a>
                                                </h2>
                                            </header>
                                        </div>
                                    </div>
                                </article>
                                @endforeach
                            </div>
                        </section>
                    </aside>
                    <aside>
                        <section class="collection-video">
                            <header class="heading">
                                <h3 class="title">Videos</h3>
                            </header>
                            <div class="list-article list-videos">
                                <article>
                                    <div class="d-flex">
                                        <div class="m">
                                            <a href="#">
                                                <img src="./images/sidebar_1.jpg" alt="">
                                                <span class="pill duration">5:15</span>
                                            </a>

                                        </div>
                                        <div class="info">
                                            <header class="header-info">
                                                <div class="meta">
                                                    <span class="eyebrow">
                                                        <a href="#">Lorem ryc</a>
                                                    </span>
                                                </div>
                                                <h2 class="title sp-line-4">
                                                    <a href="#">Hannity: Republicans must demand to know if whistleblower was deep state operative</a>
                                                </h2>
                                            </header>
                                        </div>
                                    </div>
                                </article>
                                <article>
                                    <div class="d-flex">
                                        <div class="m">
                                            <a href="#">
                                                <img src="./images/sidebar_2.jpg" alt="">
                                                <span class="pill duration">5:15</span>
                                            </a>

                                        </div>
                                        <div class="info">
                                            <header class="header-info">
                                                <div class="meta">
                                                    <span class="eyebrow">
                                                        <a href="#">Lorem ryc</a>
                                                    </span>
                                                </div>
                                                <h2 class="title sp-line-4">
                                                    <a href="#">Hannity: Republicans must demand to know if whistleblower was deep state operative</a>
                                                </h2>
                                            </header>
                                        </div>
                                    </div>
                                </article>
                                <article>
                                    <div class="d-flex">
                                        <div class="m">
                                            <a href="#">
                                                <img src="./images/sidebar_3.jpg" alt="">
                                                <span class="pill duration">5:15</span>
                                            </a>

                                        </div>
                                        <div class="info">
                                            <header class="header-info">
                                                <div class="meta">
                                                    <span class="eyebrow">
                                                        <a href="#">Lorem ryc</a>
                                                    </span>
                                                </div>
                                                <h2 class="title sp-line-4">
                                                    <a href="#">Hannity: Republicans must demand to know if whistleblower was deep state operative</a>
                                                </h2>
                                            </header>
                                        </div>
                                    </div>
                                </article>
                                <article>
                                    <div class="d-flex">
                                        <div class="m">
                                            <a href="#">
                                                <img src="./images/sidebar_4.jpg" alt="">
                                                <span class="pill duration">5:15</span>
                                            </a>

                                        </div>
                                        <div class="info">
                                            <header class="header-info">
                                                <div class="meta">
                                                    <span class="eyebrow">
                                                        <a href="#">Lorem ryc</a>
                                                    </span>
                                                </div>
                                                <h2 class="title sp-line-4">
                                                    <a href="#">Hannity: Republicans must demand to know if whistleblower was deep state operative</a>
                                                </h2>
                                            </header>
                                        </div>
                                    </div>
                                </article>
                                <article>
                                    <div class="d-flex">
                                        <div class="m">
                                            <a href="#">
                                                <img src="./images/sidebar_1.jpg" alt="">
                                                <span class="pill duration">5:15</span>
                                            </a>

                                        </div>
                                        <div class="info">
                                            <header class="header-info">
                                                <div class="meta">
                                                    <span class="eyebrow">
                                                        <a href="#">Lorem ryc</a>
                                                    </span>
                                                </div>
                                                <h2 class="title sp-line-4">
                                                    <a href="#">Hannity: Republicans must demand to know if whistleblower was deep state operative</a>
                                                </h2>
                                            </header>
                                        </div>
                                    </div>
                                </article>
                                <article>
                                    <div class="d-flex">
                                        <div class="m">
                                            <a href="#">
                                                <img src="./images/sidebar_2.jpg" alt="">
                                                <span class="pill duration">5:15</span>
                                            </a>

                                        </div>
                                        <div class="info">
                                            <header class="header-info">
                                                <div class="meta">
                                                    <span class="eyebrow">
                                                        <a href="#">Lorem ryc</a>
                                                    </span>
                                                </div>
                                                <h2 class="title sp-line-4">
                                                    <a href="#">Hannity: Republicans must demand to know if whistleblower was deep state operative</a>
                                                </h2>
                                            </header>
                                        </div>
                                    </div>
                                </article>
                                <article>
                                    <div class="d-flex">
                                        <div class="m">
                                            <a href="#">
                                                <img src="./images/sidebar_3.jpg" alt="">
                                                <span class="pill duration">5:15</span>
                                            </a>

                                        </div>
                                        <div class="info">
                                            <header class="header-info">
                                                <div class="meta">
                                                    <span class="eyebrow">
                                                        <a href="#">Lorem ryc</a>
                                                    </span>
                                                </div>
                                                <h2 class="title sp-line-4">
                                                    <a href="#">Hannity: Republicans must demand to know if whistleblower was deep state operative</a>
                                                </h2>
                                            </header>
                                        </div>
                                    </div>
                                </article>
                                <article>
                                    <div class="d-flex">
                                        <div class="m">
                                            <a href="#">
                                                <img src="./images/sidebar_4.jpg" alt="">
                                                <span class="pill duration">5:15</span>
                                            </a>

                                        </div>
                                        <div class="info">
                                            <header class="header-info">
                                                <div class="meta">
                                                    <span class="eyebrow">
                                                        <a href="#">Lorem ryc</a>
                                                    </span>
                                                </div>
                                                <h2 class="title sp-line-4">
                                                    <a href="#">Hannity: Republicans must demand to know if whistleblower was deep state operative</a>
                                                </h2>
                                            </header>
                                        </div>
                                    </div>
                                </article>
                                <article>
                                    <div class="d-flex">
                                        <div class="m">
                                            <a href="#">
                                                <img src="./images/sidebar_1.jpg" alt="">
                                                <span class="pill duration">5:15</span>
                                            </a>

                                        </div>
                                        <div class="info">
                                            <header class="header-info">
                                                <div class="meta">
                                                    <span class="eyebrow">
                                                        <a href="#">Lorem ryc</a>
                                                    </span>
                                                </div>
                                                <h2 class="title sp-line-4">
                                                    <a href="#">Hannity: Republicans must demand to know if whistleblower was deep state operative</a>
                                                </h2>
                                            </header>
                                        </div>
                                    </div>
                                </article>
                                <article>
                                    <div class="d-flex">
                                        <div class="m">
                                            <a href="#">
                                                <img src="./images/sidebar_2.jpg" alt="">
                                                <span class="pill duration">5:15</span>
                                            </a>

                                        </div>
                                        <div class="info">
                                            <header class="header-info">
                                                <div class="meta">
                                                    <span class="eyebrow">
                                                        <a href="#">Lorem ryc</a>
                                                    </span>
                                                </div>
                                                <h2 class="title sp-line-4">
                                                    <a href="#">Hannity: Republicans must demand to know if whistleblower was deep state operative</a>
                                                </h2>
                                            </header>
                                        </div>
                                    </div>
                                </article>
                                <article>
                                    <div class="d-flex">
                                        <div class="m">
                                            <a href="#">
                                                <img src="./images/sidebar_3.jpg" alt="">
                                                <span class="pill duration">5:15</span>
                                            </a>

                                        </div>
                                        <div class="info">
                                            <header class="header-info">
                                                <div class="meta">
                                                    <span class="eyebrow">
                                                        <a href="#">Lorem ryc</a>
                                                    </span>
                                                </div>
                                                <h2 class="title sp-line-4">
                                                    <a href="#">Hannity: Republicans must demand to know if whistleblower was deep state operative</a>
                                                </h2>
                                            </header>
                                        </div>
                                    </div>
                                </article>
                                <article>
                                    <div class="d-flex">
                                        <div class="m">
                                            <a href="#">
                                                <img src="./images/sidebar_4.jpg" alt="">
                                                <span class="pill duration">5:15</span>
                                            </a>

                                        </div>
                                        <div class="info">
                                            <header class="header-info">
                                                <div class="meta">
                                                    <span class="eyebrow">
                                                        <a href="#">Lorem ryc</a>
                                                    </span>
                                                </div>
                                                <h2 class="title sp-line-4">
                                                    <a href="#">Hannity: Republicans must demand to know if whistleblower was deep state operative</a>
                                                </h2>
                                            </header>
                                        </div>
                                    </div>
                                </article>
                                <article>
                                    <div class="d-flex">
                                        <div class="m">
                                            <a href="#">
                                                <img src="./images/sidebar_1.jpg" alt="">
                                                <span class="pill duration">5:15</span>
                                            </a>

                                        </div>
                                        <div class="info">
                                            <header class="header-info">
                                                <div class="meta">
                                                    <span class="eyebrow">
                                                        <a href="#">Lorem ryc</a>
                                                    </span>
                                                </div>
                                                <h2 class="title sp-line-4">
                                                    <a href="#">Hannity: Republicans must demand to know if whistleblower was deep state operative</a>
                                                </h2>
                                            </header>
                                        </div>
                                    </div>
                                </article>
                                <article>
                                    <div class="d-flex">
                                        <div class="m">
                                            <a href="#">
                                                <img src="./images/sidebar_2.jpg" alt="">
                                                <span class="pill duration">5:15</span>
                                            </a>

                                        </div>
                                        <div class="info">
                                            <header class="header-info">
                                                <div class="meta">
                                                    <span class="eyebrow">
                                                        <a href="#">Lorem ryc</a>
                                                    </span>
                                                </div>
                                                <h2 class="title sp-line-4">
                                                    <a href="#">Hannity: Republicans must demand to know if whistleblower was deep state operative</a>
                                                </h2>
                                            </header>
                                        </div>
                                    </div>
                                </article>
                                <article>
                                    <div class="d-flex">
                                        <div class="m">
                                            <a href="#">
                                                <img src="./images/sidebar_3.jpg" alt="">
                                                <span class="pill duration">5:15</span>
                                            </a>

                                        </div>
                                        <div class="info">
                                            <header class="header-info">
                                                <div class="meta">
                                                    <span class="eyebrow">
                                                        <a href="#">Lorem ryc</a>
                                                    </span>
                                                </div>
                                                <h2 class="title sp-line-4">
                                                    <a href="#">Hannity: Republicans must demand to know if whistleblower was deep state operative</a>
                                                </h2>
                                            </header>
                                        </div>
                                    </div>
                                </article>
                                <article>
                                    <div class="d-flex">
                                        <div class="m">
                                            <a href="#">
                                                <img src="./images/sidebar_4.jpg" alt="">
                                                <span class="pill duration">5:15</span>
                                            </a>

                                        </div>
                                        <div class="info">
                                            <header class="header-info">
                                                <div class="meta">
                                                    <span class="eyebrow">
                                                        <a href="#">Lorem ryc</a>
                                                    </span>
                                                </div>
                                                <h2 class="title sp-line-4">
                                                    <a href="#">Hannity: Republicans must demand to know if whistleblower was deep state operative</a>
                                                </h2>
                                            </header>
                                        </div>
                                    </div>
                                </article>
                            </div>
                        </section>
                    </aside>
                </div>
            </div>
            <div class="row">
                @foreach($relCate as $key => $rel)
                <div class="col-md-12 mb-3">
                    <div class="row sticky-region mx-md-0">
                        <div class="collection collection-list collection-article-list">
                            <header class="heading">
                                <h3 class="title">{{ $key }}</h3>
                                <div class="more"><a href="{{ url()->full() .'/'. str_slug($key) }}">View All</a></div>
                            </header>
                            <section class="border-0 p-0">
                                <div class="content list-article list-article-plist">
                                    @foreach ($rel as $item)
                                    <article class="article  story-1">
                                        <div>
                                            <div class="m">
                                                <a href="{{ route('news.detail', ['alias' => $item->alias]) }}">
                                                    <img src="{{ $item->getImageUrl('medium')}} " alt="{{ $item->title }}" title="{{ $item->title }}">
                                                </a>
                                            </div>
                                            <div class="info">
                                                <header class="header-info">
                                                    <div class="meta">
                                                    <span class="eyebrow">
                                                        <a href="{{ url()->full() .'/'. str_slug($key) }}" title="{{ $key }}">{{ $key }}</a>
                                                    </span>
                                                    </div>
                                                    <h2 class="title sp-line-2">
                                                        <a href="{{ route('news.detail', ['alias' => $item->alias]) }}" title="{{ $item->title }}">{{ $item->title }}</a>
                                                    </h2>
                                                </header>
                                            </div>
                                        </div>
                                    </article>
                                    @endforeach
                                </div>
                            </section>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </div>
    @endif
</main>
@endsection

