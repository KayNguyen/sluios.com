@extends('FrontEnd::layouts.home', ['bodyClass' => 'homepage'])

@section('title') {!! \Lib::siteTitle($site_title, $def['site_title']) !!} @stop

@section('content')
<main id="vote">
    <div class="container">
        {!! \Lib::renderBreadcrumb(false, true,'FrontEnd::layouts.breadcrumb') !!}

        <div class="row">
            <div class="col-12 col-lg-9">
                <div class="emtry-detail">
                    <div class="content-page-detail">
                        <h1 class="title">{{$data->title}}</h1>

                        <div class="conduct">
                            <h2 class="fs-18">{!! mb_substr($data->sort_body, 0) !!}</h2>
                        </div>
                        <div class="main-content">
                            <div class="content">
                                {!! mb_substr($data->body, 0) !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>

@endsection
