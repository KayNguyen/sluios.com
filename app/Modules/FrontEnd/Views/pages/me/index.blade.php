@extends('FrontEnd::layouts.home', ['bodyClass' => 'has-cover'])

@section('title') {!! \Lib::siteTitle($site_title, $def['site_title']) !!} @stop

@section('content')

<main>

    <!-- ================
    Banner
    ================ -->
    <section class="slider_home">
        <div class="js-carousel" data-items="1" data-arrows="false">
            @foreach($slide as $sl)
            <div class="home-slider-item">
                <a href="{{ $sl->link }}">
                    <img src="{{ $sl->getImageUrl('original', 'image') }}" alt="">
                </a>
            </div>
            @endforeach

        </div>
    </section>
    <!-- ================
    //Banner
    ================ -->
        <style>
            .about-item-text li {
                list-style: unset;
            }
        </style>
    <!-- ================
    Content
    ================ -->
    <section class="about_content container">
        @foreach ($data as $item)
        <div class="row about-item">
            <div class="col-12 col-lg-6">
                <figure class="about-item-img">
                    <img src="{{ $item->getImageUrl('original', 'image') }}" alt="">
                </figure>
            </div>
            <div class="col-12 col-lg-6">
                <div class="about-item-text">
                    <h6>{{ $item->title }} :</h6>
                    @if(!empty($item->body))
                    <p class="b-dec" id="content">
                        {!! $item->body !!}
                    </p>
                    @endif
                </div>
            </div>
        </div>
        @endforeach
        
    </section>
    <!-- ================
    //Content
    ================ -->


    <!-- ================
    banner botton
    ================ -->

    <section class="slider_home">
        <div class="js-carousel" data-items="1" data-arrows="false">
            @foreach($slide as $sl)
            @if($loop->last)
                <div class="home-slider-item">
                    <a href="{{ $sl->link }}">
                        <img src="{{ $sl->getImageUrl('original', 'image') }}" alt="">
                    </a>
                </div>
            @endif
            @endforeach

        </div>
    </section>

    <!-- ================
    //banner botton
    ================ -->

</main>
@endsection

