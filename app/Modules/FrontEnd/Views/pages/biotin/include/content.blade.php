<div class="biotin-content-page">
    <div class="sec-1 container">
        <h2 class="title">
            Những lầm tưởng về Biotin
        </h2>
        <div class="wrap-content">
            <div class="content">
                <div class="wrap">
                    <div class="num">01</div>
                    <div class="des">Biotin là một chất hóa học được bổ sung vào các sản phẩm chăm sóc tóc và móng
                        tay, giúp mượt tóc và nuôi móng cứng cáp</div>
                    <div class="quote">
                            <span>Biotin hoàn toàn không phải là chất hóa học do con người tạo ra. Tinh
                                chất này là một loại vitamin nhóm B có lợi cho cơ thể, được hấp thụ thông qua các loại
                                thực phẩm, tuy nhiên, cơ thể không dự trữ được Biotin</span>
                    </div>
                </div>
                <div class="wrap-img">
                    <img src="{{ asset('html-washfriends/images/biotin-chemistry-1.png') }}" alt="">
                </div>
            </div>
            <div class="che">
                <img src="{{ asset('html-washfriends/images/biotin_chemistry.png') }}" alt="">
            </div>
        </div>
    </div>
    <div class="sec-2">
        <div class="sec-2-wrap">
            <div class="wrap-img">
                <img src="{{ asset('html-washfriends/images/danger_biotin.png') }}" alt="">
            </div>
            <div class="box">
                <div class="num">02</div>
                <div class="des"><span>Uống nhầm Biotin sẽ rất có hại cho cơ thể, chỉ được dùng ngoài da</span>
                </div>
                <div class="quote">
                        <span>Tinh chất biotin có trong dầu gội, dầu xả và các sản phẩm làm đẹp thì hoàn toàn
                            không thể uống được. Tinh chất Biotin được thêm vào các sản phẩm thực phẩm chức năng, hỗ trợ
                            sức
                            khỏe thì chúng ta hoàn toàn uống được.</span>
                </div>
            </div>
        </div>
    </div>
    <div class="sec-3">
        <div class="wrap-content container">
            <div class="content">
                <div class="wrap">
                    <div class="num">03</div>
                    <div class="des">Nếu Biotin có thể ổn định đường huyết thì không cần dùng thuốc hoặc sản phẩm khác</div>
                    <div class="quote">
                            <span>Biotin hoàn toàn không phải là chất hóa học do con người tạo ra. Tinh
                                chất này là một loại vitamin nhóm B có lợi cho cơ thể, được hấp thụ thông qua các loại
                                thực phẩm, tuy nhiên, cơ thể không dự trữ được Biotin</span>
                    </div>
                </div>
                <div class="wrap-img">
                    <img src="{{ asset('html-washfriends/images/book_biotin.png') }}" alt="">
                </div>
            </div>
            <div class="che">
                <img src="{{ asset('html-washfriends/images/doctor_biotin.png') }}" alt="">
            </div>
        </div>
    </div>
</div>