@extends('FrontEnd::layouts.home', ['bodyClass' => 'has-cover'])

@section('title') {!! \Lib::siteTitle($site_title, $def['site_title']) !!} @stop

@section('content')
    <div class="container">
        <div class="solution__biotin__blog scientific__research">
            <div class="row">
                @foreach($data as $v)
                    <div class="col-md-4">
                        <div class="blog__wrap">
                            <div class="image">
                                <img src="{{ \ImageURL::getImageUrl($v->image, 'news', 'large') }}" alt srcset />
                            </div>
                            <div class="desc">
                                <div class="date__time">{{Lib::dateFormat($v->published, 'd/m/Y') }}</div>
                                <h4>
                                    <a href="{{ route('news.detail', ['alias' => $v->alias]) }}">{{ $v->title }}</a>
                                </h4>
                                <div>{!! mb_substr($v->sort_body, 0) !!}</div>
                                <div class="btn-research">
                                    <a href="{{ route('news.detail', ['alias' => $v->alias]) }}">
                                        Tìm hiểu thêm
                                        <i class="fa fa-long-arrow-right" aria-hidden="true"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
        <div class="solution__biotin__blog scientific__slide">
            <div class="js-carousel box-suggest" data-items="3" data-margin="25" data-dots="false" data-loop="true">
                @foreach($data as $k => $v)
                    <div class="slide">
                        <div class="blog__wrap">
                            <div class="image">
                                <img src="{{ \ImageURL::getImageUrl($v->image, 'news', 'large') }}" alt srcset />
                            </div>
                            <div class="desc">
                                <div class="date__time">{{Lib::dateFormat($v->published, 'd/m/Y') }}</div>
                                <h4>
                                    <a href="{{ route('news.detail', ['alias' => $v->alias]) }}">{{ $v->title }}</a>
                                </h4>
                                <div class="sort-body">{!! mb_substr($v->sort_body, 0) !!}</div>
                                <div class="btn-research">
                                    <a href="{{ route('news.detail', ['alias' => $v->alias]) }}">
                                        Tìm hiểu thêm
                                        <i class="fa fa-long-arrow-right" aria-hidden="true"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
@endsection

