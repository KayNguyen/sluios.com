@extends('FrontEnd::layouts.home', ['bodyClass' => 'has-cover'])

@section('title') {!! \Lib::siteTitle($site_title, $def['site_title']) !!} @stop

@section('content')
    <div class="banner-biotin" style="background-image: url({{ asset('html-washfriends/images/biotin_banner.png') }})">
        <div class="container">
            <div class="box">
                <div class="title">
                    <span>Bổ sung tinh chất Biotin</span>
                    <span>Tăng hiệu quả ổn định đường huyết</span>
                </div>
                <div class="content">
                    <span>Bảo vệ và phục hồi chức năng tuyến tụy</span>
                    <span>Tăng cường khả năng ổn định đường huyết</span>
                    <span>Tăng hấp thu, chuyển hóa giảm nguy cơ thiếu hụt dinh dưỡng</span>
                </div>
            </div>
        </div>
    </div>
    @include('FrontEnd::pages.biotin.include.content')
    <div class="biotin_box_sugar_blood biotin-page">
        <h2 class="title text-center">Tại sao Biotin lại quan trọng với bệnh nhân tiểu đường ?</h2>
        <div class="content">
            <div class="desc text-center">
                Tiểu đường là một bệnh rối loạn chuyển hóa, Biotin (Vitamin H/B7) là một vitamin, coenzyme có vai trò quan trọng trong dây chuyền hấp thụ và chuyển hóa của cơ thể. Khi cơ thể có đủ lượng biotin cần thiết sẽ giúp thúc đẩy hoạt động chuyển hóa tốt hơn, kích thích hormon điều hòa của tuyến tụy làm việc hiệu quả, điều chỉnh các rối loạn, giải phòng đường và chất béo dư thừa
            </div>
        </div>
        <img src="images/bg-box-solution-mobile.png" alt="" class="bg-mobile">
    </div>
    @foreach($data as $key => $item)
    <div class="biotin-page container">
        <div class="solution__biotin__blog scientific__research">
            <h2 class="title text-center">{{ $key }}</h2>
            <div class="row">
                @foreach($item as $k => $v)
                <div class="col-md-4">
                    <div class="blog__wrap">
                        <div class="image">
                            <img src="{{ \ImageURL::getImageUrl($v->image, 'news', 'large') }}" alt srcset />
                        </div>
                        <div class="desc">
                            <h4>
                                <a href="{{ route('news.detail', ['alias' => $v->alias]) }}">{{ $v->title }}</a>
                            </h4>
                            <div class="sort-body">{!! mb_substr($v->sort_body, 0) !!}</div>login
                            <div class="btn-research">
                                <a href="{{ route('news.detail', ['alias' => $v->alias]) }}">
                                    Tìm hiểu thêm
                                    <i class="fa fa-long-arrow-right" aria-hidden="true"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </div>
    @endforeach
    @php($slider = array_slice($data, 0, 1))
    @foreach($slider as $index => $sli)
    <div class="biotin-page container">
        <h2 class="title text-center">{{ $index }}</h2>
        <div class="solution__biotin__blog scientific__slide">
            <div class="js-carousel box-suggest" data-items="3" data-margin="25" data-dots="false" data-loop="true">
                @foreach($sli as $k => $v)
                <div class="slide">
                    <div class="blog__wrap">
                        <div class="image">
                            <img src="{{ \ImageURL::getImageUrl($v->image, 'news', 'large') }}" alt srcset />
                        </div>
                        <div class="desc">
                            <div class="date__time">{{Lib::dateFormat($v->published, 'd/m/Y') }}</div>
                            <h4>
                                <a href="{{ route('news.detail', ['alias' => $v->alias]) }}">{{ $v->title }}</a>
                            </h4>
                            <div class="sort-body">{!! mb_substr($v->sort_body, 0) !!}</div>
                            <div class="btn-research">
                                <a href="{{ route('news.detail', ['alias' => $v->alias]) }}">
                                    Tìm hiểu thêm
                                    <i class="fa fa-long-arrow-right" aria-hidden="true"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </div>
    @endforeach

    <div class="biotin-more">
        <div class="wrap-bg">
            <img src="{{ asset('html-washfriends/images/biotin_more_bg.png') }}" alt="">
        </div>
        <div class="biotin-more-desc">
            <div class="box">
                <h2 class="title">Làm sao để bổ sung Biotin?</h2>
                <div class="quote"><span>Biotin là vitamin tan trong nước nên không thể dự trữ trong cơ thể, tinh chất
                        này được tổng hợp và sử dụng vừa đủ cho quá trình chuyển hóa hằng ngày. Những người bị tiểu
                        đường, chức năng tổng hợp và hấp thụ biotin kém đi, vì vậy, việc bổ sung biotin bằng thực phẩm
                        hằng ngày là không đủ</span></div>
            </div>
        </div>
        <div class="wrap-img">
            <img src="{{ asset('html-washfriends/images/biotin_more.png') }}" alt="">
        </div>
    </div>
    <div class="biotin-append">
        <div class="container">
            <div class="wrap-book">
                <div class="wrap-img">
                    <img src="{{ asset('html-washfriends/images/biotin_append.png') }}" alt="">
                </div>
                <h2 class="title">Các cách bổ sung biotin khác</h2>
            </div>
            <div class="wrap-box">
                <div class="box">
                    <div class="quote">
                        <span>Cách tốt nhất để người bị tiểu đường có đủ lượng biotin cần thiết (150 –
                            300mcg) mỗi ngày mà vẫn đảm bảo an toàn là dùng vitamin, viên uống bitoin hoặc thực phẩm bảo
                            vệ sức khỏe có kết hợp thành phẩn biotin</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="solution__biotin__blog related-post">
        <div class="container">
            <div class="row">
                @foreach($related as $v)
                <div class="col-md-4">
                    <div class="blog__wrap">
                        <div class="image">
                            <img src="{{ \ImageURL::getImageUrl($v->image, 'news', 'large') }}" alt srcset />
                        </div>
                        <div class="desc">
                            <div class="date__time">{{Lib::dateFormat($v->published, 'd/m/Y') }}</div>
                            <h4>
                                <a href="{{ route('news.detail', ['alias' => $v->alias]) }}">{{ $v->title }}</a>
                            </h4>
                            <div class="sort-body">{!! mb_substr($v->sort_body, 0) !!}</div>
                            <div class="btn-research">
                                <a href="{{ route('news.detail', ['alias' => $v->alias]) }}">
                                    Tìm hiểu thêm
                                    <i class="fa fa-long-arrow-right" aria-hidden="true"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </div>
@endsection

