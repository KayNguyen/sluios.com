@extends('FrontEnd::layouts.home', ['bodyClass' => 'has-cover'])

@section('title') {!! \Lib::siteTitle($site_title, $def['site_title']) !!} @stop

@section('content')
    <main>
        <div class="home-page-content">
            <div class="container page-content">
                <div class="video-block section-padding">
                    <div class="row">
                        <div class="col-md-8">
                            <div class="single-video-left">
                                <div class="single-video">
                                    <iframe width="100%" height="315" src="https://www.youtube-nocookie.com/embed/{{ $data->id }}?rel=0&amp;controls=0&amp;" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen=""></iframe>
                                </div>
                                <div class="single-video-title box mb-3">
                                    <h2><a href="#">{{ $data->snippet->title }}</a></h2>
                                    <p class="mb-0"><i class="fas fa-eye"></i> {{ \Lib::numberFormat($data->statistics->viewCount, ',') }} views</p>
                                </div>
                                
                                <div class="single-video-info-content box mb-3">
                                    <h6>About :</h6>
                                        <pre>{{ $data->snippet->description }}</pre>
                                    <h6>Tags :</h6>
                                    <p class="tags mb-0">
                                        @if(!empty($data->snippet->tags))
                                        @foreach ($data->snippet->tags as $tag)
                                            <span class="mb-1 d-inline-block"><a href="#">{{ $tag }}</a></span>
                                        @endforeach
                                        @endif
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            @if(!empty($related))
                            @foreach ($related as $key => $rel)
                            <div class="single-video-right">
                                <div class="row">

                                    <div class="col-md-12">
                                        @if($rel->total() > 0)
                                        @foreach ($rel as $item)
                                        @php($videos = Youtube::getVideoInfo($item->videos_id))
                                            <div class="video-card video-card-list">
                                                <div class="video-card-image">
                                                    <a class="play-icon" href="{{ route('videos.detail', ['alias' => $item->videos_id]) }}"><img src="{{ asset('html/images/play.svg') }}" alt="play icone" /></a>
                                                    <div class="m">
                                                        <a href="{{ route('videos.detail', ['alias' => $item->videos_id]) }}"><img class="img-fluid" src="{{ ($videos->snippet->thumbnails->maxres) ? $videos->snippet->thumbnails->maxres->url : $videos->snippet->thumbnails->standard->url }}" alt="{{ $videos->snippet->title }}"></a>
                                                    </div>
                                                    <div class="time">{{ \Lib::duration($videos->contentDetails->duration) }}</div>
                                                </div>
                                                <div class="video-card-body">
                                                    <div class="video-title">
                                                        <a href="{{ route('videos.detail', ['alias' => $item->videos_id]) }}" class="sp-line-2">{{ $videos->snippet->title }}</a>
                                                    </div>
                                                    <div class="video-page text-success">
                                                        {{ $videos->snippet->channelTitle }} <a title="{{ $videos->snippet->channelTitle }}" data-placement="top" data-toggle="tooltip" href="#" data-original-title="{{ $videos->snippet->channelTitle }}"><i class="fe-check-circle text-success"></i></a>
                                                    </div>
                                                    <div class="video-view">
                                                        {{ \Lib::numberFormat($videos->statistics->viewCount, ',') }} views &nbsp;<i class="fas fa-calendar-alt"></i> {{ \Lib::dateFormat($videos->snippet->publishedAt, 'd/m/Y') }}
                                                    </div>
                                                </div>
                                            </div>
                                        @endforeach
                                        @endif
                                    </div>
                                </div>
                            </div>
                            @endforeach
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
@endsection

@section('css')
    <link rel="stylesheet" href="{{ asset('html/css/videos.min.css') }}">
@endsection

