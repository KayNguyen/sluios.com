@extends('FrontEnd::layouts.home', ['bodyClass' => 'has-cover'])

@section('title') {!! \Lib::siteTitle($site_title, $def['site_title']) !!} @stop

@section('content')
    <main>
        <div class="home-page-content">
            <div class="container page-content">
                <div class="video-block section-padding">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="main-title">
                                <div class="btn-group float-right right-action">
                                    <a href="#" class="right-action-link text-gray" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        Sort by <i class="fa fa-caret-down" aria-hidden="true"></i>
                                    </a>
                                    <div class="dropdown-menu dropdown-menu-right" x-placement="bottom-end" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(67px, 21px, 0px);">
                                        <a class="dropdown-item" href="#"><i class="fas fa-fw fa-star"></i> &nbsp; Top Rated</a>
                                        <a class="dropdown-item" href="#"><i class="fas fa-fw fa-signal"></i> &nbsp; Viewed</a>
                                        <a class="dropdown-item" href="#"><i class="fas fa-fw fa-times-circle"></i> &nbsp; Close</a>
                                    </div>
                                </div>
                                <h6>{{ $site_title }}</h6>
                            </div>
                        </div>
                        @if($data->total() > 0)
                            @foreach($data as $item)
                                @php($videos = Youtube::getVideoInfo($item->videos_id))
                                <div class="col-xl-3 col-sm-6 mb-3">
                                    <div class="video-card">
                                        <div class="video-card-image">
                                            <a class="play-icon" href="{{ route('videos.detail', ['alias' => $item->videos_id]) }}"><img src="{{ asset('html/images/play.svg') }}" alt="play icone" /></a>
                                            <div class="m">
                                                <a href="{{ route('videos.detail', ['alias' => $item->videos_id]) }}"><img class="img-fluid" src="{{ ($videos->snippet->thumbnails->maxres) ? $videos->snippet->thumbnails->maxres->url : $videos->snippet->thumbnails->standard->url }}" alt=""></a>
                                            </div>
                                            <div class="time">{{ \Lib::duration($videos->contentDetails->duration) }}</div>
                                        </div>
                                        <div class="video-card-body">
                                            <div class="video-title">
                                                <a href="{{ route('videos.detail', ['alias' => $item->videos_id]) }}" class="sp-line-2">{{ $videos->snippet->title }}</a>
                                            </div>
                                            <div class="video-page text-success">
                                                {{ $site_title }} <a title="{{ $site_title }}" data-placement="top" data-toggle="tooltip" href="#" data-original-title="{{ $site_title }}"><i class="fas fa-check-circle text-success"></i></a>
                                            </div>
                                            <div class="video-view">
                                                {{ \Lib::numberFormat($videos->statistics->viewCount, ',') }} views &nbsp;<i class="fas fa-calendar-alt"></i> {{ \Lib::dateFormat($videos->snippet->publishedAt, 'd/m/Y') }}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </main>
@endsection

@section('css')
    <link rel="stylesheet" href="{{ asset('html/css/videos.min.css') }}">
@endsection

