<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="{{$def['site_description']}}">
  <meta name="keyword" content="{{$def['site_keyword']}}">

  <link rel="shortcut icon" href="{{asset($def['favicon'])}}">

{!! \Lib::siteTitle('404 - Page Not Found') !!}


<!-- Icons -->
@foreach($def['site_media'] as $css)
  {!! \Lib::addMedia($css) !!}
@endforeach

<!-- Main styles for this application -->
@foreach($def['site_css'] as $css)
  {!! \Lib::addMedia($css) !!}
@endforeach

</head>

<body class="no-results" >

<header>
  <div class="row align-items-center justify-content-center full-height mg-none">
    <div class="container">
      <h1 class="font-md">Oops! We couldn’t find results for your search: <span class="font-h5 bold">404</span></h1>
    </div>
  </div>
  <img src="">
</header>

<footer class="footer footer-alt">
   <p class="text-white">&copy; 2019 VCCorpTeam.</p>
</footer>
<script>
  var ENV = {
    version: '{{ env('APP_VER', 1) }}',
  @foreach($def['site_js_val'] as $key => $val)
  {{$key}}: '{{$val}}'
  @endforeach
  };
</script>
<!-- Bootstrap and necessary plugins -->
@foreach($def['site_js'] as $js)
  {!! \Lib::addMedia($js) !!}
@endforeach
{!! \Lib::addMedia('admin/js/error/404.js') !!}
</body>

</html>