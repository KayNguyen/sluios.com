
<header id="topnav">
    <!-- Topbar Start -->
    <div class=" navbar-custom">
        <div class="container-fluid">
            <ul class="list-unstyled topnav-menu float-right mb-0">

                <li class="dropdown notification-list">
                    <!-- Mobile menu toggle-->
                    <a class="navbar-toggle nav-link">
                        <div class="lines">
                            <span></span>
                            <span></span>
                            <span></span>
                        </div>
                    </a>
                    <!-- End mobile menu toggle-->
                </li>
        
                <li class="d-none d-sm-block">
                    <form class="app-search">
                        <div class="app-search-box">
                            <div class="input-group">
                                <input type="text" class="form-control" placeholder="Search...">
                                <div class="input-group-append">
                                    <button class="btn" type="submit">
                                        <i class="fe-search"></i>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </form>
                </li>
        
                <li class="dropdown notification-list">
                    <a class="nav-link dropdown-toggle nav-user mr-0 waves-effect" data-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false">
                        <img src="{{ \Lib::get_gravatar(Auth::user()->email, 35) }}" alt="{{Auth::user()->email}}" class="rounded-circle">
                        <span class="pro-user-name ml-1">
                            {{ Auth::user()->fullname }} <i class="mdi mdi-chevron-down"></i> 
                        </span>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right profile-dropdown ">
                        <!-- item-->
                        <div class="dropdown-header noti-title">
                            <h6 class="text-overflow m-0">Welcome !</h6>
                        </div>
        
                        <!-- item-->
                        <a href="{{ route("admin.user.profile") }}" class="dropdown-item notify-item">
                            <i class="fe-user"></i>
                            <span>Thông tin cá nhân</span>
                        </a>
        
                        <!-- item-->
                        <a href="javascript:void(0);" onclick="shop.admin.showChangePasswordForm()" class="dropdown-item notify-item">
                            <i class="fe-settings"></i>
                            <span>Đổi mật khẩu</span>
                        </a>
        
                        <div class="dropdown-divider"></div>
        
                        <!-- item-->
                        <a href="{{ route('logout') }}" class="dropdown-item notify-item">
                            <i class="fe-log-out"></i>
                            <span>Logout</span>
                        </a>
        
                    </div>
                </li>
        
                <li class="dropdown notification-list">
                    <a href="javascript:void(0);" class="nav-link right-bar-toggle waves-effect">
                        <i class="fe-settings noti-icon"></i>
                    </a>
                </li>
        
            </ul>
        
            <!-- LOGO -->
            <div class="logo-box">
                <a href="index" class="logo text-center">
                    <span class="logo-lg">
                        <img src="assets/images/logo-dark.png" alt="" height="18">
                        <!-- <span class="logo-lg-text-light">UBold</span> -->
                    </span>
                    <span class="logo-sm">
                        <!-- <span class="logo-sm-text-dark">U</span> -->
                        <img src="assets/images/logo-sm.png" alt="" height="24">
                    </span>
                </a>
            </div>
        
            <ul class="list-unstyled topnav-menu topnav-menu-left m-0">
                
                <li class="dropdown d-none d-lg-block">
                    <a class="nav-link dropdown-toggle waves-effect" data-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false">
                        <i class="fe-settings noti-icon mr-1"></i>
                        Cấu hình hệ thống
                        <i class="mdi mdi-chevron-down"></i> 
                    </a>
                    <div class="dropdown-menu">
                        @foreach ($menuLeft as $k => $menu)
                            @if($k == '_99999999999')
                                @php($show=false)
                                @if(!empty($menu['perm']))
                                    @can($menu['perm'], $menu['perm'])
                                        @php($show=true)
                                    @endcan
                                @else
                                    @php($show=true)
                                @endif
                                @if($show)
                                    @if(!empty($menu['sub']))
                                        @foreach ($menu['sub'] as $sub)
                                            @php($show=false)
                                            @if(!empty($sub['perm']))
                                                @can($sub['perm'], $sub['perm'])
                                                    @php($show=true)
                                                @endcan
                                            @else
                                                @php($show=true)
                                            @endif
                    
                                            @if($show)
                                                @if(empty($sub['sub']))
                                                <!-- item-->
                                                <a href="{{ $sub['link']!='' ? $sub['link'] : 'javascript:void(0)' }}"@if($sub['link']!='' && $sub['newtab'] == 1) target="_blank"@endif class="dropdown-item">
                                                    @if (!empty($sub['icon'])) <i class="mr-1 {{ $sub['icon'] }}"></i> @endif
                                                    <span> {{ $sub['title'] }}</span>
                                                </a>
                                                @endif
                                            @endif
                                        @endforeach
                                    @else
                                        <a href="{{ $menu['link']!='' ? $menu['link'] : 'javascript:void(0)' }}"@if($menu['link']!='' && $menu['newtab'] == 1) target="_blank"@endif class="dropdown-item">
                                            @if (!empty($menu['icon'])) <i class="{{ $menu['icon'] }}"></i> @endif {{ $menu['title'] }}
                                        </a>
                                    @endif
                                @endif
                            @endif
                        @endforeach
                    </div>
                </li>
            </ul>
        </div> <!-- end container-fluid-->
    </div>
    <!-- end Topbar -->
    @include("BackEnd::layouts.menu")
</header>