@if ($paginator->hasPages())
    <div class="row">
        <div class="col-sm-12 col-md-5">
            <div class="dataTables_info" id="basic-datatable_info" role="status" aria-live="polite">Tổng {{ $data->count() }} bản ghi / {{ $data->lastPage() }} trang</div>
        </div>
        
        <div class="col-sm-12 col-md-7">
            <div class="paginate" id="basic-datatable_paginate">
                <ul class="pagination pagination-rounded">
                    <li class="paginate_button page-item previous disabled" id="basic-datatable_previous">
                        <a href="#" aria-controls="basic-datatable" data-dt-idx="0" tabindex="0" class="page-link">
                            <i class="mdi mdi-chevron-left"></i>
                        </a>
                    </li>
                    @foreach ($elements as $element)
                        {{-- "Three Dots" Separator --}}
                        @if (is_string($element))
                            <li class="page-item disabled"><span class="page-link">{{ $element }}</span></li>
                        @endif
        
                        {{-- Array Of Links --}}
                        @if (is_array($element))
                            @foreach ($element as $page => $url)
                                @if ($page == $paginator->currentPage())
                                    <li class="paginate_button page-item active">
                                        <a href="#" aria-controls="basic-datatable" data-dt-idx="{{ $page }}" tabindex="0" class="page-link">{{ $page }}</a>
                                    </li>
                                @else
                                    <li class="paginate_button page-item">
                                        <a href="#" aria-controls="basic-datatable" data-dt-idx="{{ $page }}" tabindex="0" onclick="changePage({{ $page }})" class="page-link">{{ $page }}</a>
                                    </li>
                                @endif
                            @endforeach
                        @endif
                    @endforeach
                    <li class="paginate_button page-item previous disabled" id="basic-datatable_previous">
                        <a href="#" aria-controls="basic-datatable" data-dt-idx="0" tabindex="0" class="page-link">
                            <i class="mdi mdi-chevron-right"></i>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
@endif

<script>
    function changePage(page) {
        var form = $('form');
        if(form.length > 1) {
            form = $('#searchForm');
        }
        form.append('<input type=hidden value="' + page + '" name="page" />');
        form.submit();
    }
</script>