@php($countBr = count($breadcrumb))
@if($countBr > 0 || !empty($extraCommand))
    <div class="page-title-box position-relative">
        <div class="page-title-right position-absolute" style="right: 0">
            <ol class="breadcrumb m-0">
                @if($countBr > 1)
                    @foreach($breadcrumb as $item)
                        @if($loop->last)
                        <li class="breadcrumb-item active">{{ $item['title'] }}</li>
                        @else
                            <li class="breadcrumb-item">
                                @if(!empty($item['link']))
                                    <a href="{{ $item['link'] }}">{{ $item['title'] }}</a>
                                @else
                                    {{ $item['title'] }}
                                @endif
                            </li>
                        @endif
                        @endforeach
                @else
                    <li class="breadcrumb-item">{{ $defBr['title'] }}</li>
                @endif
            </ol>
        </div>
        <h4 class="page-title">@if(isset($item['title'])){{ $item['title'] }}@endif
        @if(!empty($extraCommand))
                @foreach($extraCommand as $item)
                    <a class="btn{{!empty($item['class'])?' '.$item['class']:''}} btn-sm badge badge-success font-10" href="{{ route($item['link']) }}">@if(!empty($item['icon']))<i class="{{ $item['icon'] }}"></i>@endif &nbsp;{{ $item['title'] }}</a>
                @endforeach
        @endif
        </h4>
    </div>
@endif
