<footer class="app-footer">
    <div class="wrapper">
        <div class="container-fluid">
            <div class="row">
                <span class="text-primary font-weight-bold">© {{date('Y')}} KhoaTeam.</span>
            </div>
        </div>
    </div>
</footer>