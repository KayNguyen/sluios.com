<div class="topbar-menu">
    <div class="container-fluid">
        <div id="navigation">
            <!-- Navigation Menu-->
            <ul class="navigation-menu">
                @foreach ($menuLeft as $k => $menu)
                    @if($k == '_72')
                        @php($show=false)
                        @if(!empty($menu['perm']))
                            @can($menu['perm'], $menu['perm'])
                                @php($show=true)
                            @endcan
                        @else
                            @php($show=true)
                        @endif
                        @if($show)
                            @if(!empty($menu['sub']))
                                @foreach ($menu['sub'] as $sub)
                                    @php($show=false)
                                    @if(!empty($sub['perm']))
                                        @can($sub['perm'], $sub['perm'])
                                            @php($show=true)
                                        @endcan
                                    @else
                                        @php($show=true)
                                    @endif
            
                                    @if($show)
                                        @if(!empty($sub['sub']))
                                        <li class="has-submenu">
                                            <a class="" href="javascript:void(0)">
                                                @if (!empty($sub['icon'])) <i class="{{ $sub['icon'] }}"></i> @endif {{ $sub['title'] }}
                                            </a>
                                            <ul class="submenu">
                                                @foreach ($sub['sub'] as $sub2)
                                                    @php($show=false)
                                                    @if(!empty($sub2['perm']))
                                                        @can($sub2['perm'], $sub2['perm'])
                                                            @php($show=true)
                                                        @endcan
                                                    @else
                                                        @php($show=true)
                                                    @endif
                                                    @if($show)
                                                        <li>
                                                            <a href="{{ $sub2['link']!='' ? $sub2['link'] : 'javascript:void(0)' }}"@if($sub2['link']!='' && $sub2['newtab'] == 1) target="_blank"@endif>
                                                                @if (!empty($sub2['icon'])) <i class="{{ $sub2['icon'] }}"></i> @endif {{ $sub2['title'] }}
                                                            </a>
                                                        </li>
                                                    @endif
                                                @endforeach
                                            </ul>
                                        </li>
                                        @else
                                            <li class="">
                                                <a href="{{ $sub['link']!='' ? $sub['link'] : 'javascript:void(0)' }}"@if($sub['link']!='' && $sub['newtab'] == 1) target="_blank"@endif>
                                                    @if (!empty($sub['icon'])) <i class="{{ $sub['icon'] }}"></i> @endif {{ $sub['title'] }}
                                                </a>
                                            </li>
                                        @endif
                                    @endif
                                @endforeach
                                <li class="divider"></li>
                            @endif
                        @endif
                    @endif
                @endforeach
            </ul>
            <!-- End navigation menu -->

            <div class="clearfix"></div>
        </div>
        <!-- end #navigation -->
    </div>
    <!-- end container -->
</div>

