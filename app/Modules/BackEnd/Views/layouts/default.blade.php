<!doctype html>
<html lang="{{ $defLang }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="{{$def['site_description']}}">
    <meta name="keyword" content="{{$def['site_keyword']}}">

    <link rel="shortcut icon" href="{{ asset($def['favicon_images']) }}">
@if(\View::hasSection('title'))
    @yield('title')
@else
    {!! \Lib::siteTitle($site_title, $def['site_title']) !!}
@endif

<!-- Styles required by this views -->
@yield('css')

<!-- Icons -->
@foreach($def['site_media'] as $css)
    {!! \Lib::addMedia($css) !!}
@endforeach

<!-- Main styles for this application -->
@foreach($def['site_css'] as $css)
    {!! \Lib::addMedia($css) !!}
@endforeach
@stack('css_after_all')
<!-- top script -->
@yield('js_top')

</head>

<body class="app drop-menu-dark gradient-topbar topbar-dark header-fixed sidebar-fixed aside-menu-fixed aside-menu-hidden pb-0">

@include("BackEnd::layouts.header")

<div class="app-body">

    <!-- Main content -->
    <div class="wrapper">
        <div class="container-fluid">
            @if(\View::hasSection('breadcrumb'))
                @yield('breadcrumb')
            @else
                {!! \Lib::renderBreadcrumb() !!}
            @endif
            <div class="row">
                <div class="col-md-12">
                    <div class="animated fadeIn">
                        @yield('content')
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>

@include("BackEnd::layouts.footer")
@include("BackEnd::layouts.sidebar")
<script type="text/javascript">
    var ENV = {
        version: '{{ env('APP_VER', 0) }}',
        token: '{{ csrf_token() }}',
@foreach($def['site_js_val'] as $key => $val)
        {{$key}}: '{{$val}}',
@endforeach
    },
    COOKIE_ID = '{{ env('APP_NAME', '') }}',
    DOMAIN_COOKIE_REG_VALUE = 1,
    DOMAIN_COOKIE_STRING = '{{ config('session.domain') }}';
</script>

@if ( env('APP_DEBUG') )
{!! \Lib::addMedia('js/prettyprint.js') !!}
@endif

<!-- Bootstrap and necessary plugins -->
@foreach($def['site_js'] as $js)
{!! \Lib::addMedia($js) !!}
@endforeach

<!-- other script -->
@yield('js_bot')
@stack('js_bot_all')
<script type="text/javascript">
    jQuery(document).ready(shop.ready.run);
</script>

</body>
</html>