@extends('BackEnd::auth.layout')

@section('title') {!! \Lib::siteTitle($site_title, $def['site_title']) !!} @stop
@section('content')
    <div class="account-pages mt-5 mb-5">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-8 col-lg-6 col-xl-5">
                    <div class="card bg-pattern">

                        <div class="card-body p-4">
                            @if( count($errors) > 0)
                                <div class="alert alert-danger">
                                    @foreach ($errors->all() as $error)
                                        <div>{!! $error !!}</div>
                                    @endforeach
                                </div>
                            @endif

                            @if (session('status'))
                                <div class="alert alert-success">
                                    {{ session('status') }}
                                </div>
                            @endif

                            <div class="text-center w-75 m-auto">
                                <a href="javascript:;">
                                    <span style="font-family:'Lobster'; font-size: 28px">Washfriends</span>
                                    {{-- <span><img src="{{ asset('html-washfriends/images/logo@2x.png') }}" alt="" height="22"></span> --}}
                                </a>
                                <p class="text-muted mb-4 mt-3">Reset password</p>
                            </div>

                            {!! Form::open(['url' => route('password.request')]) !!}
                                <input type="hidden" name="token" value="{{ $token }}">

                                <div class="form-group mb-3">
                                    <label for="email">Email</label>
                                    <input type="text" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" id="email" placeholder="Email" name="email" value="{{ $email or old('email') }}" required>
                                </div>

                                <div class="form-group mb-3">
                                    <label for="password">Password</label>
                                    <input type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" id="password" placeholder="Enter the password" name="password" required>
                                </div>

                                <div class="form-group mb-4">
                                    <label for="password_confirmation">Password again</label>
                                    <input type="password" id="password_confirmation" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" placeholder="Enter the password again" name="password_confirmation" required>
                                </div>

                                <button type="submit" class="btn btn-block btn-success">Confirm new password</button>
                            {!! Form::close() !!}

                        </div> <!-- end card-body -->
                    </div>
                    <!-- end card -->

                </div> <!-- end col -->
            </div>
            <!-- end row -->
        </div>
        <!-- end container -->
    </div>
@endsection
