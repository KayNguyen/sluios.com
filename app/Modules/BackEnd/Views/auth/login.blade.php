@extends('BackEnd::auth.layout')

@section('title') {!! \Lib::siteTitle($site_title, $def['site_title']) !!} @stop
@section('content')
    <div class="account-pages mt-5 mb-5">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-8 col-lg-6 col-xl-5">
                    <div class="card bg-pattern">

                        <div class="card-body p-4">
                            @if ($errors->has('user_name'))
                                <p class="text-danger">{{ $errors->first('user_name') }}</p>
                            @endif

                            @if ($errors->has('password'))
                                <p class="text-danger">{{ $errors->first('password') }}</p>
                            @endif
                            <div class="text-center w-75 m-auto">
                                <a href="javascript:;">
                                    <span style="font-family:'Lobster'; font-size: 28px">Zunews.com</span>
                                    {{-- <span><img src="{{ asset('html-washfriends/images/logo@2x.png') }}" alt="" height="22"></span> --}}
                                </a>
                                <p class="text-muted mb-4 mt-3">Enter your username and password to access admin panel.</p>
                            </div>

                            {!! Form::open(['url' => route('login')]) !!}

                                <div class="form-group mb-3">
                                    <label for="emailaddress">Username</label>
                                    <input class="form-control" type="text" name="user_name" autocomplete="off" value="{{ old('user_name') }}" required autofocus placeholder="Enter your username">
                                </div>

                                <div class="form-group mb-3">
                                    <label for="password">Password</label>
                                    <input class="form-control" type="password" required autocomplete="off" name="password" id="password" placeholder="Enter your password">
                                </div>

                                <div class="form-group mb-3">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox" id="remember_pass" name="remember" {{ old('remember') ? 'checked' : '' }} class="custom-control-input">
                                        <label class="custom-control-label" for="remember_pass">Remember me</label>
                                    </div>
                                </div>

                                <div class="form-group mb-0 text-center">
                                    <button class="btn btn-primary btn-block" type="submit"> Log In </button>
                                </div>

                            {!! Form::close() !!}

                        </div> <!-- end card-body -->
                    </div>
                    <!-- end card -->

                    <div class="row mt-3">
                        <div class="col-12 text-center">
                            <p> <a href="{{ route('password.request') }}" class="text-white-50 ml-1">Forgot your password?</a></p>
                            <p class="text-white-50">Don't have an account? <a href="{{ route('register') }}" class="text-white ml-1"><b>Sign Up</b></a></p>
                        </div> <!-- end col -->
                    </div>
                    <!-- end row -->

                </div> <!-- end col -->
            </div>
            <!-- end row -->
        </div>
        <!-- end container -->
    </div>
@stop
