@extends('BackEnd::auth.layout')

@section('title') {!! \Lib::siteTitle($site_title, $def['site_title']) !!} @stop
@section('content')
    <div class="account-pages mt-5 mb-5">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-8 col-lg-6 col-xl-5">
                    <div class="card bg-pattern">

                        <div class="card-body p-4">
                            @if ($errors->has('user_name'))
                                <p class="text-danger">{{ $errors->first('user_name') }}</p>
                            @endif
                            @if( count($errors) > 0)
                                <div class="alert alert-danger">
                                    @foreach ($errors->all() as $error)
                                        <div>{!! $error !!}</div>
                                    @endforeach
                                </div>
                            @endif

                            @if ($errors->has('password'))
                                <p class="text-danger">{{ $errors->first('password') }}</p>
                            @endif
                            <div class="text-center w-75 m-auto">
                                <a href="javascript:;">
                                    <span style="font-family:'Lobster'; font-size: 28px">Washfriends</span>
                                    {{-- <span><img src="{{ asset('html-washfriends/images/logo@2x.png') }}" alt="" height="22"></span> --}}
                                </a>
                                <p class="text-muted mb-4 mt-3">Create an account.</p>
                            </div>

                            {!! Form::open(['url' => route('register')]) !!}

                            <div class="form-group mb-3">
                                <label for="user_name">Username</label>
                                <input class="form-control" type="text" id="user_name" name="user_name" autocomplete="off" value="{{ old('user_name') }}" required autofocus placeholder="Enter your username">
                            </div>

                            <div class="form-group mb-3">
                                <label for="email">Email</label>
                                <input type="text" id="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" placeholder="Email" name="email" value="{{ old('email') }}" required>
                            </div>

                            <div class="form-group mb-3">
                                <label for="password">Password</label>
                                <input class="form-control" type="password" required autocomplete="off" name="password" id="password" placeholder="Enter your password">
                            </div>

                            <div class="form-group mb-3">
                                <label for="password_confirmation">Password again</label>
                                <input class="form-control" type="password" required autocomplete="off" id="password_confirmation" placeholder="Enter the password again" name="password_confirmation">
                            </div>

                            <div class="form-group mb-0 text-center">
                                <button class="btn btn-primary btn-block" type="submit"> Sign up  </button>
                            </div>

                            {!! Form::close() !!}

                        </div> <!-- end card-body -->
                    </div>
                    <!-- end card -->

                    <div class="row mt-3">
                        <div class="col-12 text-center">
                            <p class="text-white-50">Already have an account? <a href="{{ route('login') }}" class="text-white ml-1"><b>Log In</b></a></p>
                        </div> <!-- end col -->
                    </div>
                    <!-- end row -->

                </div> <!-- end col -->
            </div>
            <!-- end row -->
        </div>
        <!-- end container -->
    </div>
@stop