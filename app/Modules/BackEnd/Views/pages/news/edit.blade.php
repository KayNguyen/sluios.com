@extends('BackEnd::layouts.default')

@section('content')
    <div class="row">
        <div class="col-sm-12">
            @if(old_blade('editMode'))
                {!! Form::open(['url' => route('admin.'.$key.'.edit.post', old_blade('id')), 'class' => 'row', 'files' => true]) !!}
            @else
                {!! Form::open(['url' => route('admin.'.$key.'.add.post'), 'class' => 'row', 'files' => true]) !!}
            @endif
            {!! Form::open(['url' => route('admin.'.$key.'.add.post'), 'class' => 'row', 'id' => 'news-editor', 'files' => true ]) !!}
            <div class="col-lg-8 col-12">
                @if( count($errors) > 0)
                    <div class="alert alert-danger alert-dismissible bg-danger text-white border-0 fade show" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        @foreach ($errors->all() as $error)
                        {!! $error !!}
                        @endforeach
                    </div>
                @endif
                @if (session('status'))
                    <div class="alert alert-success">
                        {!! session('status') !!}
                    </div>
                @endif

                <div class="card">
                    <div class="card-header">
                        <i class="fe-database"></i> Thông tin chi tiết
                    </div>
                    <div class="card-body">
                        <div class="row" id="slug-alias">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="title">Tiêu đề <span class="text-danger">*</span></label>
                                    <input type="text" required v-model="input" class="title_link form-control{{ $errors->has('title') ? ' is-invalid' : '' }} font-weight-bold" maxlength="250" id="placement" name="title" value="{{ old('title') }}">
                                </div>
                            </div>
                            <div class="col-sm-12">
                                @if(old_blade('editMode'))
                                <div class="input-group mb-3">
                                    <span id="sample-permalink" class="fs-13"><strong>Liên kết tĩnh: </strong> <a href="{{ route('news.detail', ['alias' => @$data->alias]) }}" target="_blank"> {{ env('APP_URL')}}/<span id="editable-post-name">{{ @$data->alias }}</span></a>
                                    </span>
                                    <div id="change-slug">
                                        <button class="change-slug position-relative btn" type="button" v-if="!isShow" @click="changeSlug()">Chỉnh sửa</button>
                                        <button class="col-auto change-slug position-relative btn" type="button" v-else @click="save()">Ok</button> <a href="javascript:void(0);" v-if="isShow" @click="cancel" class="fs-13">Hủy</a>
                                    </div>
                                </div>
                                @else
                                <label for="alias">Liên kết tĩnh</label>
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text bg-blue text-white">{{ env('APP_URL')}}/</span>
                                    </div>
                                    <input :value="slug" required type="text" name="alias" class="form-control th-service text-blue" id="basic-url" aria-describedby="basic-addon3">
                                </div>
                                @endif
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-12 col">
                                <div class="form-group">
                                    <label for="link">Ngôn ngữ</label>
                                    <select id="lang" name="lang" class="selectpicker" data-style="btn-light" onchange="shop.admin.getCat(2, $('#cat_id').val(), this.value, '#cat_id')">
                                        @foreach($langOpt as $k => $v)
                                            <option value="{{ $k }}">{{ $v }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="title">Tags</label>
                                    @if(\Lib::can($permission, 'tag'))
                                        <input type="text" class="form-control" id="tags" name="tags" value="{{ old('tags', @$tags) }}">
                                    @else
                                        <input type="text" class="form-control text-danger" value="Chưa có quyền gán Tag" disabled>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="link_videos">Link videos youtube <span class="text-warning">*</span> <small class="d-block text-warning">(nếu có sẽ nằm ở đầu bài viết thay thế ảnh đại diện)</small></label>
                                    <input class="form-control text-primary" autocomplete="off" type="text" value="{{ old('link_videos', @$data->link_videos) }}" name="link_videos">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="sort_desc">Mô tả ngắn <span class="text-warning">*</span></label>
                                    <textarea class="form-control" id="sort_desc" rows="5" name="sort_body" placeholder="Please enter description">{{ old('sort_body', @$data->sort_body) }}</textarea>
                                    </textarea>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <div class="form-group">
                                    <label for="content">Nội dung <span class="text-warning">*</span></label>
                                    <textarea class="form-control" id="content" rows="5" name="body" placeholder="Please enter description">{{ old('body', @$data->body) }}</textarea>
                                </div> <!-- end card-->
                            </div><!-- end col -->
                        </div>
                    </div>
                </div>

                <div class="mb-3">
                    <button type="submit" class="btn btn-sm btn-success"><i class="fe-send"></i> Cập nhật</button>
                    &nbsp;&nbsp;
                    <a class="btn btn-sm btn-danger" href="{{ redirect()->back()->getTargetUrl() }}"><i class="fa fa-ban"></i> Hủy bỏ</a>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="card">
                    <div class="card-header">
                        <i class="mdi mdi-folder-image"></i> Ảnh đại diện <span class="text-warning font-weight-bold">*</span>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-sm-12">
                                <input type="file" class="dropify" data-default-file="{{ isset($data) ? $data->getImageUrl('original') : '' }}" name="image"  />
                            </div>
                        </div>
                        <!-- end row -->
                    </div>
                </div>

                <div class="card">
                    <div class="card-header">
                        <i class="mdi mdi-crosshairs-gps"></i> Danh mục cha <span class="text-danger font-weight-bold">*</span>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-sm-12">
                                @foreach ($catOpt as $k => $cat)
                                    <div class="radio radio-success mb-2">
                                        <input type="radio" onchange="return showSubjectCateByRootSubject(this.value)" name="cate_par" id="radio-xvi{{ $k }}" @if($cat['id'] == @$data->cate_par) checked @elseif($loop->first) checked @endif value="{{ $cat['id'] }}">
                                        <label for="radio-xvi{{ $k }}">
                                            {{$cat['title']}}
                                        </label>
                                    </div>

                                @endforeach
                            </div>
                        </div>
                        <!-- end row -->
                    </div>
                </div>
                <div class="card">
                    <div class="card-header">
                        <i class="mdi mdi-animation-play-outline"></i> Danh mục con <span class="text-danger font-weight-bold">*</span>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-sm-12" id="inputCateCheckBoxRegion">
                                @foreach ($catOpt as $k => $cat)
                                   @if($loop->first && !old_blade('editMode'))
                                        @foreach($cat['sub'] as $sub)
                                            <div class="row">
                                                @if(!empty($sub['sub']))
                                                    <div class="checkbox checkbox-info mb-2 ml-1 col-md-6">
                                                        <input id="checkbox-2-{{$sub['id']}}" name="cate[]" class="cate_fe" type="checkbox" value="{{$sub['id']}}">
                                                        <label for="checkbox-2-{{$sub['id']}}">
                                                            {{$sub['title']}}
                                                        </label>
                                                        @foreach($sub['sub'] as $sub_2)
                                                            @if(empty($sub_2['sub']))
                                                                <div class="checkbox checkbox-info mt-2 ml-2 col-md-6">
                                                                    <input id="checkbox-3-{{$sub_2['id']}}" name="cate[]" class="cate_fe" type="checkbox" value="{{$sub_2['id']}}">
                                                                    <label for="checkbox-3-{{$sub_2['id']}}">
                                                                        {{$sub_2['title']}}
                                                                    </label>

                                                                </div>
                                                            @endif
                                                        @endforeach
                                                    </div>
                                                @else
                                                    <div class="checkbox checkbox-info mb-2 ml-1 col-md-12">
                                                        <input id="checkbox-10-{{$sub['id']}}" name="cate[]" type="checkbox" value="{{$sub['id']}}">
                                                        <label for="checkbox-10-{{$sub['id']}}">
                                                            {{$sub['title']}}
                                                        </label>
                                                    </div>
                                                @endif
                                            </div>
                                        @endforeach
                                   @elseif(@$data->cate_par == $cat['id'])
                                        @foreach($cat['sub'] as $sub)
                                            <div class="row">
                                                @if(!empty($sub['sub']))
                                                    <div class="checkbox checkbox-info mb-2 ml-1 col-md-6">
                                                        <input id="checkbox-2-{{$sub['id']}}" name="cate[]" {{ (@$data['categories'][$sub['id']]) ? 'checked' : '' }} class="cate_fe" type="checkbox" value="{{$sub['id']}}">
                                                        <label for="checkbox-2-{{$sub['id']}}">
                                                            {{$sub['title']}}
                                                        </label>
                                                        @foreach($sub['sub'] as $sub_2)
                                                            @if(empty($sub_2['sub']))
                                                                <div class="checkbox checkbox-info mt-2 ml-2 col-md-6">
                                                                    <input id="checkbox-3-{{$sub_2['id']}}" name="cate[]" {{ (@$data['categories'][$sub_2['id']]) ? 'checked' : '' }} class="cate_fe" type="checkbox" value="{{$sub_2['id']}}">
                                                                    <label for="checkbox-3-{{$sub_2['id']}}">
                                                                        {{$sub_2['title']}}
                                                                    </label>

                                                                </div>
                                                            @endif
                                                        @endforeach
                                                    </div>
                                                @else
                                                    <div class="checkbox checkbox-info mb-2 ml-1 col-md-12">
                                                        <input id="checkbox-10-{{$sub['id']}}" name="cate[]" {{ (@$data['categories'][$sub['id']]) ? 'checked' : '' }} type="checkbox" value="{{$sub['id']}}">
                                                        <label for="checkbox-10-{{$sub['id']}}">
                                                            {{$sub['title']}}
                                                        </label>
                                                    </div>
                                                @endif
                                            </div>
                                        @endforeach
                                   @endif
                                @endforeach
                            </div>
                        </div>
                        <!-- end row -->
                    </div>
                </div>

                <div id="addLine">
                    <div class="card">
                        <div class="card-header">
                            <i class="fe-database"></i> Thông tin bổ sung
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group mb-3">
                                        <label class="mb-2">Trạng thái <span class="text-danger">*</span></label>
                                        <br/>
                                        <div class="radio ml-1 form-check-inline radio-success">
                                            <input type="radio" id="inlineRadio1" value="2" name="status" {{ isset($data) ? (@$data->status == 2 ? 'checked' : '') : 'checked' }}>
                                            <label for="inlineRadio1"> Đăng ngay </label>
                                        </div>
                                        <div class="radio ml-1 form-check-inline radio-warning">
                                            <input type="radio" id="inlineRadio2" value="1" name="status" {{ @$data->status == 1 ? 'checked' : '' }}>
                                            <label for="inlineRadio2"> Chờ xét duyệt </label>
                                        </div>
                                        <div class="radio ml-1 form-check-inline">
                                            <input type="radio" id="inlineRadio3" value="-2" name="status" {{ @$data->status == -2 ? 'checked' : '' }}>
                                            <label for="inlineRadio3"> Bản nháp </label>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-lg-12 col">
                                    <div class="form-group">
                                        <label for="link">Thời gian hiển thị (mới nhất lên trên)</label>
                                        <input type="text" name="published" class="datepicker form-control" id="datetime-datepicker-m-d-Y" placeholder="Ngày xuất bản" autocomplete="off" value="{{ \Lib::dateFormat(@$data->published, 'd/m/Y H:i') }}">
                                    </div>
                                </div>
                            </div>

                            <div class="row searching">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="title_seo" >Tiêu đề seo <span class="text-warning">*</span></label>
                                        <input id="placement" type="text" class="form-control text_1" maxlength="70" name="title_seo"  value="{{ old('title_seo', @$data->title_seo) }}"/>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="keywords">Từ khóa <small class="text-warning d-block">Mỗi từ khóa cách nhau bởi dấu phẩy</small></label>
                                        <textarea id="title_2" class="form-control{{ $errors->has('keywords') ? ' is-invalid' : '' }}"  name="keywords">{{ old('keywords', @$data->keywords) }}</textarea>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="link" >Thẻ mô tả <span class="text-warning">*</span></label>
                                        <textarea id="textarea"  class="form-control{{ $errors->has('description_seo') ? ' is-invalid' : '' }}" rows="4"  maxlength="160" name="description_seo">{{ old('description_seo', @$data->description_seo) }}</textarea>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="robots">Robots <span class="text-warning">*</span></label>
                                        <select class="form-control" name="robots" id="robots">
                                            <option selected value="4">INDEX, FOLLOW</option>
                                            <option value="3">INDEX, NOFOLLOW</option>
                                            <option value="2">NOINDEX, FOLLOW</option>
                                            <option value="1">NOINDEX, NOFOLLOW</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="previews" class="mb-0" id="live1"> Preview</label>
                                        <div class="search-item">
                                            <h4 class="mb-1" id="live2"><a href="#" >aaaaaaaaa</a></h4>
                                            <div class="font-13 text-success mb-2 text-truncate">https://sluios.com/<p id="text_link" class="d-inline"></p></div>
                                            <p class="mb-0 text-muted" id="live3">sssssssssssssssssssss</p>
                                        </div> <!-- end search item -->
                                    </div>
                                </div>
                            </div>
                            <!-- end row -->
                        </div>
                    </div>

                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
@stop


@section('css')
    {!! \Lib::addMedia('admin/libs/flatpickr/flatpickr.min.css') !!}
    {!! \Lib::addMedia('admin/js/library/tag/jquery.tag-editor.css') !!}
    {!! \Lib::addMedia('admin/js/library/uploadifive/uploadifive.css') !!}
    {!! \Lib::addMedia('admin/libs/summernote/summernote-bs4.css') !!}
    {!! \Lib::addMedia('admin/libs/quill/quill.snow.css') !!}
@stop

@section('js_bot')
    {!! \Lib::addMedia('admin/libs/flatpickr/flatpickr.min.js') !!}
    {!! \Lib::addMedia('admin/js/library/uploadifive/jquery.uploadifive.min.js') !!}
    {!! \Lib::addMedia('admin/js/library/uploadifive/multiupload.js') !!}
    {!! \Lib::addMedia('admin/js/library/tag/jquery.caret.min.js') !!}
    {!! \Lib::addMedia('admin/js/library/tag/jquery.tag-editor.min.js') !!}
    {!! \Lib::addMedia('admin/libs/summernote/summernote.min.js') !!}
    {!! \Lib::addMedia('admin/libs/summernote/summernote-image-title.js') !!}
    {!! \Lib::addMedia('admin/libs/quill/quill.min.js') !!}
    {!! \Lib::addMedia('admin/js/form-fileuploads.init.js') !!}
    {!! \Lib::addMedia('admin/js/form-quilljs.init.js') !!}
    {!! \Lib::addMedia('admin/js/form-pickers.init.js') !!}
    {!! \Lib::addMedia('admin/js/form-advanced.init.js') !!}
    {!! \Lib::addMedia('admin/js/add-product.init.js') !!}

    <script>
        var news = {!! (old_blade('editMode')) ? json_encode($data) : '{title: "", alias: ""}' !!};

    </script>
    {!! \Lib::addMedia('admin/js/library/slug.js') !!}
    <script>

        <?php
            echo "var allCateSubject ='" . json_encode($catOpt) . "';";
            echo "var allCateEditChecked ='" . json_encode(@$data['categories']) . "';";
            ?>
            allCateSubject = JSON.parse(allCateSubject);
            allCateEditChecked = JSON.parse(allCateEditChecked);

        function showSubjectCateByRootSubject(parents) {
            var html = '<div class="row">';
            if (typeof allCateSubject['_'+parents] !== 'undefined') {

                for (var i in allCateSubject['_'+parents]['sub']) {
                    var alias = allCateSubject['_'+parents]['sub'][i];
                    if (typeof alias != 'undefined') {
                        //console.log(allCateSubject['items'][alias]['name'])
                        var name = alias['title'];
                        var _id = alias['id'];
                        html += '<div class="checkbox checkbox-info mb-2 ml-1 col-md-6"><input type="checkbox"';
                        /*if(allCateEditChecked != null && typeof allCateEditChecked[_id] !== 'undefined') {
                            console.log( allCateEditChecked[_id]);
                            html += 'checked';
                        }*/
                        html += ' name="cate[' + _id + ']" id="check_child_' + _id + '" value="' + _id + '"/><label for="check_child_'+ _id + '"> ' + name + '</label>';
                        html += '</div>';
                    }

                }
            }
            html += '</div>';
            //document.getElementById('inputCateCheckBoxRegion').innerHTML = html;
            jQuery('#inputCateCheckBoxRegion').html(html);

        }
        
        shop.ready.add(function(){
            // shop.multiupload('body');
            @if(\Lib::can($permission, 'tag'))
            shop.admin.tags.init({{ $tagType }}, '#tags', {{ @$data->id }});
            @endif;

        }, true);

    var title_1 = document.getElementsByClassName("text_1");
    var title_2 = document.getElementById('title_2');
    var title_3 = document.getElementById('textarea');
    var title_1live = document.getElementById('live2');
    var text_link = document.getElementById('text_link');
    var title_3live = document.getElementById('live3');
    var title_link = document.getElementsByClassName("title_link");


    title_1[0].onkeyup = function(){
        'use strict';
        title_1live.textContent = this.value;
        
    };console.log(title_1, title_2);
    // title_2.onkeyup = function(){
    //     'use strict';
    //     title_2live.textContent = this.value;
    // };
    title_link[0].onkeyup = function(){
        'use strict';
        text_link.textContent = this.value;
    };
    title_3.onkeyup = function(){
        'use strict';
        title_3live.textContent = this.value;
    };


    </script>
@stop