@extends('BackEnd::layouts.default')

@section('content')
<div class="row">
    <div class="col-lg-12">

        @if( count($errors) > 0)
            <div class="alert alert-danger">
                @foreach ($errors->all() as $error)
                    <div>{!! $error !!}</div>
                @endforeach
            </div>
        @endif

        @if (session('status'))
            <div class="alert alert-success">
                {!! session('status') !!}
            </div>
        @endif

        {!! Form::open(['url' => route('admin.'.$key), 'method' => 'get', 'id' => 'searchForm']) !!}
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="form-group col-sm-3">
                        <div class="input-group">
                            <input type="text" name="title" class="form-control" placeholder="Tiêu đề" value="{{$search_data->title}}">
                        </div>
                    </div>
                    <div class="form-group col">
                        <div class="input-group">
                            <select name="status" class="form-control">
                                <option value="">Chọn trạng thái </option>
                                <option value="2"{{ $search_data->status == 2 ? ' selected="selected"' : '' }}>Đã đăng</option>
                                <option value="-2"{{ $search_data->status == -2 ? ' selected="selected"' : '' }}>Bản nháp</option>
                                <option value="1"{{ $search_data->status == 1 ? ' selected="selected"' : '' }}>Chờ xét duyệt</option>
                                <option value="-1"{{ $search_data->status == -1 ? ' selected="selected"' : '' }}>Deleted</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group col">
                        <div class="input-group">
                            <select id="cat_id" name="type" class="form-control">
                                <option value=""> Chọn danh mục</option>
                                @foreach ($catOpt as $v)
                                <option value="{{ $v['id'] }}"{{ $search_data->type==$v['id'] ? ' selected="selected"' : '' }}>{{ $v['title'] }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group col">
                        <div class="input-group">
                            <input type="text" name="time_from" class="datepicker form-control" id="basic-datepicker" placeholder="Ngày tạo từ" autocomplete="off" value="{{ $search_data->time_from }}">
                        </div>
                    </div>
                    <div class="form-group col">
                        <div class="input-group">
                            <input type="text" name="time_to" class="datepicker form-control" id="basic-2-datepicker" placeholder="Ngày tạo đến" autocomplete="off" value="{{ $search_data->time_to }}">
                        </div>
                    </div>
                </div>
                <div class="float-right">
                    <button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-search"></i> Tìm kiếm</button>
                </div>
            </div>

        </div>
        {!! Form::close() !!}
        <div class="row">
            <div class="col-lg-12">
                <div class="card-box">
                    <h4 class="header-title">Danh sách bài viết</h4>
                    <p class="sub-header">
                    </p>
                    <div class="table-responsive">
                        <table class="table table-centered mb-0">
                            <thead class="thead-light">
                            <tr>
                                <th width="200">ID</th>
                                <th>Tiêu đề</th>
                                <th>Ảnh đại diện</th>
                                <th width="100">Danh mục</th>
                                <th width="120">Trạng thái</th>
                                <th width="160">Ngày xuất bản</th>
                                @if(\Lib::can($permission, 'edit') || \Lib::can($permission, 'delete'))
                                    <th width="55">Lệnh</th>
                                @endif
                            </tr>
                            </thead>
                            <tbody>
                            @if(!empty($data) || count($data) > 0)
                                @foreach ($data as $item)
                                    <tr>
                                        <td><a href="javascript: void(0);" class="text-body font-weight-bold">#1605{{ $item['id'] }}</a> </td>
                                        <td>
                                            <a class="text-primary font-weight-bold" href="{{route('admin.news.edit', $item->id)}}">{{ $item->title }}</a>

                                        </td>
                                        <td>
                                            <img src="{{ $item->getImageURL('original') }}" width="150px" alt="{{ $item->title }}">
                                        </td>
                                        <td>
                                            <span class="badge badge-success badge-pill">{{ $item->category->title }}</span>
                                        </td>
                                        <td>
                                            @if($item->status == 2)
                                                    <span class="badge badge-success badge-pill">Đã đăng</span>
                                                @elseif($item->status == 1)
                                                    <span class="badge badge-warning badge-pill">Chờ xét duyệt</span>
                                                @elseif($item->status == -2)
                                                    <span class="badge badge-secondary badge-pill">Bản nháp</span>
                                                @elseif($item->status == -1)
                                                    <span class="badge badge-danger badge-pill">Deleted</span>
                                                @endif

                                        </td>
                                        <td align="center">{{ \Lib::dateFormat($item->created, 'd/m/Y - H:i') }}</td>
                                        <td align="center">
                                            @if(\Lib::can($permission, 'edit'))

                                                @if($item->status == 2)
                                                    <a href="javascript:void(0)" class="text-white btn btn-sm btn-success mb-1" onclick="shop.admin.updateStatus({{ $item->id }},false,'news')" title="Đang hiển thị, Click để ẩn"><i class="fe-check-square"></i></a>
                                                @else
                                                    <a href="javascript:void(0)" class="text-danger btn btn-sm btn-dark mb-1" onclick="shop.admin.updateStatus({{ $item->id }}, true,'news')" title="Đang ẩn, Phê duyệt để hiển thị"><i class="fe-check-square"></i></a>
                                                @endif
                                                <a href="{{ route('admin.'.$key.'.edit', $item->id) }}" title="Sửa" class="btn mb-1 btn-sm btn-info text-light" ><i class="fe-edit icons"></i></a>
                                            @endif
                                            <a href="{{ route('news.detail', ['alias' => $item->alias]) }}" title="Preview" target="_blank" class="btn mb-1 btn-sm btn-info text-light"><i class="fe-send"></i></a>
                                            @if(\Lib::can($permission, 'delete'))
                                                <a href="{{ route('admin.'.$key.'.delete', $item->id) }}" title="Xóa" class="btn mb-1 btn-sm btn-danger text-light" onclick="return confirm('Bạn muốn xóa ?')"><i class="icon-trash icons"></i></a>
                                            @endif
                                        </td>
                                        
                                    </tr>
                                @endforeach
                            @endif

                            </tbody>
                        </table>
                    </div>
                    @if(empty($data) || $data->isEmpty())
                        <h4 align="center">Không tìm thấy dữ liệu phù hợp</h4>
                    @else
                        {!! $data->links('BackEnd::layouts.pagin', ['data' => $data]) !!}
                    @endif
                </div> <!-- end card-box -->
            </div> <!-- end col -->

        </div>
        <!--- end row -->
    </div>
</div>
@stop


@section('css')
    {!! \Lib::addMedia('admin/libs/flatpickr/flatpickr.min.css') !!}
    {!! \Lib::addMedia('admin/libs/switchery/switchery.min.css') !!}
@stop

@section('js_bot')
    {!! \Lib::addMedia('admin/libs/flatpickr/flatpickr.min.js') !!}
    {!! \Lib::addMedia('admin/libs/select2/select2.min.js') !!}
    {!! \Lib::addMedia('admin/js/form-pickers.init.js') !!}

@stop
