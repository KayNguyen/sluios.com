@extends('BackEnd::layouts.default')

@section('content')
<div class="row">
    <div class="col-lg-12">

        @if( count($errors) > 0)
            <div class="alert alert-danger">
                @foreach ($errors->all() as $error)
                    <div>{!! $error !!}</div>
                @endforeach
            </div>
        @endif

        @if (session('status'))
            <div class="alert alert-success">
                {!! session('status') !!}
            </div>
        @endif

        {!! Form::open(['url' => route('admin.'.$key), 'method' => 'get', 'id' => 'searchForm']) !!}
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="form-group col-sm-3">
                        <div class="input-group">
                            <input type="text" name="email" class="form-control" placeholder="Email" value="{{$search_data->email}}">
                        </div>
                    </div>
                    <div class="form-group col-sm-3">
                        <div class="input-group">
                            <input type="text" name="phone" class="form-control" placeholder="Điện thoại" value="{{$search_data->phone}}">
                        </div>
                    </div>
                    <div class="form-group col-sm-3">
                        <div class="input-group">
                            <input type="text" name="time_from" class="datepicker form-control" id="basic-datepicker" placeholder="Ngày tạo từ" autocomplete="off" value="{{ $search_data->time_from }}">
                        </div>
                    </div>
                    <div class="form-group col-sm-3">
                        <div class="input-group">
                            <input type="text" name="time_to" class="datepicker form-control" id="basic-2-datepicker" placeholder="Ngày tạo đến" autocomplete="off" value="{{ $search_data->time_to }}">
                        </div>
                    </div>
                </div>
                <div class="float-right">
                    <button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-search"></i> Tìm kiếm</button>
                </div>
            </div>

        </div>
        {!! Form::close() !!}
        <div class="row">
            <div class="col-lg-12">
                <div class="card-box">
                    <h4 class="header-title">Danh sách quy trình từng chuyên mục</h4>
                    <p class="sub-header">
                    </p>
                    <div class="table-responsive">
                        <table class="table table-centered mb-0">
                            <thead class="thead-light">
                            <tr>
                                <th width="200">ID</th>
                                <th>Email</th>
                                <th width="160">Ngày nhập</th>
                                @if(\Lib::can($permission, 'edit'))
                                    <th width="55">Sửa</th>
                                @endif
                                @if(\Lib::can($permission, 'delete'))
                                    <th width="55">Xóa</th>
                                @endif
                            </tr>
                            </thead>
                            <tbody>
                            @if(!empty($data) || count($data) > 0)
                                @foreach ($data as $item)
                                    <tr>
                                        <td><a href="javascript: void(0);" class="text-body font-weight-bold">#1605{{ $item['id'] }}</a> </td>
                                        <td>
                                            @if($item->customer_id > 0)
                                                <a class="text-primary" href="{{route('admin.customer.edit', $item->customer_id)}}" target="_blank">{{ $item->email }}</a>
                                            @else
                                                <a class="text-dark">{{ $item->email }}</a>
                                            @endif
                                        </td>
                                        <td align="center">{{ \Lib::dateFormat($item->created, 'd/m/Y - H:i') }}</td>
                                        @if(\Lib::can($permission, 'edit'))
                                            <td align="center"><a href="{{ route('admin.'.$key.'.edit', $item->id) }}" class="text-primary"><i class="icon-pencil icons"></i></a></td>
                                        @endif
                                        @if(\Lib::can($permission, 'delete'))
                                            <td align="center"><a href="{{ route('admin.'.$key.'.delete', $item->id) }}"  class="text-danger" onclick="return confirm('Bạn muốn xóa ?')"><i class="icon-trash icons"></i></a></td>
                                        @endif
                                    </tr>
                                @endforeach
                            @endif

                            </tbody>
                        </table>
                    </div>
                    @if(empty($data) || $data->isEmpty())
                        <h4 align="center">Không tìm thấy dữ liệu phù hợp</h4>
                    @else
                        {!! $data->links('BackEnd::layouts.pagin', ['data' => $data]) !!}
                    @endif
                </div> <!-- end card-box -->
            </div> <!-- end col -->

        </div>
        <!--- end row -->
    </div>
</div>
@stop


@section('css')
    {!! \Lib::addMedia('admin/libs/flatpickr/flatpickr.min.css') !!}
    {!! \Lib::addMedia('admin/libs/switchery/switchery.min.css') !!}
@stop

@section('js_bot')
    {!! \Lib::addMedia('admin/libs/flatpickr/flatpickr.min.js') !!}
    {!! \Lib::addMedia('admin/libs/select2/select2.min.js') !!}
    {!! \Lib::addMedia('admin/js/form-pickers.init.js') !!}

@stop