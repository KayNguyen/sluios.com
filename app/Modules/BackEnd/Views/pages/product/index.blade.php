@extends('BackEnd::layouts.default')

@section('content')
<div class="row">
    <div class="col-lg-12">
        
        @if( count($errors) > 0)
            <div class="alert alert-danger">
                @foreach ($errors->all() as $error)
                    <div>{!! $error !!}</div>
                @endforeach
            </div>
        @endif

        @if (session('status'))
            <div class="alert alert-success">
                {!! session('status') !!}
            </div>
        @endif

        {!! Form::open(['url' => route('admin.'.$key), 'method' => 'get', 'id' => 'searchForm']) !!}
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="form-group col-md-4 col-lg-3">
                        <div class="input-group">
                            <input type="text" name="title" class="form-control" placeholder="Tên sản phẩm" value="{{ $search_data->title }}">
                        </div>
                    </div>
                    <div class="form-group col-md-4 col-lg-3">
                        <div class="input-group">
                            <select id="cat_id" name="type" class="form-control">
                                <option value=""> Chọn danh mục</option>
                                @include('BackEnd::pages.category.option', [
                                    'options' => $catOpt,
                                    'def' => old('cat_id', $search_data->cat_id)
                                ])
                            </select>
                        </div>
                    </div>

                    <div class="form-group col-md-4 col-lg-2">
                        <div class="input-group">
                            <select name="status" class="form-control">
                                <option value="">Chọn trạng thái </option>
                                <option value="2"{{ $search_data->status == 2 ? ' selected="selected"' : '' }}>Online</option>
                                <option value="-2"{{ $search_data->status == -2 ? ' selected="selected"' : '' }}>Draff</option>
                                <option value="1"{{ $search_data->status == 1 ? ' selected="selected"' : '' }}>Offline</option>
                                <option value="-1"{{ $search_data->status == -1 ? ' selected="selected"' : '' }}>Deleted</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group col-md-6 col-lg-2">
                        <div class="input-group">
                            <input type="text" name="time_from" class="datepicker form-control" id="basic-datepicker" placeholder="Ngày tạo từ" autocomplete="off" value="{{ $search_data->time_from }}">
                        </div>
                    </div>
                    <div class="form-group col-md-6 col-lg-2">
                        <div class="input-group">
                            <input type="text" name="time_to" class="datepicker form-control" id="basic-2-datepicker" placeholder="Ngày tạo đến" autocomplete="off" value="{{ $search_data->time_to }}">
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-12">
                        <div class="text-lg-right mt-3 mt-lg-0">
                            <button type="submit" class="btn btn-primary"><i class="fa fa-search"></i> Tìm kiếm</button>
                        </div>
                    </div><!-- end col-->
                </div>
            </div>
        </div>
        {!! Form::close() !!}


        <div class="row">
            @if(!empty($data) || !$data->isEmpty())
                @foreach ($data as $k => $item)
                    <div class=" col mb-3">
                        <div class="card-box product-box h-100">

                            <div class="product-action">
                                @if(\Lib::can($permission, 'edit'))
                                <a href="{{ route('admin.'.$key.'.edit', $item->id) }}" class="btn btn-success btn-xs waves-effect waves-light"><i class="mdi mdi-pencil"></i></a>
                                @endif
                                @if(\Lib::can($permission, 'delete'))
                                <a href="{{ route('admin.'.$key.'.delete', $item->id) }}" title="Xóa" onclick="return confirm('Bạn muốn xóa ?')" class="btn btn-danger btn-xs waves-effect waves-light"><i class="mdi mdi-close"></i></a>
                                @endif
                            </div>

                            <div class="product-img-bg">
                                <img src="{{ $item->getImageUrl('medium') }}" alt="product-pic" style="height:300px" class="img-fluid" />
                            </div>

                            <div class="product-info">
                                <div class="row align-items-center">
                                    <div class="col">
                                        <h5 class="font-16 mt-0 sp-line-1"><a href="{{ route('admin.'.$key.'.edit', $item->id) }}" class="text-dark">{{ $item->title }}</a> </h5>
                                        <div class="text-warning mb-2 font-13">
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                        </div>
                                        <h5 class="mb-2">
                                                <span class="text-muted"> Trạng thái : @if($item->status == 2)
                                                    <span class="badge badge-success badge-pill">Online</span>
                                                @elseif($item->status == 1)
                                                    <span class="badge badge-warning badge-pill">Offline</span>
                                                @elseif($item->status == -2)
                                                    <span class="badge badge-secondary badge-pill">Draff</span>
                                                @elseif($item->status == -1)
                                                    <span class="badge badge-danger badge-pill">Deleted</span>
                                                @endif</span></h5>
                                        <h5 class="mb-2">
                                            <span class="text-muted"> Danh mục : {!! !empty($item->category->title) ? '<span class="text-primary">'.$item->category->title.'</span>' : '<span class="badge badge-warning badge-pill">Chưa cập nhật</span>' !!}</span>
                                        </h5>
                                        <h5 class="m-0">
                                            <span class="text-muted"> Ngày tạo : {{ \Lib::dateFormat($item->created, 'd/m/Y') }}</span>
                                        </h5>
                                    </div>
                                    <div class="col-auto">
                                        <div class="product-price-tag mb-2">
                                            <a href="{{ route('product.detail', ['alias' => $item->alias]) }}" target="_blank" class="text-info" title="Xem sản phẩm"><i class="fe-send"></i></a>
                                        </div>
                                        <div class="product-price-tag mb-2" id="valid-feedback-{{ $item->id }}">
                                            @if($item->hot == 1)
                                            <a href="javascript:;" onclick="shop.admin.setHot({{ $item->id }},true,'product')" class="text-info" title="Click để chọn sản phẩm ưa chuộng"><i class="fe-trending-up"></i></a>
                                            @else
                                            <a href="javascript:;" onclick="shop.admin.setHot({{ $item->id }},false,'product')" class="text-danger" title="Sản phẩm ưa chuộng"><i class="fe-trending-up"></i></a>
                                            @endif
                                        </div>
                                        <div class="product-price-tag set-home" id="valid-feedback-home-{{ $item->id }}">
                                            @if($item->home == 1)
                                                <a href="javascript:;" onclick="shop.admin.setHome({{ $item->id }},true,'product')" class="text-info" title="Click để chọn sản phẩm nổi bật"><i class="fe-heart-on"></i></a>
                                            @else
                                                <a href="javascript:;" onclick="shop.admin.setHome({{ $item->id }},false,'product')" class="text-danger" title="Hiển thị sản phẩm nổi bật ở trang chủ"><i class="fe-heart-on"></i></a>
                                            @endif
                                        </div>
                                    </div>
                                </div> <!-- end row -->
                            </div> <!-- end product info-->
                        </div> <!-- end card-box-->
                    </div> <!-- end col-->
                @endforeach
            @endif
        </div>
        @if(empty($data) || $data->isEmpty())
                <h4 align="center">Không tìm thấy dữ liệu phù hợp</h4>
        @else
            {!! $data->links('BackEnd::layouts.pagin', ['data' => $data]) !!}
        @endif
        <!--- end row -->
    </div>

</div>

@stop

@section('css')
    {!! \Lib::addMedia('admin/libs/flatpickr/flatpickr.min.css') !!}
    {!! \Lib::addMedia('admin/libs/switchery/switchery.min.css') !!}
@stop

@section('js_bot')
    {!! \Lib::addMedia('admin/libs/flatpickr/flatpickr.min.js') !!}
    {!! \Lib::addMedia('admin/libs/select2/select2.min.js') !!}
    {!! \Lib::addMedia('admin/js/form-pickers.init.js') !!}
@stop