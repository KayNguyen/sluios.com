@extends('BackEnd::layouts.default')

@section('content')
    <div class="row">
        <div class="col-sm-12 col-md-12">

            @if(old_blade('editMode'))
                {!! Form::open(['url' => route('admin.'.$key.'.edit.post', old_blade('id')), 'class' => 'row', 'files' => true]) !!}
            @else
                {!! Form::open(['url' => route('admin.'.$key.'.add.post'), 'class' => 'row', 'files' => true]) !!}
            @endif
            <div class="col-md-12 col-12">
                @if( count($errors) > 0)
                    <div class="alert alert-danger">
                        @foreach ($errors->all() as $error)
                            <div>{!! $error !!}</div>
                        @endforeach
                    </div>
                @endif
                @if (session('status'))
                    <div class="alert alert-success">
                        {!! session('status') !!}
                    </div>
                @endif
                <div class="row">
                    <div class="col-lg-8">
                        <div class="card-box" id="slug-alias">
                            <h5 class="text-uppercase bg-light p-2 mt-0 mb-3">Thông tin cơ bản</h5>

                            <div class="form-group mb-3">
                                <label for="title">Tên sản phẩm <span class="text-danger">*</span></label>
                                <input type="text" maxlength="250"  autocomplete="off" id="placement" required v-model="input" name="title" value="{{ old('title') }}" class="form-control" placeholder="e.g : Apple iMac">
                            </div>
                            @if(old_blade('editMode'))
                                <div class="input-group mb-3">
                                    <span id="sample-permalink" class="fs-13"><strong>Liên kết tĩnh: </strong> <a href="{{ route('news.detail', ['alias' => @$data->alias]) }}" target="_blank"> {{ env('APP_URL')}}/<span id="editable-post-name">{{ @$data->alias }}</span></a>
                                    </span>
                                    <div id="change-slug">
                                        <button class="change-slug position-relative btn" type="button" v-if="!isShow" @click="changeSlug()">Chỉnh sửa</button>
                                        <button class="col-auto change-slug position-relative btn" type="button" v-else @click="save('product')">Ok</button> <a href="javascript:void(0);" v-if="isShow" @click="cancel" class="fs-13">Hủy</a>
                                    </div>
                                </div>
                            @else
                                <label for="alias">Liên kết tĩnh</label>
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text bg-blue text-white">{{ env('APP_URL')}}/</span>
                                    </div>
                                    <input :value="slug" required type="text" name="alias" class="form-control th-service text-blue" id="basic-url" aria-describedby="basic-addon3">
                                </div>
                            @endif

                            <div class="form-group mb-3">
                                <label for="title">Danh mục <span class="text-danger">*</span></label>
                                <select onchange="load_filter_cate(this)" id="cat_id" name="cat_id" class="form-control{{ $errors->has('cat_id') ? ' is-invalid' : '' }}">
                                    @if(!empty($catOpt))
                                    @include('BackEnd::pages.category.option', [
                                        'options' => $catOpt,
                                        'def' => old_blade('cat_id'),
                                        'mode' => 1
                                    ])
                                    @endif
                                </select>
                            </div>
                            <div class="form-group mb-3">
                                <div class="row">
                                    <div class="col-md-6">
                                        <label for="price">Giá bán</label>
                                        <div class="input-group mb-3">
                                            <input onkeypress="return shop.numberOnly()" onkeyup="mixMoney(this)" onfocus="this.select()"  type="text" name="price" value="{{ old('price', @$data->price) }}" autocomplete="off" class="form-control text-danger">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <label for="link_video">Chú ý</label>
                                        <div class="input-group mb-3">
                                            <input  type="text" name="note" value="{{ old('note', @$data->note) }}" autocomplete="off" class="form-control" placeholder="Giá đặc biệt khi mua Online đến 31/08: 10.290.000đ">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group mb-3">
                                <label for="link_video">Link video</label>
                                <div class="input-group mb-3">
                                    <input  type="text" name="link_video" value="{{ old('link_video', @$data->link_video) }}" autocomplete="off" class="form-control text-primary">
                                </div>
                            </div>
                            
                            <div class="form-group mb-3">
                                <label for="product-description">Mô tả chi tiết <span class="text-warning">*</span></label>
                                <textarea class="form-control" id="product-description" required name="sort_body" rows="5" placeholder="Please enter description">
                                    {!! @$data->sort_body !!}
                                </textarea>
                            </div>

                            <div class="form-group mb-3">
                                <label class="mb-2">Trạng thái <span class="text-danger">*</span></label>
                                <br/>
                                <div class="radio ml-1 form-check-inline radio-success">
                                    <input type="radio" id="inlineRadio1" value="2" name="status" {{ isset($data) ? (@$data->status == 2 ? 'checked' : '') : 'checked' }}>
                                    <label for="inlineRadio1"> Đăng ngay </label>
                                </div>
                                <div class="radio ml-1 form-check-inline radio-warning">
                                    <input type="radio" id="inlineRadio2" value="1" name="status" {{ @$data->status == 1 ? 'checked' : '' }}>
                                    <label for="inlineRadio2"> Chờ xét duyệt </label>
                                </div>
                                <div class="radio ml-1 form-check-inline">
                                    <input type="radio" id="inlineRadio3" value="-2" name="status" {{ @$data->status == -2 ? 'checked' : '' }}>
                                    <label for="inlineRadio3"> Bản nháp </label>
                                </div>
                            </div>
                        </div> <!-- end card-box -->
                    </div> <!-- end col -->

                    <div class="col-lg-4">

                        <div class="card-box">
                            <h5 class="text-uppercase mt-0 mb-3 bg-light p-2">Ảnh đại diện</h5>
                            <input type="file" class="dropify" data-default-file="{{ isset($data) ? $data->getImageUrl('original') : '' }}" name="image"  />

                        </div> <!-- end col-->

                        <div class="card-box">
                            <h5 class="text-uppercase mt-0 mb-3 bg-light p-2">Ảnh liên quan</h5>
                            <div class=" mb-3">
                                <div class="col-sm-12">
                                    @include('BackEnd::pages.product.include.upload_multi', [
                                                'object_id' => old_blade('id')
                                            ])
                                </div>
                            </div>

                        </div> <!-- end col-->

                        <div class="card-box" id="product-field-add">
                            <div class="row">
                                <div class="col-xl-12">
                                    <div id="accordion" class="mb-3">
                                        <div class="card mb-1">
                                            <div class="card-header" id="headingOne">
                                                <h5 class="m-0">
                                                    <a class="text-dark" data-toggle="collapse" href="#collapseOne" aria-expanded="true">
                                                        <i class="mdi mdi-help-circle mr-1 text-primary"></i>
                                                        Thuộc tính chính
                                                    </a>
                                                </h5>
                                            </div>

                                            <div id="collapseOne" class="collapse show main-pro" aria-labelledby="headingOne" data-parent="#accordion">
                                                <div class="card-body">
                                                    <div class="form-group px-0">
                                                        <div class="row" v-for="(pro, ind) in properties" :key="ind">
                                                            <div class="col-md-6">
                                                                <input type="text" class="form-control" required :name="'property['+ind+'][title]'" v-model="pro.title" placeholder="Type of Washing Machine">
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="d-flex">
                                                                    <input type="text" autocomplete="off" required :name="'property['+ind+'][value]'" v-model="pro.value" placeholder="Máy giặt cửa dưới" class="form-control th-service mb-2">
                                                                    <span class="mb-2 text-success plus-service position-relative" @click="addProperty('properties')"></span>
                                                                    <span class="mb-2 text-success remove-service position-relative" @click="del(ind, 'properties')"></span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div> <!-- end #accordions-->
                                </div> <!-- end col -->
                            </div>
                        </div>

                    </div> <!-- end col-->
                </div>
                <!-- end row -->

                <div class="row">
                    <div class="col-12">
                        <div class="text-center mb-3">
                            <button type="submit" id="submit" class="btn w-sm btn-success waves-effect waves-light">Cập nhật</button>
                            <button type="button" class="btn w-sm btn-danger waves-effect waves-light">Hủy bỏ</button>
                        </div>
                    </div> <!-- end col -->
                </div>
                <!-- end row -->
            </div>

            {!! Form::close() !!}
        </div>
    </div>
@stop

@section('css')
    {!! \Lib::addMedia('admin/js/library/tag/jquery.tag-editor.css') !!}
    {!! \Lib::addMedia('admin/libs/summernote/summernote-bs4.css') !!}
    {!! \Lib::addMedia('admin/js/library/uploadifive/uploadifive.css') !!}
@stop

@section('js_bot')

    {!! \Lib::addMedia('admin/js/library/uploadifive/jquery.uploadifive.min.js') !!}
    {!! \Lib::addMedia('admin/js/library/uploadifive/multiupload.js') !!}
    {!! \Lib::addMedia('admin/js/library/tag/jquery.caret.min.js') !!}
    {!! \Lib::addMedia('admin/js/library/tag/jquery.tag-editor.min.js') !!}
    {!! \Lib::addMedia('admin/libs/summernote/summernote.min.js') !!}
    {!! \Lib::addMedia('admin/libs/summernote/summernote-bs4.min.js') !!}
    {!! \Lib::addMedia('admin/libs/select2/select2.min.js') !!}
    {!! \Lib::addMedia('admin/libs/dropzone/dropzone.min.js') !!}


    {!! \Lib::addMedia('admin/js/form-fileuploads.init.js') !!}
    {!! \Lib::addMedia('admin/js/add-product.init.js') !!}

    <script>


        var news = {!! (old_blade('editMode')) ? json_encode($data) : '{title: "", alias: ""}' !!};
        if(!news.properties || news.properties === 'null') {

            news.properties = [{
                title: '', value: ''
            }]
        }

        shop.ready.add(function(){


            // shop.multiupload('body');
            @if(\Lib::can($permission, 'tag'))
            shop.admin.tags.init({{ $tagType }}, '#tags', {{ @$data->id }});
            @endif;

        }, true);
        function mixMoney(myfield) {
            var thousands_sep = '.',
                val = parseInt(myfield.value.replace(/[.*+?^${}()|[\]\\]/g, ''));
            myfield.value = shop.numberFormat(val, 0, '', thousands_sep);
        }
    </script>

    {!! \Lib::addMedia('admin/js/library/slug.js') !!}
    {!! (old_blade('editMode')) ? \Lib::addMedia('admin/js/library/product/vue-edit-product.js') : \Lib::addMedia('admin/js/library/product/vue-add-product.js') !!}
@stop