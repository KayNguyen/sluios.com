@extends('BackEnd::layouts.default')

@section('content')
<div class="row">
    <div class="col-lg-12">
        
        @if( count($errors) > 0)
            <div class="alert alert-danger">
                @foreach ($errors->all() as $error)
                    <div>{!! $error !!}</div>
                @endforeach
            </div>
        @endif

        @if (session('status'))
            <div class="alert alert-success">
                {!! session('status') !!}
            </div>
        @endif

        {!! Form::open(['url' => route('admin.'.$key), 'method' => 'get', 'id' => 'searchForm']) !!}
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="form-group col-sm-3">
                        <div class="input-group">
                            <input type="text" name="title" class="form-control" placeholder="Tiêu đề" value="{{$search_data->title}}">
                        </div>
                    </div>
                    <div class="form-group col">
                        <div class="input-group">
                            <select name="status" class="form-control">
                                <option value="">Chọn trạng thái </option>
                                <option value="2"{{ $search_data->status == 2 ? ' selected="selected"' : '' }}>Đã đăng</option>
                                <option value="-2"{{ $search_data->status == -2 ? ' selected="selected"' : '' }}>Bản nháp</option>
                                <option value="1"{{ $search_data->status == 1 ? ' selected="selected"' : '' }}>Chờ xét duyệt</option>
                                <option value="-1"{{ $search_data->status == -1 ? ' selected="selected"' : '' }}>Deleted</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group col">
                        <div class="input-group">
                            <select id="cat_id" name="type" class="form-control">
                                <option value=""> Chọn danh mục</option>
                                @foreach ($catOpt as $v)
                                <option value="{{ $v['id'] }}"{{ $search_data->type==$v['id'] ? ' selected="selected"' : '' }}>{{ $v['title'] }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group col">
                        <div class="input-group">
                            <input type="text" name="time_from" class="datepicker form-control" id="basic-datepicker" placeholder="Ngày tạo từ" autocomplete="off" value="{{ $search_data->time_from }}">
                        </div>
                    </div>
                    <div class="form-group col">
                        <div class="input-group">
                            <input type="text" name="time_to" class="datepicker form-control" id="basic-2-datepicker" placeholder="Ngày tạo đến" autocomplete="off" value="{{ $search_data->time_to }}">
                        </div>
                    </div>
                </div>
                <div class="float-right">
                    <button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-search"></i> Tìm kiếm</button>
                </div>
            </div>

        </div>
        {!! Form::close() !!}


        <div class="row">
            @if(!empty($data) || !$data->isEmpty())
                @foreach ($data as $k => $item)
                    @php($videos = Youtube::getVideoInfo($item->videos_id))
                    <div class="col-lg-3 col mb-3">
                        <div class="card-box product-box h-100">

                            <div class="product-action">
                                @if(\Lib::can($permission, 'edit'))
                                <a href="{{ route('admin.'.$key.'.edit', $item->id) }}" class="btn btn-success btn-xs waves-effect waves-light"><i class="mdi mdi-pencil"></i></a>
                                @endif
                                @if(\Lib::can($permission, 'delete'))
                                <a href="{{ route('admin.'.$key.'.delete', $item->id) }}" title="Xóa" onclick="return confirm('Bạn muốn xóa ?')" class="btn btn-danger btn-xs waves-effect waves-light"><i class="mdi mdi-close"></i></a>
                                @endif
                            </div>

                            <div class="product-img-bg video-img">
                                <img src="{{ ($videos->snippet->thumbnails->maxres) ? $videos->snippet->thumbnails->maxres->url : $videos->snippet->thumbnails->standard->url }}" alt="product-pic" class="img-fluid" />
                                <div class="ms_play_icon play_btn play_music">
                                    <a href="#full-width-modal" data-animation="fadein" data-target="#full-width-modal" data-plugin="custommodal" data-overlayColor="#38414a" onclick="shop.admin.previewVideoAjax({{ $item->id }}, 'videos')">
                                        <img src="{{ asset('html/images/play.svg') }}" alt="play icone" />
                                    </a>
                                </div>
                                <div class="ms_box_overlay"></div>
                            </div>

                            <div class="product-info">
                                <div class="row align-items-center">
                                    {{-- <div class="col">
                                        <h5 class="font-16 mt-0 sp-line-1"><a href="{{ route('admin.'.$key.'.edit', $item->id) }}" class="text-dark">{{ $videos->snippet->title }}</a> </h5>
                                        <div class="text-warning mb-2 font-13">
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                        </div>
                                        <h5 class="mb-2">
                                                <span class="text-muted"> Trạng thái : @if($item->status == 2)
                                                    <span class="badge badge-success badge-pill">Online</span>
                                                @elseif($item->status == 1)
                                                    <span class="badge badge-warning badge-pill">Offline</span>
                                                @elseif($item->status == -2)
                                                    <span class="badge badge-secondary badge-pill">Draff</span>
                                                @elseif($item->status == -1)
                                                    <span class="badge badge-danger badge-pill">Deleted</span>
                                                @endif</span></h5>
                                        <h5 class="mb-2">
                                            <span class="text-muted"> Danh mục : {!! !empty($item->category->title) ? '<span class="text-primary">'.$item->category->title.'</span>' : '<span class="badge badge-warning badge-pill">Chưa cập nhật</span>' !!}</span>
                                        </h5>
                                        <h5 class="mb-2">
                                            <span class="text-muted"> Ngày tạo : {{ \Lib::dateFormat($videos->snippet->publishedAt, 'd/m/Y H:i') }}</span>
                                        </h5>
                                        @isset($videos->snippet->defaultAudioLanguage)
                                            <h5 class="m-0">
                                                <span class="text-muted"> Ngôn ngữ âm thanh : {{ $item->lang($videos->snippet->defaultAudioLanguage) }}</span>
                                            </h5>
                                        @endisset
                                    </div> --}}
                                    <div class="col">
                                        <h5 class="font-16 mt-0 sp-line-1"><a href="{{ route('admin.'.$key.'.edit', $item->id) }}" class="text-dark">{{ $videos->snippet->title }}</a> </h5>
                                        <div class="justify-content-between meta d-flex">
                                            <span class="date"><i class="fas fa-clock"></i>{{ \Lib::dateFormat($videos->snippet->publishedAt, 'd/m/Y') }}</span>
                                            <div class="statistics">
                                                <span class="views"><i class="fas fa-eye"></i>{{ \Lib::numberFormat($videos->statistics->viewCount, ',') }}</span>
                                                <span class="like"><i class="fas fa-thumbs-up"></i>{{ \Lib::numberFormat($videos->statistics->likeCount, ',') }}</span>
                                                {{-- <span class="dislike"><i class="fas fa-thumbs-down"></i>{{ \Lib::numberFormat($videos->statistics->dislikeCount, ',') }}</span>
                                                <span class="fcomments"><i class="fas fa-comments"></i>{{ \Lib::numberFormat($videos->statistics->commentCount, ',') }}</span> --}}
                                            </div>
                                        </div>
                                    </div>
                                </div> <!-- end row -->
                            </div> <!-- end product info-->
                        </div> <!-- end card-box-->
                    </div> <!-- end col-->
                @endforeach
            @endif
        </div>
        <!-- Custom Modal -->
        <div id="full-width-modal" class="modal-demo">
            <div class="custom-modal-text embed-responsive embed-responsive-16by9">
                <iframe class="embed-responsive-item" id="preview_videos" frameborder="0" allowfullscreen></iframe>
            </div>
        </div>  
        @if(empty($data) || $data->isEmpty())
                <h4 align="center">Không tìm thấy dữ liệu phù hợp</h4>
        @else
            {!! $data->links('BackEnd::layouts.pagin', ['data' => $data]) !!}
        @endif
        <!--- end row -->
    </div>

</div>

@stop

@section('css')
    {!! \Lib::addMedia('admin/libs/flatpickr/flatpickr.min.css') !!}
    {!! \Lib::addMedia('admin/libs/switchery/switchery.min.css') !!}
@stop

@section('js_bot')
    {!! \Lib::addMedia('admin/libs/flatpickr/flatpickr.min.js') !!}
    {!! \Lib::addMedia('admin/libs/select2/select2.min.js') !!}
    {!! \Lib::addMedia('admin/js/form-pickers.init.js') !!}
@stop