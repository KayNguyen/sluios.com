@extends('BackEnd::layouts.default')

@section('content')
    <div class="row">
        <div class="col-sm-12">
            @if(old_blade('editMode'))
                {!! Form::open(['url' => route('admin.'.$key.'.edit.post', old_blade('id')), 'class' => 'row', 'files' => true]) !!}
            @else
                {!! Form::open(['url' => route('admin.'.$key.'.add.post'), 'class' => 'row', 'files' => true]) !!}
            @endif
            {!! Form::open(['url' => route('admin.'.$key.'.add.post'), 'class' => 'row', 'id' => 'news-editor', 'files' => true ]) !!}
            <div class="col-lg-8 col-12">
                @if( count($errors) > 0)
                    <div class="alert alert-danger alert-dismissible bg-danger text-white border-0 fade show" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        @foreach ($errors->all() as $error)
                        {!! $error !!}
                        @endforeach
                    </div>
                @endif
                @if (session('status'))
                    <div class="alert alert-success">
                        {!! session('status') !!}
                    </div>
                @endif

                <div class="card">
                    <div class="card-header">
                        <i class="fe-database"></i> Thông tin chi tiết
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col">
                                <div class="form-group">
                                    <label for="videos_id">ID videos youtube <span class="text-danger">*</span></label>
                                    <input type="text" onchange="shop.admin.previewVideo($(this).val())" required class="form-control{{ $errors->has('videos_id') ? ' is-invalid' : '' }} font-weight-bold" maxlength="50" id="placement" name="videos_id" value="{{ old('videos_id', @$data->videos_id) }}">
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="form-group">
                                @if(!old_blade('editMode'))
                                    <div class="embed-responsive embed-responsive-16by9 d-none">
                                        <iframe class="embed-responsive-item" id="preview_videos" src=""></iframe>
                                    </div>
                                @else
                                    <div class="embed-responsive embed-responsive-16by9">
                                        <iframe class="embed-responsive-item" id="preview_videos" src="https://www.youtube.com/embed/{{ $data->videos_id }}"></iframe>
                                    </div>
                                @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="mb-3">
                    <button type="submit" class="btn btn-sm btn-success"><i class="fe-send"></i> Cập nhật</button>
                    &nbsp;&nbsp;
                    <a class="btn btn-sm btn-danger" href="{{ redirect()->back()->getTargetUrl() }}"><i class="fa fa-ban"></i> Hủy bỏ</a>
                </div>
            </div>

            <div class="col-lg-4">
                
                <div class="card">
                    <div class="card-header">
                        <i class="mdi mdi-crosshairs-gps"></i> Danh mục cha <span class="text-danger font-weight-bold">*</span>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-sm-12">
                                @foreach ($catOpt as $k => $cat)
                                    <div class="radio radio-success mb-2">
                                        <input type="radio" onchange="return showSubjectCateByRootSubject(this.value)" name="cate_par" id="radio-xvi{{ $k }}" @if($cat['id'] == @$data->cate_par) checked @elseif($loop->first) checked @endif value="{{ $cat['id'] }}">
                                        <label for="radio-xvi{{ $k }}">
                                            {{$cat['title']}}
                                        </label>
                                    </div>

                                @endforeach
                            </div>
                        </div>
                        <!-- end row -->
                    </div>
                </div>
                <div class="card">
                    <div class="card-header">
                        <i class="mdi mdi-animation-play-outline"></i> Danh mục con <span class="text-danger font-weight-bold">*</span>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-sm-12" id="inputCateCheckBoxRegion">
                                @foreach ($catOpt as $k => $cat)
                                   @if($loop->first && !old_blade('editMode'))
                                        @foreach($cat['sub'] as $sub)
                                            <div class="row">
                                                @if(!empty($sub['sub']))
                                                    <div class="checkbox checkbox-info mb-2 ml-1 col-md-6">
                                                        <input id="checkbox-2-{{$sub['id']}}" name="cate[]" class="cate_fe" type="checkbox" value="{{$sub['id']}}">
                                                        <label for="checkbox-2-{{$sub['id']}}">
                                                            {{$sub['title']}}
                                                        </label>
                                                        @foreach($sub['sub'] as $sub_2)
                                                            @if(empty($sub_2['sub']))
                                                                <div class="checkbox checkbox-info mt-2 ml-2 col-md-6">
                                                                    <input id="checkbox-3-{{$sub_2['id']}}" name="cate[]" class="cate_fe" type="checkbox" value="{{$sub_2['id']}}">
                                                                    <label for="checkbox-3-{{$sub_2['id']}}">
                                                                        {{$sub_2['title']}}
                                                                    </label>

                                                                </div>
                                                            @endif
                                                        @endforeach
                                                    </div>
                                                @else
                                                    <div class="checkbox checkbox-info mb-2 ml-1 col-md-12">
                                                        <input id="checkbox-10-{{$sub['id']}}" name="cate[]" type="checkbox" value="{{$sub['id']}}">
                                                        <label for="checkbox-10-{{$sub['id']}}">
                                                            {{$sub['title']}}
                                                        </label>
                                                    </div>
                                                @endif
                                            </div>
                                        @endforeach
                                   @elseif(@$data->cate_par == $cat['id'])
                                        @foreach($cat['sub'] as $sub)
                                            <div class="row">
                                                @if(!empty($sub['sub']))
                                                    <div class="checkbox checkbox-info mb-2 ml-1 col-md-6">
                                                        <input id="checkbox-2-{{$sub['id']}}" name="cate[]" {{ (@$data['categories'][$sub['id']]) ? 'checked' : '' }} class="cate_fe" type="checkbox" value="{{$sub['id']}}">
                                                        <label for="checkbox-2-{{$sub['id']}}">
                                                            {{$sub['title']}}
                                                        </label>
                                                        @foreach($sub['sub'] as $sub_2)
                                                            @if(empty($sub_2['sub']))
                                                                <div class="checkbox checkbox-info mt-2 ml-2 col-md-6">
                                                                    <input id="checkbox-3-{{$sub_2['id']}}" name="cate[]" {{ (@$data['categories'][$sub_2['id']]) ? 'checked' : '' }} class="cate_fe" type="checkbox" value="{{$sub_2['id']}}">
                                                                    <label for="checkbox-3-{{$sub_2['id']}}">
                                                                        {{$sub_2['title']}}
                                                                    </label>

                                                                </div>
                                                            @endif
                                                        @endforeach
                                                    </div>
                                                @else
                                                    <div class="checkbox checkbox-info mb-2 ml-1 col-md-12">
                                                        <input id="checkbox-10-{{$sub['id']}}" name="cate[]" {{ (@$data['categories'][$sub['id']]) ? 'checked' : '' }} type="checkbox" value="{{$sub['id']}}">
                                                        <label for="checkbox-10-{{$sub['id']}}">
                                                            {{$sub['title']}}
                                                        </label>
                                                    </div>
                                                @endif
                                            </div>
                                        @endforeach
                                   @endif
                                @endforeach
                            </div>
                        </div>
                        <!-- end row -->
                    </div>
                </div>
                <div class="card">
                    <div class="card-header">
                        <i class="fe-database"></i> Thông tin bổ sung
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group mb-3">
                                    <label class="mb-2">Trạng thái <span class="text-danger">*</span></label>
                                    <br/>
                                    <div class="radio ml-1 form-check-inline radio-success">
                                        <input type="radio" id="inlineRadio1" value="2" name="status" {{ isset($data) ? (@$data->status == 2 ? 'checked' : '') : 'checked' }}>
                                        <label for="inlineRadio1"> Đăng ngay </label>
                                    </div>
                                    <div class="radio ml-1 form-check-inline radio-warning">
                                        <input type="radio" id="inlineRadio2" value="1" name="status" {{ @$data->status == 1 ? 'checked' : '' }}>
                                        <label for="inlineRadio2"> Chờ xét duyệt </label>
                                    </div>
                                    <div class="radio ml-1 form-check-inline">
                                        <input type="radio" id="inlineRadio3" value="-2" name="status" {{ @$data->status == -2 ? 'checked' : '' }}>
                                        <label for="inlineRadio3"> Bản nháp </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- end row -->
                    </div>
                </div>
            </div>

            {!! Form::close() !!}
        </div>
    </div>
@stop


@section('css')
    
@stop

@section('js_bot')
    {!! \Lib::addMedia('admin/libs/flatpickr/flatpickr.min.js') !!}
    {!! \Lib::addMedia('admin/js/form-pickers.init.js') !!}

    <script>
        var news = {!! (old_blade('editMode')) ? json_encode($data) : '{title: "", alias: ""}' !!};
    </script>
    {!! \Lib::addMedia('admin/js/library/slug.js') !!}
    <script>

        <?php
            echo "var allCateSubject ='" . json_encode($catOpt) . "';";
            echo "var allCateEditChecked ='" . json_encode(@$data['categories']) . "';";
            ?>
            allCateSubject = JSON.parse(allCateSubject);
            allCateEditChecked = JSON.parse(allCateEditChecked);

        function showSubjectCateByRootSubject(parents) {
            var html = '<div class="row">';
            if (typeof allCateSubject['_'+parents] !== 'undefined') {

                for (var i in allCateSubject['_'+parents]['sub']) {
                    var alias = allCateSubject['_'+parents]['sub'][i];
                    if (typeof alias != 'undefined') {
                        //console.log(allCateSubject['items'][alias]['name'])
                        var name = alias['title'];
                        var _id = alias['id'];
                        html += '<div class="checkbox checkbox-info mb-2 ml-1 col-md-6"><input type="checkbox"';
                        /*if(allCateEditChecked != null && typeof allCateEditChecked[_id] !== 'undefined') {
                            console.log( allCateEditChecked[_id]);
                            html += 'checked';
                        }*/
                        html += ' name="cate[' + _id + ']" id="check_child_' + _id + '" value="' + _id + '"/><label for="check_child_'+ _id + '"> ' + name + '</label>';
                        html += '</div>';
                    }

                }
            }
            html += '</div>';
            //document.getElementById('inputCateCheckBoxRegion').innerHTML = html;
            jQuery('#inputCateCheckBoxRegion').html(html);

        }
        shop.ready.add(function(){
            // shop.multiupload('body');
            @if(\Lib::can($permission, 'tag'))
            shop.admin.tags.init({{ $tagType }}, '#tags', {{ @$data->id }});
            @endif;

        }, true);
    </script>

@stop