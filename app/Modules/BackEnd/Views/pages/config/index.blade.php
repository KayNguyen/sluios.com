@extends('BackEnd::layouts.default')

@section('content')
    <div class="row bg-white py-3">
        <div class="col-sm-12">
            {!! Form::open(['url' => route('admin.config.post'), 'files' => true ]) !!}

            @if( count($errors) > 0)
                <div class="alert alert-danger">
                    @foreach ($errors->all() as $error)
                        <div>{!! $error !!}</div>
                    @endforeach
                </div>
            @endif

            @if (session('status'))
                <div class="alert alert-success">
                    {!! session('status') !!}
                </div>
            @endif

            <ul class="nav nav-tabs" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active text-success" data-toggle="tab" href="#website" role="tab" aria-controls="website" aria-expanded="true"><i class="icon-globe"></i> Website</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link text-danger" data-toggle="tab" href="#contact" role="tab" aria-controls="contact" aria-expanded="false"><i class="icon-phone"></i> Liên hệ</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link text-info" data-toggle="tab" href="#social" role="tab" aria-controls="social" aria-expanded="false"><i class="icon-star"></i> Mạng XH</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link text-danger" data-toggle="tab" href="#developers" role="tab" aria-controls="developers" aria-expanded="false"><i class="fe-trending-up"></i> Nhà phát triển</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#route" role="tab" aria-controls="route" aria-expanded="false"><i class="icon-directions"></i> Định tuyến</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#security" role="tab" aria-controls="security" aria-expanded="false"><i class="icon-shield"></i> Bảo mật</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#other" role="tab" aria-controls="other" aria-expanded="false"><i class="icon-settings"></i> Khác</a>
                </li>
            </ul>

            <div class="tab-content mb-4">
                <div class="tab-pane active" id="website" role="tabpanel" aria-expanded="true">
                    <div class="row">
                        <div class="col-sm-9">
                            <div class="form-group">
                                <label for="title">Tiêu đề website</label>
                                <input type="text" class="form-control{{ $errors->has('site_name') ? ' is-invalid' : '' }}" id="site_name" name="site_name" value="{{ old('site_name', $data['site_name']) }}" required>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-9">
                            <div class="form-group">
                                <label for="title">Từ khóa</label>
                                <input type="text" class="form-control{{ $errors->has('keywords') ? ' is-invalid' : '' }}" id="keywords" name="keywords" value="{{ old('keywords', $data['keywords']) }}">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-9">
                            <div class="form-group">
                                <label for="title">Mô tả website</label>
                                <textarea rows="9" class="form-control{{ $errors->has('keywords') ? ' is-invalid' : '' }}" id="description" name="description">{{ old('description', $data['description']) }}</textarea>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-9">
                            <div class="form-group">
                                <label for="title">Wash Friends Video</label>
                                <input type="text" class="mb-3 text-pink  form-control{{ $errors->has('link_video_1') ? ' is-invalid' : '' }}" id="link_video_1" name="link_video_1" value="{{ old('link_video_1', (isset($data['link_video_1']) ? $data['link_video_1'] : '')) }}">
                                <input type="text" class=" text-pink form-control{{ $errors->has('link_video_2') ? ' is-invalid' : '' }}" id="link_video_2" name="link_video_2" value="{{ old('link_video_2', (isset($data['link_video_2']) ? $data['link_video_2'] : '')) }}">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="title">Logo website <span class="text-warning">*</span></label>
                                <input type="file" class="dropify" data-default-file="{{ !empty($data['logo_images']) ? $data['logo_images'] : '' }}" id="logo" name="logo">
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label for="title">Favicon <span class="text-warning">*</span></label>
                                <input type="file" class="dropify" data-default-file="{{ !empty($data['favicon_images']) ? $data['favicon_images'] : '' }}" id="favicon" name="favicon">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-9">
                            <div class="form-group">
                                <label for="title">Ảnh seo <span class="text-warning">Ảnh vuông kích thước chiều ngang: 800x800px</span></label>
                                <input type="file" class="dropify"  id="image" name="image">
                                <br />
                                @if(!empty($data['image_medium_seo']))
                                    <div class="pull-right">
                                        <img src="{{ $data['image_medium_seo'] }}" />
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>

                <div class="tab-pane" id="contact" role="tabpanel" aria-expanded="false">
                    <div class="row">
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label for="title">CEO</label>
                                <input type="text" class="form-control{{ $errors->has('ceo') ? ' is-invalid' : '' }}" id="ceo" name="ceo" value="{{ old('ceo', $data['ceo']) }}" required>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label for="title">Email liên hệ</label>
                                <input type="text" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" id="email" name="email" value="{{ old('email', $data['email']) }}" required>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label for="title">Hotline</label>
                                <input type="text" class="form-control{{ $errors->has('hotline') ? ' is-invalid' : '' }}" id="hotline" name="hotline" value="{{ old('hotline', $data['hotline']) }}">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label for="title">Tel Việt Nam</label>
                                <input type="text" class="form-control{{ $errors->has('tel') ? ' is-invalid' : '' }}" id="tel" name="tel" value="{{ old('tel', $data['tel']) }}">
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label for="title">Tel Hàn Quốc</label>
                                <input type="text" class="form-control{{ $errors->has('tel_korea') ? ' is-invalid' : '' }}" id="tel_korea" name="tel_korea" value="{{ old('tel_korea', !empty($data['tel_korea']) ? $data['tel_korea'] : '') }}">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-9">
                            <div class="form-group">
                                <label for="title">Địa chỉ Việt Nam (VI)</label>
                                <textarea type="text" class="form-control{{ $errors->has('address') ? ' is-invalid' : '' }}" id="address" name="address" rows="5">{{ old('address', $data['address']) }}</textarea>
                            </div>
                        </div>
                        <div class="col-sm-9">
                            <div class="form-group">
                                <label for="title">Địa chỉ Việt Nam (EN)</label>
                                <textarea type="text" class="form-control{{ $errors->has('address_en') ? ' is-invalid' : '' }}" id="address_en" name="address_en" rows="5">{{ old('address_en', $data['address_en']) }}</textarea>
                            </div>
                        </div>
                        <div class="col-sm-9">
                            <div class="form-group">
                                <label for="title">Địa chỉ Hàn Quốc</label>
                                <textarea type="text" class="form-control{{ $errors->has('address_korea') ? ' is-invalid' : '' }}" id="address_korea" name="address_korea" rows="5">{{ old('address_korea', $data['address_korea']) }}</textarea>

                            </div>
                        </div>
                    </div>
                </div>

                <div class="tab-pane" id="social" role="tabpanel" aria-expanded="false">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="facebook">Facebook account</label>
                                <input type="text" class="form-control{{ $errors->has('facebook_name') ? ' is-invalid' : '' }}" id="facebook_name" name="facebook_name" value="{{ old('facebook_name', !empty($data['facebook_name']) ? $data['facebook_name'] : '') }}">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="facebook">Facebook</label>
                                <input type="text" class="form-control{{ $errors->has('facebook') ? ' is-invalid' : '' }}" id="facebook" name="facebook" value="{{ old('facebook', !empty($data['facebook']) ? $data['facebook'] : '') }}">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="facebook">Group Facebook</label>
                                <input type="text" class="form-control{{ $errors->has('facebook') ? ' is-invalid' : '' }}" id="facebook" name="group_facebook" value="{{ old('group_facebook', !empty($data['group_facebook']) ? $data['group_facebook'] : '') }}">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="facebook">Network</label>
                                <input type="text" class="form-control{{ $errors->has('facebook') ? ' is-invalid' : '' }}" id="network" name="network" value="{{ old('network', !empty($data['network']) ? $data['network'] : '') }}">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="google">Google+</label>
                                <input type="text" class="form-control{{ $errors->has('google') ? ' is-invalid' : '' }}" id="google" name="google" value="{{ old('google', !empty($data['google']) ? $data['google'] : '') }}">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="google">Twitter</label>
                                <input type="text" class="form-control{{ $errors->has('twitter') ? ' is-invalid' : '' }}" id="twitter" name="twitter" value="{{ old('twitter', !empty($data['twitter']) ? $data['twitter'] : '') }}">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="instagram">Instagram</label>
                                <input type="text" class="form-control{{ $errors->has('instagram') ? ' is-invalid' : '' }}" id="instagram" name="instagram" value="{{ old('instagram', !empty($data['instagram']) ? $data['instagram'] : '') }}">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="youtube">Youtube</label>
                                <input type="text" class="form-control{{ $errors->has('youtube') ? ' is-invalid' : '' }}" id="youtube" name="youtube" value="{{ old('youtube', !empty($data['youtube']) ? $data['youtube'] : '') }}">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="zalo">Zalo+</label>
                                <input type="text" class="form-control{{ $errors->has('zalo') ? ' is-invalid' : '' }}" id="zalo" name="zalo" value="{{ old('zalo', !empty($data['zalo']) ? $data['zalo'] : '') }}">
                            </div>
                        </div>
                    </div>
                </div>

                <div class="tab-pane" id="route" role="tabpanel" aria-expanded="false">
                    <div class="row">
                        <div class="col-sm-12">
                            <div id="result"></div>
                            <button type="button" class="btn btn-sm btn-primary" onclick="shop.updateRoutes()"><i class="fe-send"></i> Cập nhật định tuyến</button>
                        </div>
                    </div>
                </div>

                <div class="tab-pane" id="security" role="tabpanel" aria-expanded="false">
                    <div class="row">
                        <div class="col-sm-9">
                            <div class="form-group">
                                <label for="weakpass">Blacklist mật khẩu (cách nhau dấu ";")</label>
                                <textarea rows="10" class="form-control{{ $errors->has('weakpass') ? ' is-invalid' : '' }}" id="weakpass" name="weakpass">{{ old('weakpass', !empty($data['weakpass']) ? $data['weakpass'] : '') }}</textarea>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="tab-pane" id="other" role="tabpanel" aria-expanded="false">
                    <div class="row">
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label for="title">Phiên bản JS/CSS</label>
                                <input type="text" class="form-control{{ $errors->has('version') ? ' is-invalid' : '' }}" id="version" name="version" value="{{ old('version', $data['version']) }}" required>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <label class="checkbox-inline" for="mobile_active">
                                <input type="checkbox" id="mobile_active" name="mobile_active" value="1" @if(old('mobile_active', isset($data['mobile_active']) ? $data['mobile_active'] : 0) == 1) checked @endif> Kích hoạt Mobile Wrap
                            </label>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="title">Mã nhúng đầu trang (trước < /head>)</label>
                                <textarea type="text" class="form-control{{ $errors->has('script_head') ? ' is-invalid' : '' }}" rows="10" id="script_head" name="script_head">
                                    {{ old('script_head', !empty($data['script_head']) ? $data['script_head'] : '') }}
                                </textarea>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="title">Mã nhúng cuối trang (trước < /body>)</label>
                                <textarea type="text" class="form-control{{ $errors->has('script_body') ? ' is-invalid' : '' }}" rows="10" id="script_body" name="script_body">
                                    {{ old('script_body', !empty($data['script_body']) ? $data['script_body'] : '') }}
                                </textarea>
                            </div>
                        </div>
                    </div>


                </div>
                <div class="tab-pane" id="developers" role="tabpanel" aria-expanded="false">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label for="title">Mô tả</label>
                                <textarea class="form-control" id="product-description" name="body" rows="5" placeholder="Please enter description">{{ old('body', !empty($data['body']) ? $data['body'] : '') }}</textarea>
                            </div>

                            <div class="form-group">
                                <label for="title">Ảnh minh họa</label>
                                <input type="file" class="dropify" data-default-file="{{  !empty($data['images_dev']) ? $data['images_dev'] : '' }}"  id="image_dev" name="image_dev">
                            </div>
                        </div>
                    </div>


                </div>
            </div>

            <div class="mb-3">
                <button type="submit" id="submit" class="btn btn-sm btn-primary"><i class="fe-send"></i> Cập nhật</button>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
@stop

@section('css')
    {!! \Lib::addMedia('admin/js/library/tag/jquery.tag-editor.css') !!}
    {!! \Lib::addMedia('admin/libs/summernote/summernote-bs4.css') !!}
    {!! \Lib::addMedia('admin/js/library/uploadifive/uploadifive.css') !!}
@stop

@section('js_bot')
    {!! \Lib::addMedia('admin/libs/summernote/summernote.min.js') !!}
<script type="text/javascript">
    $("#submit").on("click", function () {
        var hvalue = $('#product-description + .note-editor .note-editable.card-block').html();
        $(this).append("<textarea name='body' style='display:none'>"+hvalue+"</textarea>");
    });

    shop.updateRoutes = function () {
        shop.ajax_popup('route', 'POST', {}, function(json) {
            console.log(json);
            var html,i;
            html = '<div><b>PUBLIC ROUTES: </b></div>';
            for(i in json.data){
                html += '<p>'+json.data[i]+'</p>';
            }
            $('#result').html(html);
            shop.ajax_popup('config/route', 'POST', {}, function(json) {
                console.log(json);
                html = '<div><b>ADMIN ROUTES: </b></div>';
                for(i in json.data){
                    html += '<p>'+json.data[i]+'</p>';
                }
                $('#result').append(html);
            });
        },{
            baseUrl: ENV.PUBLIC_URL
        });
    };


    jQuery(document).ready(function(){
        // Summernote
        $('#product-description').summernote({
            height: 580,                 // set editor height
            minHeight: null,             // set minimum height of editor
            maxHeight: null,             // set maximum height of editor
            focus: false,                 // set focus to editable area after initializing summernote
            toolbar: [
                // [groupName, [list of button]]
                ['style', ['bold', 'italic', 'underline', 'clear']],
                ['font', ['strikethrough', 'superscript', 'subscript']],
                ['fontsize', ['fontsize']],
                ['fontname', ['fontname']],
                ['color', ['color']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['height', ['height']],
                ['view', ['fullscreen', 'codeview', 'help']],
            ]
        });

        // Select2
        $('.select2').select2();

    });
</script>
    {!! \Lib::addMedia('admin/js/form-fileuploads.init.js') !!}
@stop