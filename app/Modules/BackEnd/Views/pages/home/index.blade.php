@extends('BackEnd::layouts.default')

@section('content')
<div class="jumbotron">
    <h1 class="display-3">Xin chào, {{ \Auth::user()->fullname }}</h1>
    <p class="lead">Đây là trang quản trị của dự án {{ env('APP_NAME') }}</p>
    <hr class="my-4">
    <p>Trong quá trình thao tác bạn gặp khó khăn hoặc cần cung cấp thêm quyền sử dụng vui lòng liên hệ với Admin</p>
    <p class="lead">
        <a class="btn btn-primary btn-lg" href="mailto:lymanhha@gmail.com" role="button"><i class="mdi mdi-email-check"></i>&nbsp; Gửi email liên hệ</a>
    </p>
</div>
<style>
    body {
        height: 100vh;
    }
    footer {
        position: absolute;
        bottom: 0;
        width: 100%;
    }
</style>
@stop