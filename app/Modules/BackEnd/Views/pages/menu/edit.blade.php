@extends('BackEnd::layouts.default')

@section('content')
    <div class="row">
        <div class="col-sm-12">
            {!! Form::open(['url' => route('admin.'.$key.'.edit.post', $data->id), 'files' => true ]) !!}

            @if( count($errors) > 0)
                <div class="alert alert-danger">
                    @foreach ($errors->all() as $error)
                        <div>{!! $error !!}</div>
                    @endforeach
                </div>
            @endif
            @if (session('status'))
                <div class="alert alert-success">
                    {!! session('status') !!}
                </div>
            @endif

            <div class="card">
                <div class="card-header">
                    <i class="fe-edit"></i>THÔNG TIN CƠ BẢN
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="title">Tiêu đề</label>
                                <input type="text" class="form-control{{ $errors->has('title') ? ' is-invalid' : '' }}" id="title" name="title" value="{{ old('title', $data->title) }}" required>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="link">URL hoặc Route name</label>
                                <input type="text" class="form-control{{ $errors->has('link') ? ' is-invalid' : '' }}" id="link" name="link" value="{{ old('link', $data->link) }}">
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label for="sort">Sắp xếp</label>
                                <input type="text" class="form-control{{ $errors->has('sort') ? ' is-invalid' : '' }}" id="sort" name="sort" value="{{ old('sort', $data->sort) }}">
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label for="lang">Ngôn ngữ</label>
                                <select id="lang" name="lang" class="form-control{{ $errors->has('lang') ? ' is-invalid' : '' }}" onchange="shop.getMenu($('#type').val(), this.value)">
                                    @foreach($langOpt as $k => $v)
                                        <option value="{{ $k }}" @if(old('lang', $data->lang) == $k) selected="selected" @endif>{{ $v }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="card">
                <div class="card-header">
                    <i class="fe-edit"></i>PHÂN LOẠI MENU
                </div>
                <div class="card-body">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for="type">Loại Menu</label>
                            <select id="type" name="type" class="form-control{{ $errors->has('type') ? ' is-invalid' : '' }}" onchange="shop.getMenu(this.value, $('#lang').val())">
                                <option value="-1">-- Chọn --</option>
                                @foreach($allType as $k => $v)
                                    <option value="{{ $k }}" @if(old('type', $data->type) == $k) selected="selected" @endif>{{ $v }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for="pid">Menu cha</label>
                            <select id="pid" name="pid" class="form-control{{ $errors->has('pid') ? ' is-invalid' : '' }}">
                                <option value="0">-- Chọn --</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="card-box">
                    <h5 class="text-uppercase mt-0 mb-3 bg-light p-2">Dành cho SEO</h5>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group mb-3">
                                <label for="product-meta-title">Meta title</label>
                                <input type="text" class="form-control" name="title_seo" value="{{ old('title_seo', $data->title_seo) }}" id="product-meta-title" placeholder="Enter title">
                            </div>

                            <div class="form-group mb-3">
                                <label for="product-meta-keywords">Meta Keywords</label>
                                <input type="text" class="form-control" name="keywords" value="{{ old('keywords', $data->keywords) }}" id="product-meta-keywords" placeholder="Enter keywords">
                            </div>

                            <div class="form-group mb-0">
                                <label for="product-meta-description">Meta Description </label>
                                <textarea class="form-control" rows="5" name="description" id="product-meta-description" placeholder="Please enter description">{{ old('description', $data->description) }}</textarea>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group mb-3">
                                <label for="image">Ảnh seo <span class="text-danger">*</span></label>
                                <input type="file" class="dropify" data-default-file="{{ $data->getImageUrl('original') }}" name="image"  />
                            </div>
                        </div>
                    </div>
                </div> <!-- end card-box -->
            </div>
            <div class="card">
                <div class="card-header">
                    <i class="fe-edit"></i>THÔNG TIN PHỤ
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label for="icon">Icon Class</label>
                                <input type="text" class="form-control{{ $errors->has('icon') ? ' is-invalid' : '' }}" id="icon" name="icon" value="{{ old('icon', $data->icon) }}">
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-6">
                            <label class="checkbox-inline" for="no_follow">
                                <input type="checkbox" id="no_follow" name="no_follow" value="1" @if(old('no_follow', $data->no_follow) == 1) checked @endif>  No Follow
                            </label>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-6">
                            <label class="checkbox-inline" for="newtab">
                                <input type="checkbox" id="newtab" name="newtab" value="1" @if(old('newtab', $data->newtab) == 1) checked @endif>  Bật Tab mới
                            </label>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label for="perm">Phân quyền</label>
                                <select id="perm" name="perm" class="form-control{{ $errors->has('perm') ? ' is-invalid' : '' }}">
                                    <option value="">-- Chọn --</option>
                                    @foreach($permList as $k => $v)
                                        <optgroup label="{{ $v['title'] }}">
                                            @foreach($v['perm'] as $p => $t)
                                                <option value="{{ $k.'-'.$p }}" @if(old('perm', $data->perm) == $k.'-'.$p) selected="selected" @endif>{{ $k.'.'.$p.' - '.$t }}</option>
                                            @endforeach
                                        </optgroup>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="mb-3">
                <button type="submit" class="btn btn-sm btn-primary"><i class="fe-send"></i> Cập nhật</button>
                &nbsp;&nbsp;
                <a class="btn btn-sm btn-danger" href="{{ redirect()->back()->getTargetUrl() }}"><i class="fa fa-ban"></i> Hủy bỏ</a>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
@stop
@section('css')
    {!! \Lib::addMedia('admin/libs/dropzone/dropzone.min.css') !!}
@stop

@section('js_bot')
    {!! \Lib::addMedia('admin/libs/dropzone/dropzone.min.js') !!}
    {!! \Lib::addMedia('admin/js/form-fileuploads.init.js') !!}
    <script type="text/javascript">
        shop.ready.add(function(){
            shop.getMenu($('#type').val(), $('#lang').val(), {{ $data->pid }});
            $("#link").autocomplete({
                position: { my : "right top", at: "right bottom" },
                minLength: 1,
                delay: 500,
                scroll: true,
                source: {!! $routes !!}
            });
        },true);
        shop.getMenu = function (type, lang, def) {
            var html = '<option value="0">-- Chọn --</option>';
            shop.ajax_popup('menu/get-menu', 'POST', {type:type, lang:lang}, function(json) {
                $.each(json.data,function (ind,value) {
                    html += '<option value="'+value.id+'"'+(def == value.id?' selected':'')+'>'+value.title+'</option>';
                    if(value.sub.length != 0){
                        $.each(value.sub,function (k,sub) {
                            html += '<option value="'+sub.id+'"'+(def == sub.id?' selected':'')+'> &nbsp;&nbsp;&nbsp; '+sub.title+'</option>';
                        });
                    }
                });
                $('#pid').html(html);
            });
        };
    </script>
@stop