<?php

namespace App\Modules\BackEnd\Controllers;

use App\Models\Category;
use Illuminate\Http\Request;
use App\Models\Widget as THIS;


class WidgetController extends BackendController
{
    public function __construct(){
        parent::__construct(new THIS());
        \View::share('catOpt', Category::getCat(3));
        $this->registerAjax('add-widget', 'ajaxAddWidget', 'change');
    }

    public function index(Request $request){
        return $this->returnView('index', [
            'widgets' => THIS::widgetsInit(),
            'sidebar' => THIS::$register_sidebar,
            'site_title' => $this->title,
        ]);
    }



    protected function ajaxAddWidget(Request $request){
        dd($request->all());
        return \Lib::ajaxRespond(true, 'success', $request->all());
        if(isset($request->title) && !empty($request->title)){

        }
        parent::beforeSave($request);
    }


}
