<?php

namespace App\Modules\BackEnd\Controllers;

use App\Models\OrderLog;
use App\Models\Service;
use Illuminate\Http\Request;
use App\Models\Orders as THIS;
use Validator;
use Illuminate\Support\Facades\Auth;

class OrdersController extends BackendController
{
    protected $timeStamp = 'created';
    protected static $splitKey;
    public function __construct(){
        $this->bladeAdd = 'add';
        parent::__construct(new THIS());
        /* assign:  1 -> 2

        */
        $this->registerAjax('assign', 'ajaxAssignOrder4User', 'edit');
        $this->registerAjax('get-type-service-4orders', 'ajaxGetService4Orders', 'view');
    }

    public function index(Request $request){
        $order = 'created DESC, id DESC';
        $cond = [];
        if ($request->status != '') {
            $cond[] = ['status', $request->status];
        } else {
            $cond[] = ['status', '>', 0];
        }
        if($request->name != ''){
            $cond[] = ['name','LIKE','%'.$request->name.'%'];
        }
        if(!empty($request->time_from)){
            $timeStamp = \Lib::getTimestampFromVNDate($request->time_from);
            array_push($cond, ['created', '>=', $timeStamp]);
        }
        if(!empty($request->time_to)){
            $timeStamp = \Lib::getTimestampFromVNDate($request->time_to, true);
            array_push($cond, ['created', '<=', $timeStamp]);
        }
        if(!empty($cond)) {
            $data = THIS::with('service')->where($cond)->orderByRaw($order)->paginate($this->recperpage);
        }else{
            $data = THIS::with('service')->orderByRaw($order)->paginate($this->recperpage);
        }

        return $this->returnView('index', [
            'data' => $data,
            'search_data' => $request
        ]);
    }

    public static function fetchCat($cat, $imgSize = ''){
        $out = $cat->toArray();
        $out['sub'] = [];
        return $out;
    }

    /*Gan user xu ly don hang*/
    protected function ajaxAssignOrder4User(Request $request)
    {
        if (\Auth::user() && $request->id > 0) {
            $order = THIS::where('id', $request->id)->update([
                'updated' => time(),
                'status' => ($request->is_take) ?  $request->is_take : 1,
            ]);
//            if ($order == 1 && $request->is_take == 1) {
////                event('received', [$request->id, \Lib::getDefaultLang()]);
//            }
//            OrderLog::add($request->id,$request->is_take == 1 ? 'assign' : 'unassign');
            return \Lib::ajaxRespond(true, 'success');
        }
        return \Lib::ajaxRespond(false, 'Không tìm thấy dữ liệu');
    }

    /*Gan user xu ly don hang*/
    protected function ajaxGetService4Orders(Request $request)
    {
        if (\Auth::user() && $request->id > 0) {
            $opt_detail = [];
            $order = THIS::where('id', $request->id)->first();
            if(!empty($order['opt_detail'])) {
                $order['opt_detail'] = json_decode($order['opt_detail']);
                $serice = Service::where('status', '>', 1)->get();
                foreach ($serice as $ser) {
                    foreach ($order['opt_detail'] as $k => $or_dl)
                    if($ser->id == $k && $or_dl != null){
                        $or_dl[] = $ser->title;
                        $opt_detail[$ser->id] = $or_dl;
    
                    }
                }
                $tpl['opt_detail'] = $opt_detail;
                return \Lib::ajaxRespond(true, 'success', $tpl);
            }else {
                $tpl['opt_detail']['_0_'][] = $order->service->title;
                return \Lib::ajaxRespond(true, 'success', $tpl);
            }
            return \Lib::ajaxRespond(true, 'Không tìm thấy dữ liệu');
            
        }
        return \Lib::ajaxRespond(false, 'Không tìm thấy dữ liệu');
    }
}
