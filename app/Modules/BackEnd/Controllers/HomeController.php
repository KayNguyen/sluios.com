<?php

namespace App\Modules\BackEnd\Controllers;

use App\Http\Controllers\Controller;

class HomeController extends Controller
{
    //
    public function __construct(){
        \Lib::addBreadcrumb();
    }

    public function index(){
        return view('BackEnd::pages.home.index', [
            'site_title' => 'Trang chủ'
        ]);
    }

    public function checkAuth(){
        return redirect()->to(url()->full().'/login')->send();
    }
}
