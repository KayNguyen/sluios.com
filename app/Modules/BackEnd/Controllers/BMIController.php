<?php

namespace App\Modules\BackEnd\Controllers;

use Illuminate\Http\Request;
use App\Models\Glycemic_BMI_Pressure as THIS;


class BMIController extends BackendController
{
    protected $timeStamp = 'created';

    //config controller, ez for copying and paste
    public function __construct(){
        $this->bladeAdd = 'add';
        parent::__construct(new THIS());
        
    }

    public function index(Request $request){
        return $this->returnView('index', [
            'site_title' => $this->title,
            'data' => $this->loadInfo(),
            'type' => THIS::$bmiType,
        ]);
    }

    public function submit(Request $request){
        
        $default = $this->loadInfo();
        foreach ($request->all() as $k => $v){
            if($k != '_token' && $k != 'image') {

                if (!empty($v)) {
                    $default[$k] = $v;
                } else {
                    $default[$k] = $v==0?$v:'';
                }
            }
        }
        THIS::setAdvice($this->key, json_encode($default));
        return redirect()->route('admin.'.$this->key)->with('status', 'Đã cập nhật thành công');
    }

    protected function loadInfo(){
        $data = THIS::getAdvice($this->key, '');
        return !empty($data) ? json_decode($data, true) : null;
    }
}
