<?php

namespace App\Modules\BackEnd\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AjaxController extends Controller
{
    public function __construct(){}

    public function init(Request $request, $cmd){
        $data = [];
        $perm = true;
        switch ($cmd) {
            /*case 'actived':
                $data = $this->actived($request);
                break;*/
            default:
                $data = $this->nothing();
        }
        if(!$perm) {
            $data = \Lib::ajaxRespond(false, 'Access denied');
        }
        return response()->json($data);
    }

    /*public function actived(Request $request){
        $user = \Auth::user();
        $user->last_active = time();
        $user->save();
        return \Lib::ajaxRespond(true, 'Actived');
    }*/

    public function nothing(){
        return "Nothing...";
    }
}
