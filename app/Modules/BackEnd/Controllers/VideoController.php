<?php


namespace App\Modules\BackEnd\Controllers;

use App\Models\Category;
use App\Models\VideosCate;
use App\Models\Video as THIS;
use Illuminate\Http\Request;

class VideoController extends BackendController
{
    protected $timeStamp = 'created';

    //config controller, ez for copying and paste
    public function __construct(){
        parent::__construct(new THIS(),[
            [
                'videos_id' => 'required|max:50',
                'cate' => 'required',
            ],
            [
                'videos_id.required' => 'ID videos không được bỏ trống',
                'cate.required' => 'Danh mục con không được bỏ trống',
                'videos_id.max' => 'ID videos không được quá 50 ký tự',
            ]
        ]);
        \View::share('catOpt', Category::getCat(3));
        $this->registerAjax('load-video', 'ajaxLoadVideo');
    }

    public function index(Request $request){
        // $author = 'active DESC, last_active DESC, last_login DESC, id DESC';
        $order = 'created DESC';
        $cond = [];
        if ($request->status != '') {
            $cond[] = ['status', $request->status];
        } else {
            $cond[] = ['status', '!=', -1];
        }
        if($request->title != ''){
            $cond[] = ['title','LIKE','%'.$request->title.'%'];
        }
        if($request->lang != ''){
            $cond[] = ['lang','=',$request->lang];
        }
        if($request->type != ''){
            $cond[] = ['cate_par','=',$request->type];
        }
        if(!empty($request->time_from)){
            $timeStamp = \Lib::getTimestampFromVNDate($request->time_from);
            array_push($cond, ['created', '>=', $timeStamp]);
        }
        if(!empty($request->time_to)){
            $timeStamp = \Lib::getTimestampFromVNDate($request->time_to, true);
            array_push($cond, ['created', '<=', $timeStamp]);
        }
        if(!empty($request->publish_from)){
            $timeStamp = \Lib::getTimestampFromVNDate($request->publish_from);
            array_push($cond, ['published', '>=', $timeStamp]);
        }
        if(!empty($request->publish_to)){
            $timeStamp = \Lib::getTimestampFromVNDate($request->publish_to, true);
            array_push($cond, ['published', '<=', $timeStamp]);
        }
        if(!empty($cond)) {
            $data = THIS::where($cond)->orderByRaw($order)->paginate($this->recperpage);
        }else{
            $data = THIS::orderByRaw($order)->paginate($this->recperpage);
        }
//        dd($data->toArray());
        return $this->returnView('index', [
            'data' => $data,
            'search_data' => $request,
            'customer' => ''
        ]);
    }

    public function showEditForm($id){
        $data = THIS::with('categories')->find($id);
        $data = $data->setRelation("categories", $data->categories->keyBy('id'));
        set_old($data);
        return $this->returnView('edit', [
            'data' => $data,
        ]);
    }

    public function buildValidate(Request $request){
        $this->addValidate(
            [
                'videos_id' => 'unique:videos,videos_id,'.$this->editID,
            ],
            [
                'videos_id.unique' => 'ID videos đã tồn tại'
            ]
        );
    }

    public function beforeSave(Request $request, $ignore_ext = [])
    {
        parent::beforeSave($request); // TODO: Change the autogenerated stub
        
        $this->model->status = ($request->status) ? $request->status : 1;
        

         // xoa truong thua
         unset($this->model->cate);
        
    }

    public function afterSave(Request $request)
    {
        if(!empty($request->cate_par)) {
            VideosCate::addCateById($this->editID, $request->cate, $request->cate_par);
        }
    }

    public function ajaxLoadVideo(Request $request) {
        if($request->id > 0) {
            $data = $this->model::find($request->id);
            if ($data) {
                return \Lib::ajaxRespond(true, 'success', $data->videos_id);

            }
        }
        return \Lib::ajaxRespond(false, 'Không tìm thấy dữ liệu');
    }

}