<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Image Driver
    |--------------------------------------------------------------------------
    |
    | Intervention Image supports "GD Library" and "Imagick" to process images
    | internally. You may choose one of them according to your PHP
    | configuration. By default PHP's "GD Library" implementation is used.
    |
    | Supported: "gd", "imagick"
    |
    */

    'driver' => 'gd',
    'defaultImg' => [
        'max' => ['with' => 1500, 'height' => 1500], //for validate
        'size'=> [
            'original' => ['width' => 0, 'height' => 0],
            'small' => ['width' => 80, 'height' => 0],
            'medium' => ['width' => 250, 'height' => 0],
            'large' => ['width' => 800, 'height' => 0],
        ]
    ],
    'data' => [
        'news' => [
            'size'=> [
                'medium' => ['width' => 851, 'height' => 0],
                'medium_2' => ['width' => 288, 'height' => 0],
                'large' => ['width' => 570, 'height' => 0],
            ]
        ],
        'product' => [
            'dir' => 'product',
            'size'=> [
                'original' => ['width' => 0, 'height' => 0],
                'small' => ['width' => 80, 'height' => 0],
                'medium' => ['width' => 450, 'height' => 0],
                'large' => ['width' => 800, 'height' => 0],
            ]
        ],
        'service' => [
            'dir' => 'service',
            'size'=> [
                'original' => ['width' => 0, 'height' => 0],
                'small' => ['width' => 80, 'height' => 0],
                'medium' => ['width' => 450, 'height' => 0],
                'large' => ['width' => 800, 'height' => 0],
            ]
        ],
        'feature' => [
            'dir' => 'feature',
            'max' => ['with' => 1500, 'height' => 1500], //for validate
            'size'=> [
                'original' => ['width' => 0, 'height' => 0],
                'small' => ['width' => 100, 'height' => 0],
                'slide' => ['width' => 600, 'height' => 400],
                'large' => ['width' => 1300, 'height' => 0],
            ]
        ],
        'config' => [
            'max' => ['with' => 845, 'height' => 845], //for validate
            'size'=> [
                'medium_seo' => ['width' => 250, 'height' => 0],
                'seo' => ['width' => 800, 'height' => 800],
            ]
        ],
        'category' => [],
        'file' => [],
        'avatar' => [
            'max' => ['with' => 500, 'height' => 500], //for validate
            'size'=> [
                'small2' => ['width' => 40, 'height' => 0],
                'large' => ['width' => 200, 'height' => 0]
            ]
        ],
        'gallery' => [
            'dir' => 'gallery',
            'max' => ['with' => 1500, 'height' => 1500], //for validate
            'size'=> [
                'original' => ['width' => 0, 'height' => 0],
                'small' => ['width' => 150, 'height' => 0],
                'slide' => ['width' => 350, 'height' => 0],
                'large' => ['width' => 640, 'height' => 0],
            ]
        ]
    ]

];
