<?php

return [
    'name' => "Admin Washfriends",
    'css' => [
        'admin/libs/custombox/custombox.min.css',
        'admin/libs/dropify/dropify.min.css',
        'admin/libs/bootstrap-select/bootstrap-select.min.css',
        'admin/libs/select2/select2.min.css',
        'admin/css/bootstrap.min.css',
        'admin/css/icons.min.css',
        'admin/css/app.min.css',
        'admin/css/custom.min.css',
    ],
    'js'  => [
        'admin/js/vendor.min.js',
        'js/lang.js',
        'js/core.js',
        'admin/js/vue.js',
        'admin/js/app.js',
        'admin/js/admin.js',
        'admin/js/axios.min.js',
        'admin/libs/custombox/custombox.min.js',
        'admin/libs/dropify/dropify.min.js',
        'admin/libs/select2/select2.min.js',
        'admin/libs/bootstrap-select/bootstrap-select.min.js',
        'admin/libs/bootstrap-maxlength/bootstrap-maxlength.min.js',
        'admin/js/ubapp.min.js',
    ],
    'favicon' => 'favicon.gif'
];