<?php

return [
    'name' => "Public site",
    'css' => [
        'html/css/style.css',
        'html/css/custom.css',
    ],
    'js'  => [
        'admin/js/vendor.min.js',
        'html/js/main.js',
        'js/core.js',
        'js/app.js',
    ],
    'site_media' => [],
    'favicon' => 'favicon.gif'
];