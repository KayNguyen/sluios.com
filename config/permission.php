<?php

return [
    'backend' => [
        'gallery'       => ['controller'=>'Gallery','title' => 'Thư viện Ảnh'],
        'category'      => ['controller'=>'Category','title' => 'Danh Mục'],
        'customer'      => ['controller'=>'Customer','title' => 'Khách hàng'],
        'feature'       => ['controller'=>'Feature','title' => 'Banner'],
        'subscriber'    => ['controller'=>'Subscriber','title' => 'Subscriber'],
        'file'          => ['controller'=>'File','title' => 'File','perm' => ['upload' => 'Upload']],
        'tag'           => ['controller'=>'Tag','title' => 'Tag'],
        'news'          => ['controller'=>'News','title' => 'Tin tức','perm_extra' => ['tag' => 'Gán tag']],
        'page'          => ['controller'=>'StaticPage','title' => 'Trang tĩnh'],
        'user'          => ['controller'=>'User','title' => 'Người dùng'],
        'role'          => ['controller'=>'Role','title' => 'Phân quyền'],
        'menu'          => ['controller'=>'Menu','title' => 'Menu'],
        'product'       => ['controller'=>'Product','title' => 'Sản phẩm'],
        'videos'       => ['controller'=>'Video','title' => 'Video'],
        'orders'       => ['controller'=>'Orders','title' => 'Orders'],
        'prices'         => ['controller'=>'Prices','title' => 'Bảng giá'],
        'history'         => ['controller'=>'History','title' => 'Lịch sử phát triển'],
        'service'         => ['controller'=>'Service','title' => 'Dịch vụ'],
        'me'         => ['controller'=>'AboutMe','title' => 'Về chúng tôi'],
        'widget'         => ['controller'=>'Widget','title' => 'Giao diện','form' => 1, 'perm'=>['change' => 'Thay đổi cấu hình']],
        'feedback'         => ['controller'=>'Feedback','title' => 'Phản hổi của khách hàng'],
        'partner'         => ['controller'=>'Partner','title' => 'Đối tác'],
        'trust'         => ['controller'=>'Trust','title' => 'Niềm tin'],

        'config'        => ['controller'=>'Config','title' => 'Cấu hình website', 'form' => 1, 'perm'=>['change' => 'Thay đổi cấu hình']],
    ]
];
