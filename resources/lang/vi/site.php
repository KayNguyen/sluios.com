<?php

return [
    'features' => 'Tính năng',
    'enterprise' => 'Doanh nghiệp',
    'support' => 'Hỗ trợ',
    'pricing' => 'Báo giá',
    'news' => 'Tin tức',
    'trangchu' => 'Trang chủ',
    'camonbandadangkyyeucaucuabandaduocghinhan' => 'Cảm ơn bạn đã đăng ký! Yêu cầu của bạn đã được ghi nhận',
];
